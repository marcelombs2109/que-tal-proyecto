(function($) {
   
   $('.slider-cuadro').slick({
      slidesToShow: 1,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: false,
      dots: false,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 1,
          }
        },
  
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
    });

   $(window).scroll(function(){
      
      if ($(window).scrollTop() >= $('.bandera').offset().top) 
      {
          $('.menu-sticky').addClass('fixed-header');
          $('.menu-sticky .site-title').addClass('visible-title');
          
          $('.banner-left').addClass('fixed-banner-lados');
          $('.banner-right').addClass('fixed-banner-lados');
      }
      else 
      {
          $('.menu-sticky').removeClass('fixed-header');
          $('.menu-sticky .site-title').removeClass('visible-title');
          $('.banner-left').removeClass('fixed-banner-lados');
          $('.banner-right').removeClass('fixed-banner-lados');

      }
      
   });
  
   

})(jQuery);

