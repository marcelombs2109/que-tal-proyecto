<?php
if (!is_page_template( 'page-templates/template-comming.php' )) 
{	
?>
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-24">

                <div class="data-footer-top">
                    <div class="columna1">
                        <p>
                        <strong>TalCual.bo</strong> es un periódico digital que difunde información general sobre los hechos más relevantes de Bolivia y del mundo.
                        </p>    
                    </div>
                    
                    <div class="columna2">
                        <p class="text-center">
                            <strong>TalCual.bo</strong> es producido por los periodistas:
                        </p>    

                        <div class="columna-inter">
                            <p style="padding-left:10px;">
                                - Mario Cañipa Vargas (Director)<br>
                                - Claudia Cordero Cardozo<br>
                                - Aleja Cuevas Pacohuanca<br>
                                - Eduardo Chávez Ballón 
                            </p>
                            <p style="padding-left:10px;">
                                - Mariela Ortiz Cañipa<br>
                                - Aline Quispe Zerrillo<br>
                                - Margoth Vargas Ortega<br>
                                - Lily Zurita Zelada
                            </p>
                        </div>     
                    </div>    
                </div>   
                <div class="data-footer-bottom">
                    <ul class="ul-socialmedia-footer">
                        <li>
                            Síguenos en
                        </li>
                        <li>
                            <a href="<?=get_field("facebook","options")?>" target="_blank" class="facebook-footer">
                                <?=get_svg_file(get_template_directory().'/assets/img/facebook-footer.svg')?>
                            </a>
                            <a href="<?=get_field("facebook","options")?>" target="_blank">
                                Facebook
                            </a>
                        </li>
                        <li>
                            <a href="<?=get_field("twitter","options")?>" target="_blank" class="twitter-footer">
                            <?=get_svg_file(get_template_directory().'/assets/img/twitter-footer.svg')?>
                            </a>
                            <a href="<?=get_field("twitter","options")?>" target="_blank">
                                Twitter
                            </a>
                        </li>
                        <li>
                            <a href="<?=get_field("instagram","options")?>" target="_blank" class="instagram-footer">
                            <?=get_svg_file(get_template_directory().'/assets/img/instagram-footer.svg')?>
                            </a>
                            <a href="<?=get_field("instagram","options")?>" target="_blank">
                                Instagram
                            </a>
                        </li>
                 
                    </ul>    
                </div>    
            </div>    
        </div>
    </div>    
</div>

<?php
}
?>

<?php wp_footer();?>    
    </body>
</html>