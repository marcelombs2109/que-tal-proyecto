<?php 
get_header(); 
?>

<div class="page-interior">
    <div class="container">

        <div class="row">
            <div class="col-md-24">
                <div class="div-title-archive">
                    <h1>
                        <span style="color:#cc2229;">404</span> Página no encontrada
                    </h1>    
                </div>        
            </div>    
        </div>  

    </div>    
</div>
    
<?php get_footer(); ?>