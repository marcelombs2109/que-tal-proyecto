<?php
include_once(__DIR__.'/functions/es_template_functions.php');
include_once(__DIR__.'/functions/es_widgets.php');

function getWordsExcerpt60($excerpt){
    $words = explode(' ', $excerpt, (60 + 1));
    if(count($words) >= 60)
    {
        array_pop($words);
        $cadena=implode(' ', $words);
        $cadena=$cadena."...";
    }
    else{
        $cadena=$excerpt;
        $cadena=$cadena."";   
    }
    return $cadena;
}

function getWordsExcerpt35($excerpt){
    $words = explode(' ', $excerpt, (35 + 1));
    if(count($words) >= 35)
    {
        array_pop($words);
        $cadena=implode(' ', $words);
        $cadena=$cadena."...";
    }
    else{
        $cadena=$excerpt;
        $cadena=$cadena."";   
    }
    return $cadena;
}

function getWordsExcerpt30($excerpt){
    $words = explode(' ', $excerpt, (30 + 1));
    if(count($words) >= 30)
    {
        array_pop($words);
        $cadena=implode(' ', $words);
        $cadena=$cadena."...";
    }
    else{
        $cadena=$excerpt;
        $cadena=$cadena."";   
    }
    return $cadena;
}

function getWordsExcerpt25($excerpt){
    $words = explode(' ', $excerpt, (25 + 1));
    if(count($words) >= 25)
    {
      array_pop($words);
      $cadena=implode(' ', $words);
      $cadena=$cadena."...";
    }
    else{
      $cadena=$excerpt;
      $cadena=$cadena."";   
    }
    return $cadena;
}

function getWordsExcerpt20($excerpt){
    $words = explode(' ', $excerpt, (15 + 1));
    if(count($words) >= 15)
    {
      array_pop($words);
      $cadena=implode(' ', $words);
      $cadena=$cadena."...";
    }
    else{
      $cadena=$excerpt;
      $cadena=$cadena."";   
    }
    return $cadena;
}

function getWordsExcerpt13($excerpt){
    $words = explode(' ', $excerpt, (13 + 1));
    if(count($words) >= 13)
    {
      array_pop($words);
      $cadena=implode(' ', $words);
      $cadena=$cadena."...";
    }
    else{
      $cadena=$excerpt;
      $cadena=$cadena."";   
    }
    return $cadena;
}

function getWordsExcerpt9($excerpt){
    $words = explode(' ', $excerpt, (9 + 1));
    if(count($words) >= 9)
    {
      array_pop($words);
      $cadena=implode(' ', $words);
      $cadena=$cadena."...";
    }
    else{
      $cadena=$excerpt;
      $cadena=$cadena."";   
    }
    return $cadena;
}

add_filter('upload_mimes', 'my_upload_mimes');
function my_upload_mimes($mimes = array()) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

function get_svg_file($url){
    $svg_file = file_get_contents($url);
    return $svg_file;
}

function custom_excerpt_length( $length ) {
    return 90;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

add_action('init', 'periodico_init');
function periodico_init ()
{
    register_nav_menus(array(
        'menu_principal'     => 'Menu Principal',
    ));
    add_theme_support('post-thumbnails');
    add_image_size( 'thumb-noticias-home', 275, 275,array( 'center', 'top' ) );
    add_image_size( 'thumb-noticias-sticky-intermedia', 758,400,array( 'center', 'top' ) );
    add_image_size( 'thumb-noticias-sticky-normal', 490,375,array( 'center', 'top' ) );
    add_image_size( 'thumb-noticias-sticky-importante', 1165,555,array( 'center', 'top' ) );
    add_image_size( 'thumb-noticias-sticky-95x75', 95,75,array( 'center', 'top' ) );

    add_image_size( 'thumb-noticias-sticky-230x130', 230,130,array( 'center', 'top' ) );

    add_image_size( 'thumb-noticias-sticky-280x185', 280,185,array( 'center', 'top' ) );

    add_image_size( 'thumb-noticias-sticky-252x250', 252,250,array( 'center', 'top' ) );
    add_image_size( 'thumb-noticias-sticky-85x85', 85,85,array( 'center', 'top' ) );

    add_image_size( 'thumb-noticias-sticky-268x204', 368,204,array( 'center', 'top' ) );


    add_image_size( 'thumb-noticias-sticky-271x180', 271,180,array( 'center', 'top' ) );

    add_image_size( 'thumb-noticias-sticky-365x243', 365,243,array( 'center', 'top' ) );

    add_image_size( 'thumb-noticias-sticky-170x113', 170,113,array( 'center', 'top' ) );

    add_image_size( 'thumb-noticias-sticky-314x170', 379,180,array( 'center', 'top' ) );

}

//Menus
add_filter('wp_nav_menu_items', 'periodico_add_menu_items', 10, 2);

function periodico_add_menu_items ($items, $args)
{
    if ($args->menu == 'menu_principal') {

        $items='<li class="icono-home"><a href="'.get_home_url().'">'.get_svg_file(get_template_directory().'/assets/img/icons8-home.svg').'</a></li>'.$items;

        $items .= '';
    }
    return $items;
}

function display_date_4() {
    return date_i18n('l') .", " . date_i18n('d') . " de ". date_i18n('F'). " de " .date_i18n('Y');
}

function getDestacado(){
    $args = array(
        'post_type' => 'noticias',
        'tax_query' => array(
            array(
                'taxonomy' => 'destacados',
                'field'    => 'slug',
                'terms'    => 'subapertura',
            ),
        ),
    );
    $array_destacado=[];
    $query = new WP_Query($args);
    while ($query->have_posts()) 
    {
        $query->the_post();
        $post_id= get_the_ID();
        $array_destacado[]=$post_id;
    }
    return $array_destacado; 
}


if( function_exists('acf_add_local_field_group') )
{

    acf_add_local_field_group(array(
        'key' => 'group_6142a9ec10894',
        'title' => 'Autor',
        'fields' => array(
            array(
                'key' => 'field_6142a9f3b8a56',
                'label' => 'autor',
                'name' => 'autor',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'analisis',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
    
}


if( function_exists('acf_add_local_field_group') ){

acf_add_local_field_group(array(
    'key' => 'group_612272471496c',
    'title' => 'Plantillas',
    'fields' => array(
        array(
            'key' => 'field_612272506e122',
            'label' => 'Seleccionar Plantilla',
            'name' => 'seleccionar_plantilla',
            'type' => 'select',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'choices' => array(
                'Normal' => 'Normal',
                'Intermedia' => 'Intermedia',
                'Importante' => 'Importante',
            ),
            'default_value' => false,
            'allow_null' => 0,
            'multiple' => 0,
            'ui' => 0,
            'return_format' => 'value',
            'ajax' => 0,
            'placeholder' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'page_template',
                'operator' => '==',
                'value' => 'page-templates/template-home.php',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

}


if( function_exists('acf_add_local_field_group') )
{

acf_add_local_field_group(array(
    'key' => 'group_613d11e90e151',
    'title' => 'Contactos',
    'fields' => array(
        array(
            'key' => 'field_613d11efb5b1d',
            'label' => 'Facebook',
            'name' => 'facebook',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array(
            'key' => 'field_613d1210b5b1e',
            'label' => 'Twitter',
            'name' => 'twitter',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array(
            'key' => 'field_613d1216b5b1f',
            'label' => 'Instagram',
            'name' => 'instagram',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_613d105992795',
    'title' => 'Personaje Entre Nos',
    'fields' => array(
        array(
            'key' => 'field_613d108f135ea',
            'label' => 'Nombre del Personaje',
            'name' => 'nombre_del_personaje',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array(
            'key' => 'field_613d10b5135eb',
            'label' => 'Frase',
            'name' => 'frase',
            'type' => 'textarea',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'rows' => '',
            'new_lines' => '',
        ),
        array(
            'key' => 'field_613d10ef135ec',
            'label' => 'Fotografías',
            'name' => 'fotografias',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'collapsed' => '',
            'min' => 0,
            'max' => 0,
            'layout' => 'table',
            'button_label' => '',
            'sub_fields' => array(
                array(
                    'key' => 'field_613d49db8e353',
                    'label' => 'Fotografía',
                    'name' => 'fotografia',
                    'type' => 'image',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'url',
                    'preview_size' => 'medium',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                ),
            ),
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_taxonomy',
                'operator' => '==',
                'value' => 'categoria_estilo_de_vida:entre-nos',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_6141505234f81',
    'title' => 'Publicidad 1',
    'fields' => array(
        array(
            'key' => 'field_61415059191a3',
            'label' => 'Publicidad 1',
            'name' => 'publicidad_1',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'url',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        array(
            'key' => 'field_6141507d191a4',
            'label' => 'Link Publicidad 1',
            'name' => 'link_publicidad_1',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));


acf_add_local_field_group(array(
    'key' => 'group_6141505234e82',
    'title' => 'Publicidad 2',
    'fields' => array(
        array(
            'key' => 'field_61415059191b4',
            'label' => 'Publicidad 2',
            'name' => 'publicidad_2',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'url',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        array(
            'key' => 'field_6141507d191b5',
            'label' => 'Link Publicidad 2',
            'name' => 'link_publicidad_2',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));


acf_add_local_field_group(array(
    'key' => 'group_6141505234k81',
    'title' => 'Publicidad 3',
    'fields' => array(
        array(
            'key' => 'field_61415059191h3',
            'label' => 'Publicidad 3',
            'name' => 'publicidad_3',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'url',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        array(
            'key' => 'field_6141507d191c9',
            'label' => 'Link Publicidad 3',
            'name' => 'link_publicidad_3',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_6141505234o81',
    'title' => 'Publicidad 4',
    'fields' => array(
        array(
            'key' => 'field_61415059191t2',
            'label' => 'Publicidad 4',
            'name' => 'publicidad_4',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'url',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        array(
            'key' => 'field_6141507d191j9',
            'label' => 'Link Publicidad 4',
            'name' => 'link_publicidad_4',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_6141505234f61',
    'title' => 'Publicidad 5',
    'fields' => array(
        array(
            'key' => 'field_61415059191q1',
            'label' => 'Publicidad 5',
            'name' => 'publicidad_5',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'url',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        array(
            'key' => 'field_6141507d191u5',
            'label' => 'Link Publicidad 5',
            'name' => 'link_publicidad_5',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));


acf_add_local_field_group(array(
    'key' => 'group_6141505234fy5',
    'title' => 'Publicidad 6',
    'fields' => array(
        array(
            'key' => 'field_61415059191y7',
            'label' => 'Publicidad 6',
            'name' => 'publicidad_6',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'url',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        array(
            'key' => 'field_6141507d191r7',
            'label' => 'Link Publicidad 6',
            'name' => 'link_publicidad_6',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));


acf_add_local_field_group(array(
    'key' => 'group_6141505234v81',
    'title' => 'Publicidad 7',
    'fields' => array(
        array(
            'key' => 'field_61415059191v2',
            'label' => 'Publicidad 7',
            'name' => 'publicidad_7',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'url',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        array(
            'key' => 'field_6141507d191w6',
            'label' => 'Link Publicidad 7',
            'name' => 'link_publicidad_7',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_6141505234m81',
    'title' => 'Publicidad 8',
    'fields' => array(
        array(
            'key' => 'field_61415059191n8',
            'label' => 'Publicidad 8',
            'name' => 'publicidad_8',
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'url',
            'preview_size' => 'medium',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        array(
            'key' => 'field_6141507d191l6',
            'label' => 'Link Publicidad 8',
            'name' => 'link_publicidad_8',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'acf-options',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));


acf_add_local_field_group(array(
    'key' => 'group_613d4a34b1f4a',
    'title' => 'Texto Introductorio',
    'fields' => array(
        array(
            'key' => 'field_613d4a43c4e91',
            'label' => 'Texto Introductorio',
            'name' => 'texto_introductorio',
            'type' => 'textarea',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'maxlength' => '',
            'rows' => '',
            'new_lines' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'noticias',
            ),
        ),
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'podcasts',
            ),
        ),
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'reporte_especial',
            ),
        ),
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'analisis',
            ),
        ),
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'enfoques_entrevistas',
            ),
        ),
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'empresas',
            ),
        ),
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'estilo_de_vida',
            ),
        ),
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'bbc_mundo',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

}


if( function_exists('acf_add_local_field_group') )
{

    acf_add_local_field_group(array(
        'key' => 'group_6145460b842c9',
        'title' => 'Reportes Relacionados',
        'fields' => array(
            array(
                'key' => 'field_614546246c22b',
                'label' => 'Reportes Relacionados',
                'name' => 'reportes_relacionados',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 0,
                'max' => 0,
                'layout' => 'table',
                'button_label' => '',
                'sub_fields' => array(
                    array(
                        'key' => 'field_614546326c22c',
                        'label' => 'Reporte Relacionado',
                        'name' => 'reporte_relacionado',
                        'type' => 'relationship',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'post_type' => array(
                            0 => 'reporte_especial',
                        ),
                        'order' => 'desc',
                        'taxonomy' => '',
                        'filters' => '',
                        'elements' => '',
                        'min' => 3,
                        'max' => 3,
                        'return_format' => 'object',
                    ),
                ),
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
    
}




/*
* Tipos de contenidos Programas
*/

$labels = array (
    'name'=>'Noticias',
    'singular_name'=>'Noticias',
    'add_new'=>'Adicionar nueva noticia',
    'add_new_item'=>'Adicionar nueva noticia',
    'edit_item'=>'Editar noticia',
    'new_item'=>'Nueva noticia',
    'all_items'=>'Todas los noticias',
    'view_item'=>'Ver noticia',
    'search_items'=>'Buscar noticia',
    'not_found'=>'No se encontro el noticia',
    'not_found_in_trash'=>'Nada en el basurero',
    'parent_item_colon'=>'',
    'menu_name'=>'Noticias'
);
$args = array (
    'labels'=>$labels,
    'public'=>true,
    'publicly_queryable'=>true,
    'show_ui'=>true,
    'show_in_menu'=>true,
    'query_var'=>true,
    'rewrite'=>true,
    'capability_type'=>'post',
    'has_archive'=>true,
    'hierarchical'=>false,
    'supports'=>array ( 'title','editor','thumbnail','excerpt','comments')
);
register_post_type('noticias',$args);


$labels = array (
    'name'=>'Podcasts',
    'singular_name'=>'Podcasts',
    'add_new'=>'Adicionar nuevo podcasts',
    'add_new_item'=>'Adicionar nuevo podcasts',
    'edit_item'=>'Editar podcasts',
    'new_item'=>'Nuevo podcasts',
    'all_items'=>'Todas los podcasts',
    'view_item'=>'Ver podcasts',
    'search_items'=>'Buscar podcasts',
    'not_found'=>'No se encontro el podcasts',
    'not_found_in_trash'=>'Nada en el basurero',
    'parent_item_colon'=>'',
    'menu_name'=>'Podcasts'
);
$args = array (
    'labels'=>$labels,
    'public'=>true,
    'publicly_queryable'=>true,
    'show_ui'=>true,
    'show_in_menu'=>true,
    'query_var'=>true,
    'rewrite'=>true,
    'capability_type'=>'post',
    'has_archive'=>true,
    'hierarchical'=>false,
    'supports'=>array ( 'title','editor','thumbnail','excerpt','comments')
);
register_post_type('podcasts',$args);


$labels = array (
    'name'=>'Reporte Especial',
    'singular_name'=>'Reporte Especial',
    'add_new'=>'Adicionar nuevo Reporte Especial',
    'add_new_item'=>'Adicionar nuevo Reporte Especial',
    'edit_item'=>'Editar Reporte Especial',
    'new_item'=>'Nuevo Reporte Especial',
    'all_items'=>'Todas los Reportes Especiales',
    'view_item'=>'Ver Reporte Especial',
    'search_items'=>'Buscar Reporte Especial',
    'not_found'=>'No se encontro el Reporte Especial',
    'not_found_in_trash'=>'Nada en el basurero',
    'parent_item_colon'=>'',
    'menu_name'=>'Reporte Especial'
);
$args = array (
    'labels'=>$labels,
    'public'=>true,
    'publicly_queryable'=>true,
    'show_ui'=>true,
    'show_in_menu'=>true,
    'query_var'=>true,
    'rewrite'=>true,
    'capability_type'=>'post',
    'has_archive'=>true,
    'hierarchical'=>false,
    'supports'=>array ( 'title','editor','thumbnail','excerpt','comments')
);
register_post_type('Reporte_especial',$args);


$labels = array (
    'name'=>'Análisis',
    'singular_name'=>'Análisis',
    'add_new'=>'Adicionar nuevo Análisis',
    'add_new_item'=>'Adicionar nuevo Análisis',
    'edit_item'=>'Editar Análisis',
    'new_item'=>'Nuevo Análisis',
    'all_items'=>'Todas los Análisis',
    'view_item'=>'Ver Análisis',
    'search_items'=>'Buscar Análisis',
    'not_found'=>'No se encontro el Análisis',
    'not_found_in_trash'=>'Nada en el basurero',
    'parent_item_colon'=>'',
    'menu_name'=>'Análisis'
);
$args = array (
    'labels'=>$labels,
    'public'=>true,
    'publicly_queryable'=>true,
    'show_ui'=>true,
    'show_in_menu'=>true,
    'query_var'=>true,
    'rewrite'=>true,
    'capability_type'=>'post',
    'has_archive'=>true,
    'hierarchical'=>false,
    'supports'=>array ( 'title','editor','thumbnail','excerpt','comments')
);
register_post_type('analisis',$args);


$labels = array (
    'name'=>'Enfoques y Entrevistas',
    'singular_name'=>'Enfoques y Entrevistas',
    'add_new'=>'Adicionar nuevo Enfoques y Entrevistas',
    'add_new_item'=>'Adicionar nuevo Enfoques y Entrevistas',
    'edit_item'=>'Editar Enfoques y Entrevistas',
    'new_item'=>'Nuevo Enfoques y Entrevistas',
    'all_items'=>'Todos los Enfoques y Entrevistas',
    'view_item'=>'Ver Enfoques y Entrevistas',
    'search_items'=>'Buscar Enfoques y Entrevistas',
    'not_found'=>'No se encontro el Enfoques y Entrevistas',
    'not_found_in_trash'=>'Nada en el basurero',
    'parent_item_colon'=>'',
    'menu_name'=>'Enfoques y Entrevistas'
);
$args = array (
    'labels'=>$labels,
    'public'=>true,
    'publicly_queryable'=>true,
    'show_ui'=>true,
    'show_in_menu'=>true,
    'query_var'=>true,
    'rewrite'=>true,
    'capability_type'=>'post',
    'has_archive'=>true,
    'hierarchical'=>false,
    'supports'=>array ( 'title','editor','thumbnail','excerpt','comments')
);
register_post_type('enfoques_entrevistas',$args);


$labels = array (
    'name'=>'Empresas',
    'singular_name'=>'Empresas',
    'add_new'=>'Adicionar nuevo Empresas',
    'add_new_item'=>'Adicionar nuevo Empresas',
    'edit_item'=>'Editar Empresas',
    'new_item'=>'Nuevo Empresas',
    'all_items'=>'Todos los Empresas',
    'view_item'=>'Ver Empresas',
    'search_items'=>'Buscar Empresas',
    'not_found'=>'No se encontro el Empresas',
    'not_found_in_trash'=>'Nada en el basurero',
    'parent_item_colon'=>'',
    'menu_name'=>'Empresas'
);
$args = array (
    'labels'=>$labels,
    'public'=>true,
    'publicly_queryable'=>true,
    'show_ui'=>true,
    'show_in_menu'=>true,
    'query_var'=>true,
    'rewrite'=>true,
    'capability_type'=>'post',
    'has_archive'=>true,
    'hierarchical'=>false,
    'supports'=>array ( 'title','editor','thumbnail','excerpt','comments')
);
register_post_type('empresas',$args);


$labels = array (
    'name'=>'Estilo de Vida',
    'singular_name'=>'Estilo de Vida',
    'add_new'=>'Adicionar nuevo Estilo de Vida',
    'add_new_item'=>'Adicionar nuevo Estilo de Vida',
    'edit_item'=>'Editar Estilo de Vida',
    'new_item'=>'Nuevo Estilo de Vida',
    'all_items'=>'Todos los Estilo de Vida',
    'view_item'=>'Ver Estilo de Vida',
    'search_items'=>'Buscar Estilo de Vida',
    'not_found'=>'No se encontro el Estilo de Vida',
    'not_found_in_trash'=>'Nada en el basurero',
    'parent_item_colon'=>'',
    'menu_name'=>'Estilo de Vida'
);
$args = array (
    'labels'=>$labels,
    'public'=>true,
    'publicly_queryable'=>true,
    'show_ui'=>true,
    'show_in_menu'=>true,
    'query_var'=>true,
    'rewrite'=>true,
    'capability_type'=>'post',
    'has_archive'=>true,
    'hierarchical'=>false,
    'supports'=>array ( 'title','editor','thumbnail','excerpt','comments')
);
register_post_type('estilo_de_vida',$args);

$labels = array (
    'name'=>'BBC Mundo',
    'singular_name'=>'BBC Mundo',
    'add_new'=>'Adicionar nuevo BBC Mundo',
    'add_new_item'=>'Adicionar nuevo BBC Mundo',
    'edit_item'=>'Editar BBC Mundo',
    'new_item'=>'Nuevo BBC Mundo',
    'all_items'=>'Todos los BBC Mundo',
    'view_item'=>'Ver BBC Mundo',
    'search_items'=>'Buscar BBC Mundo',
    'not_found'=>'No se encontro el BBC Mundo',
    'not_found_in_trash'=>'Nada en el basurero',
    'parent_item_colon'=>'',
    'menu_name'=>'BBC Mundo'
);
$args = array (
    'labels'=>$labels,
    'public'=>true,
    'publicly_queryable'=>true,
    'show_ui'=>true,
    'show_in_menu'=>true,
    'query_var'=>true,
    'rewrite'=>true,
    'capability_type'=>'post',
    'has_archive'=>true,
    'hierarchical'=>false,
    'supports'=>array ( 'title','editor','thumbnail','excerpt','comments')
);
register_post_type('bbc_mundo',$args);


// Crear Taxonomia para Noticias
function create_noticias_taxonomies() 
{
	$labels = array(
		'name'              => _x( 'Categoría Noticias', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Categoría Noticias', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Buscar Categoría Noticias', 'textdomain' ),
		'all_items'         => __( 'Todas las Categoría Noticias', 'textdomain' ),
		'parent_item'       => __( 'Categoría Noticias Superior', 'textdomain' ),
		'parent_item_colon' => __( 'Categoría Noticias Superior:', 'textdomain' ),
		'edit_item'         => __( 'Editar la Categoría Noticias', 'textdomain' ),
		'update_item'       => __( 'Actualizar Categoría Noticias', 'textdomain' ),
		'add_new_item'      => __( 'Adicionar Nueva Categoría Noticias', 'textdomain' ),
		'new_item_name'     => __( 'Nombre de la nueva Categoría Noticias', 'textdomain' ),
		'menu_name'         => __( 'Categoría Noticias', 'textdomain' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'categoria_noticias' ),
	);
	register_taxonomy( 'categoria_noticias', array( 'noticias' ), $args );
}

add_action( 'init', 'create_noticias_taxonomies', 0 );


// Crear Taxonomia para Estilo de Vida
function create_estilo_de_vida_taxonomies() 
{
	$labels = array(
		'name'              => _x( 'Categoría Estilo de Vida', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Categoría Estilo de Vida', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Buscar Categoría Estilo de Vida', 'textdomain' ),
		'all_items'         => __( 'Todas las Categoría Estilo de Vida', 'textdomain' ),
		'parent_item'       => __( 'Categoría Estilo de Vida Superior', 'textdomain' ),
		'parent_item_colon' => __( 'Categoría Estilo de Vida Superior:', 'textdomain' ),
		'edit_item'         => __( 'Editar la Categoría Estilo de Vida', 'textdomain' ),
		'update_item'       => __( 'Actualizar Categoría Estilo de Vida', 'textdomain' ),
		'add_new_item'      => __( 'Adicionar Nueva Categoría Estilo de Vida', 'textdomain' ),
		'new_item_name'     => __( 'Nombre de la nueva Categoría Estilo de Vida', 'textdomain' ),
		'menu_name'         => __( 'Categoría Estilo de Vida', 'textdomain' ),
	);
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'categoria_estilo_de_vida' ),
	);
	register_taxonomy( 'categoria_estilo_de_vida', array( 'estilo_de_vida' ), $args );
}

add_action( 'init', 'create_estilo_de_vida_taxonomies', 0 );


add_action( 'init', 'create_destacado_taxonomies', 0 );

function create_destacado_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Destacados', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Destacados', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Buscar Destacado', 'textdomain' ),
		'all_items'         => __( 'Todos los destacado', 'textdomain' ),
		'parent_item'       => __( 'Superior Destacado', 'textdomain' ),
		'parent_item_colon' => __( 'Superior Destacado:', 'textdomain' ),
		'edit_item'         => __( 'Editar Destacado', 'textdomain' ),
		'update_item'       => __( 'Actualizar Destacado', 'textdomain' ),
		'add_new_item'      => __( 'Adicionar Nuevo Destacado', 'textdomain' ),
		'new_item_name'     => __( 'Nombre Nuevo Destacado', 'textdomain' ),
		'menu_name'         => __( 'Destacados', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'destacados' ),
	);

	register_taxonomy( 'destacados', array( 'noticias' ), $args );
}



