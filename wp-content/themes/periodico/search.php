<?php 
get_header(); 
?>

<div class="page-interior">
    <div class="container">

        <div class="row">
            <div class="col-md-24">
                
                <div class="div-title-archive">
                    <h1>
                        <?php 
                            if(have_posts() ){
                                printf('Resultados encontrados para: %s', '"'.get_search_query().'"');
                            }
                            else{
                        ?>
                            Contenido no encontrado
                        <?php
                            }
                        ?>
                    </h1>    
                </div>        
            </div>    
        </div>    

        <div class="row">
            <div class="col-md-17">
                <div class="page-listado">  

                    <?php
                        while (have_posts()) {	
                        the_post();
                    ?>

                    <div class="item-post-element">
                        <div class="text-item-post-element">
                            <h4>
                                <a href="<?= get_the_permalink();?>">
                                        <?=get_the_title();?>
                                </a>
                            </h4> 
                            <p>
                                <?=getWordsExcerpt30(get_the_excerpt());?> 
                            </p>
                            <div class="date-item">
                                <span>
                                    <i class="far fa-clock"></i>
                                    <?=get_the_date('d/F/Y');?>
                                </span>
                            </div>    
                        </div>
                        
                        <div class="image-item-post-element hidden-xs">

                            <?php
                            
                            if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-170x113')){
                                $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-170x113');
                            }
                            else{
                                $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                            }
                            
                            ?>
                            <a href="<?= get_the_permalink();?>">
                                <img src="<?= $imagen ?>">
                            </a>
                        </div>                   
                    </div>
                    
                    <?php
                        }
                        wp_reset_query();
                    ?>
                 
                </div>            

            </div>
            <div class="col-md-7">
                <?php get_sidebar();?>  
            </div>    
        </div>    
    </div>    
</div>
    
<?php get_footer(); ?>