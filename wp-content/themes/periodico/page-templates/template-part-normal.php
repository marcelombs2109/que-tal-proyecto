
<div class="content-page-home">

       
        

    <div class="container ">

        <div class="row reportaje hidden-xs">
            <?php 
                get_template_part( 'template-parts/template-part', 'reporteespecial' );
            ?>
        </div>    

        <div class="row">
            <div class="col-md-24">

                <div class="divisor-normal divisor-margin">

                    <div class="columna-1">

                        <h4 class="h4-title-category-red visible-xs">
                            <span>
                                Noticias
                            </span>
                        </h4>

                        <div class="divisor-2-columnas-interior">
                            
                            <div class="columna-1-interior">

                            <?php
                                $args = array(
                                    'post_type' => 'noticias',
                                    'posts_per_page' => 1,
                                    'post__in' => get_option( 'sticky_posts' ),
                                    'ignore_sticky_posts' => 1
                                );
                                $query = new WP_Query( $args );
                                
                                if(!$query->have_posts())
                                {
                                    $args = array(
                                        'post_type' => 'noticias',
                                        'posts_per_page' => 1,
                                        'order'=>'DESC',
                                        'orderby'=>'date',
                                    );
                                    $query = new WP_Query( $args );
                                }

                                $post_id=-1;
                                while ( $query->have_posts() ) 
                                {
                                    $query->the_post();
                                    $post_id= get_the_ID();
    
                                    if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-normal')){
                                        $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-normal');
                                    }
                                    else{
                                        $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                                    }    
                            ?>
                                    <div class="noticia-destacada noticia-destacada-normal" style="background-image: url(<?= $imagen ?>)" onclick='location.href = "<?= get_the_permalink();?>";'>
                                    </div>
                                    <div class="text-noticia-destacada">
                                        <h3>
                                            <a href="<?=get_the_permalink();?>">
                                                <?=get_the_title();?>
                                            </a>
                                        </h3>
                                        <div class="date-item">
                                            <span>
                                                <i class="far fa-clock"></i>
                                                <?=get_the_date('d/F/Y');?>
                                            </span>
                                        </div>
                                        <p>
                                        <?=getWordsExcerpt25(get_the_excerpt());?>
                                        </p>    
                                    </div>
                            <?php
                                } 
                                wp_reset_query();   
                            ?>   
                            </div>
                            <div class="columna-2-interior">
                                <div class="contenedor-noticias-no-destacadas">

                                    <div class="noticias-no-destacadas">

                                    <?php
                                    $post_array = getDestacado();
                                    $post_array[]=$post_id;
                                        $args = array(
                                            'posts_per_page' => '5',
                                            'post_type' => 'noticias',
                                            'post__not_in' => $post_array,
                                            'orderby'=>'date', 
                                            'order'=>'desc',
                                        );
                                        $query = new WP_Query($args);
                                    ?>


                                    <?php
                                        
                                        $post_id=-1;
                                        while ($query->have_posts()) 
                                        {
                                            $query->the_post();
                                            $post_id= get_the_ID();
                                            $post_array[]=get_the_ID() ;
                                            
                                    ?>

                                        <div class="item item-flex">
                                            <div class="imagen-no-destacada">

                                                <?php
                                                    if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-95x75')){
                                                        $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-95x75');
                                                    }
                                                    else{
                                                        $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                                                    }
                                                ?>
                                                <a href="<?=get_the_permalink();?>">
                                                    <img src="<?= $imagen ?>" >
                                                </a>
                                            </div>
                                            <div class="text-no-destacada">
                                                <h5>
                                                <a href="<?=get_the_permalink();?>">
                                                    <?=get_the_title();?>
                                                </a>
                                                </h5>
                                                <div class="date-item">
                                                    <span>
                                                        <i class="far fa-clock"></i>
                                                        <?=get_the_date('d/F/Y');?>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>       

                                    <?php
                                        }    
                                        wp_reset_query();
                                    ?>

                                    </div>
                                        
                                </div>    
                            </div>

                        </div>

                        <?php 
                            get_template_part( 'template-parts/template-part', 'noticiascolumnas' );
                        ?>
                        
                    </div>
                    
                    <div class="columna-2">

                        <div class="row reportaje visible-xs">
                            <?php 
                                get_template_part( 'template-parts/template-part', 'reporteespecial' );
                            ?>
                        </div>        

                        <?php 
                            get_template_part( 'template-parts/template-part', 'analisis' );
                        ?>

                        <?php 
                            get_template_part( 'template-parts/template-part', 'estilodevidaslider' );
                        ?>

                    </div>    
                </div>    
                    
            </div>
        </div>

                            

        <?php 
            get_template_part( 'template-parts/template-part', 'pubmedio1' );
        ?>

        <?php 
            get_template_part( 'template-parts/template-part', 'enfoqueentrevistas' );
        ?>  

        <?php 
            get_template_part( 'template-parts/template-part', 'pubmedio2' );
        ?> 

        <?php 
            get_template_part( 'template-parts/template-part', 'ciudadinternacional' );
        ?>

        <?php 
            get_template_part( 'template-parts/template-part', 'pubmedio4' );
        ?>
        
        <div class="row">
            <div class="col-md-24">
                <h4 class="h4-title-category-black h4-title-category-50">
                    <span>
                    Empresas
                    </span>
                </h4>   
                <div class="divisor-2-columnas-full divisor-margin">
                    <div class="columna">
                        
                        <div class="divisor-2-columnas">
                            <div class="columna">
                                <div class="noticias-no-destacadas">

                                    <?php
                                        $args = array(
                                            'posts_per_page' => '1',
                                            'post_type' => 'empresas',
                                            'orderby'=>'date', 
                                            'order'=>'desc',
                                        );
                                        $query = new WP_Query($args);
                                    ?>

                                    <?php
                                        
                                        $post_id=-1;
                                        while ($query->have_posts()) 
                                        {
                                            $query->the_post();
                                            $post_id= get_the_ID();
                                    ?>
                                
                                    <div class="item">

                                        <?php
                                        
                                        if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-271x180')){
                                            $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-271x180');
                                        }
                                        else{
                                            $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                                        }
                                        
                                        ?>    

                                        <div class="imagen-no-destacada">
                                            <a href="<?=get_the_permalink();?>">
                                                <img src="<?= $imagen ?>" >
                                            </a>
                                        </div>    
                                        <h4>
                                            <a href="<?=get_the_permalink();?>">
                                                <?=get_the_title();?>
                                                
                                            </a>
                                        </h4>
                                        <p>
                                        <?=getWordsExcerpt30(get_the_excerpt());?>
                                        </p>
                                        <div class="date-item">
                                            <span>
                                                <i class="far fa-clock"></i>
                                                <?=get_the_date('d/F/Y');?>
                                            </span>
                                        </div>
                                        
                                    </div> 
                                    <?php
                                        }    
                                        wp_reset_query();
                                    ?>

                                </div>
                            </div>
                            <div class="columna">
                                <div class="noticias-no-destacadas">
                                    
                                    <?php
                                        
                                        $args = array(
                                            'posts_per_page' => '3',
                                            'post_type' => 'empresas',
                                            'post__not_in' => array($post_id),
                                            'orderby'=>'date', 
                                            'order'=>'desc',
                                        );
                                        $query = new WP_Query($args);
                                    ?>    

                                    <?php

                                        $post_array = [];
                                        $post_array[]=$post_id;
                                        while ($query->have_posts()) 
                                        {
                                            $query->the_post();
                                            $post_id_= $query->get_the_ID() ;
                                            $post_array[]=get_the_ID() ;
                                    ?>  
                                    
                                    <div class="item">
                                        <h5>
                                            <a href="<?=get_the_permalink();?>">
                                                <?=get_the_title();?>
                                            </a>
                                        </h5>
                                        <p>
                                        <?=getWordsExcerpt9(get_the_excerpt());?> 
                                        </p>
                                        <div class="date-item">
                                            <span>
                                                <i class="far fa-clock"></i>
                                                <?=get_the_date('d/F/Y');?>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <?php
                                        }    
                                        wp_reset_query();
                                        
                                    ?>

                                </div>
                            </div>    
                        </div>   

                    </div>
                    <div class="columna">
                        
                        <div class="divisor-2-columnas">
                            
                            <div class="columna">
                                <div class="noticias-no-destacadas">

                                    <?php
                                        $args = array(
                                            'posts_per_page' => '1',
                                            'post_type' => 'empresas',
                                            'post__not_in' => $post_array,
                                            'orderby'=>'date', 
                                            'order'=>'desc',
                                        );
                                        $query = new WP_Query($args);
                                    ?>

                                    <?php
                                        
                                        $post_id=-1;
                                        while ($query->have_posts()) 
                                        {
                                            $query->the_post();
                                            $post_id= get_the_ID();
                                            $post_array[]=get_the_ID();
                                    ?>
                                
                                    <div class="item">

                                        <?php
                                        
                                        if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-271x180')){
                                            $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-271x180');
                                        }
                                        else{
                                            $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                                        }
                                        
                                        ?>    

                                        <div class="imagen-no-destacada">
                                            <a href="<?=get_the_permalink();?>">
                                                <img src="<?= $imagen ?>" >
                                            </a>
                                        </div>    
                                        <h4>
                                            <a href="<?=get_the_permalink();?>">
                                                <?=get_the_title();?>
                                                
                                            </a>
                                        </h4>
                                        <p>
                                        <?=getWordsExcerpt30(get_the_excerpt());?>
                                        </p>
                                        <div class="date-item">
                                            <span>
                                                <i class="far fa-clock"></i>
                                                <?=get_the_date('d/F/Y');?>
                                            </span>
                                        </div>
                                        
                                    </div> 
                                    <?php
                                        }    
                                        wp_reset_query();
                                    ?> 
                                    
                                    
                                </div>
                            </div>
                            <div class="columna">
                                <div class="noticias-no-destacadas">
                                    
                                    <?php
                                        
                                        $args = array(
                                            'posts_per_page' => '3',
                                            'post_type' => 'empresas',
                                            'post__not_in' => $post_array,
                                            'orderby'=>'date', 
                                            'order'=>'desc',
                                        );
                                        $query = new WP_Query($args);
                                    ?>    

                                    <?php

                                        while ($query->have_posts()) 
                                        {
                                            $query->the_post();
                                            $post_id_= $query->get_the_ID() ;
                                    ?>  
                                    
                                    <div class="item">
                                        <h5>
                                            <a href="<?=get_the_permalink();?>">
                                                <?=get_the_title();?>
                                            </a>
                                        </h5>
                                        <p>
                                        <?=getWordsExcerpt9(get_the_excerpt());?> 
                                        </p>
                                        <div class="date-item">
                                            <span>
                                                <i class="far fa-clock"></i>
                                                <?=get_the_date('d/F/Y');?>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <?php
                                        }    
                                        wp_reset_query();
                                        
                                    ?>

                                </div>
                            </div>    
                        </div> 

                    </div>    
                </div>

            </div>        
        </div>

        <?php 
            get_template_part( 'template-parts/template-part', 'pubmedio5' );
        ?> 

        <?php 
            get_template_part( 'template-parts/template-part', 'estilodevida' );
        ?>
  
    </div>
</div>            