<?php 
/*
Template Name: Homepage
*/
get_header(); 
?>


<?php
    $plantilla = get_field( 'seleccionar_plantilla' );
    
    if($plantilla == "Normal" || !$plantilla)
    {
        get_template_part( 'page-templates/template-part', 'normal' );
    }
    elseif($plantilla == "Intermedia"){
        get_template_part( 'page-templates/template-part', 'intermedia' );
    }
    elseif($plantilla == "Importante"){
        get_template_part( 'page-templates/template-part', 'importante' );
    }
    else
    {
        get_template_part( 'page-templates/template-part', 'normal' );
    }
?>

<?php get_footer(); ?>
