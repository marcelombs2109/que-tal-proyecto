<div class="data-sidebar">
<?php

$postype_name=get_post_type($post->ID);
$mypost_id = $post->ID;

//echo get_post_type($post->ID);

if($postype_name != "reporte_especial")
{
    if($postype_name == "noticias")
    {
        $first_category = wp_get_post_terms( $post->ID, 'categoria_noticias' )[0]->name;
        if(isset($first_category) && $first_category!="")
        {
            $args = array(
                'post_type' => 'noticias',
                'posts_per_page'=>5,
                'post__not_in' => array($post->ID),
                'tax_query' => array(
                    array(
                        'taxonomy' => 'categoria_noticias',
                        'field'    => 'slug',
                        'terms'    => $first_category,
                    ),
                ),
            );
        }
        else{
            $args = array(
                'post_type' => $postype_name,
                'posts_per_page'=>5,
                'post__not_in' => array($post->ID),
            );
        }
    }
    elseif($postype_name == "estilo_de_vida")
    {
        $first_category = wp_get_post_terms( $post->ID, 'categoria_estilo_de_vida' )[0]->name;
        if(isset($first_category) && $first_category!="")
        {
            $args = array(
                'post_type' => 'estilo_de_vida',
                'posts_per_page'=>5,
                'post__not_in' => array($post->ID),
                'tax_query' => array(
                    array(
                        'taxonomy' => 'categoria_estilo_de_vida',
                        'field'    => 'slug',
                        'terms'    => $first_category,
                    ),
                ),
            );
        }
        else{
            $args = array(
                'post_type' => $postype_name,
                'posts_per_page'=>5,
                'post__not_in' => array($post->ID),
            );
        }
    }
    else{
        $args = array(
            'post_type' => $postype_name,
            'posts_per_page'=>5,
            'post__not_in' => array($post->ID),
        );
    }

    $my_query = new WP_Query($args);
    if( $my_query->have_posts() ) 
    {
    ?>
    <div class="contenedor-noticias-no-destacadas sidebar-posts">
        <h4 class="h4-title-category-red">
            <span>
                <?php
                    $pt = get_post_type_object( $postype_name );
                    //echo $pt->label;
                    

                    if($pt->labels->name)
                    {
                        echo $pt->labels->name;
                    }
                    else
                    {
                        echo "Relacionadas";
                    }

                    //echo $pt->labels->singular_name;
                ?>
            </span>
        </h4>
        <div class="noticias-no-destacadas">
            <?php
                while ($my_query->have_posts())
                {
                    $my_query->the_post();
                    $post_id= $my_query->get_the_ID() ;
            ?>
                <div class="item item-flex">
                    <div class="imagen-no-destacada">
                        <a href="<?=get_the_permalink();?>">
                            <?php
                                
                                
                                if($postype_name == "analisis")
                                {
                                    if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-85x85')){
                                        $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-85x85');
                                    }
                                    else{
                                        $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                                    } 
                                }
                                else{
                                    if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-95x75')){
                                        $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-95x75');
                                    }
                                    else{
                                        $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                                    }
                                }
                                
                            ?>
                            <img src="<?= $imagen ?>" <?php if($postype_name == "analisis"){echo 'class="img-redonda"';}?>/>
                        </a>
                    </div>
                    <div class="text-no-destacada">
                        <h5>
                            <a href="<?=get_the_permalink();?>">
                            <?=get_the_title();?>
                            </a>
                        </h5>
                        <div class="date-item">
                            <span>
                                <i class="far fa-clock"></i>
                                <?=get_the_date('d/F/Y');?>
                            </span>
                        </div>
                    </div>
                </div>
            <?php
                }
                wp_reset_query();
            ?>    
        </div>  
    </div>
<?php
    }
}
else
{

    if(get_field("reportes_relacionados","options"))
    {
        if( have_rows("reportes_relacionados","options") )
        {
?>
        <div class="contenedor-noticias-no-destacadas sidebar-posts">
            <h4 class="h4-title-category-red">
                <span>
                    <?php
                        $pt = get_post_type_object( $postype_name );
                            
                        if($pt->labels->name)
                        {
                            echo $pt->labels->name;
                        }
                        else
                        {
                            echo "Relacionadas";
                        }
                    ?>
                </span>
            </h4>
            <div class="noticias-no-destacadas">
<?php
            $j=1;
            while( have_rows("reportes_relacionados","options") ) 
            {
                the_row();
                $reporte_relacionado = get_sub_field('reporte_relacionado');

                if($j<=2)
                {
                    if( $reporte_relacionado )
                    {
                        foreach( $reporte_relacionado as $post )
                        {
                            setup_postdata($post);
                            $post_id = $post->ID;
                        ?>

                        <?php
                            if($mypost_id != $post_id)
                            {
                        ?>
                            <div class="item item-flex">
                                <div class="imagen-no-destacada">
                                    <a href="<?=get_the_permalink();?>">
                                        <?php
                                            if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-95x75')){
                                                $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-95x75');
                                            }
                                            else{
                                                $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                                            }
                                        ?>
                                        <img src="<?= $imagen ?>">
                                    </a>
                                </div>
                                <div class="text-no-destacada">
                                    <h5>
                                        <a href="<?=get_the_permalink();?>">
                                        <?=get_the_title();?>
                                        </a>
                                    </h5>
                                    <div class="date-item">
                                        <span>
                                            <i class="far fa-clock"></i>
                                            <?=get_the_date('d/F/Y');?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        <?php
                            }
                        
                        ?>
                            
                        <?php
                        }
                        wp_reset_postdata();
                    }
                }
                if($j>2)
                {
                    $j=0; 
                }
                $j++;
            }
?>
            </div>
        </div>
<?php

        }
    }

}


?>


<?php
    if(get_field("publicidad_8","options"))
    {
        $url=get_field("publicidad_8","options");
        $link=get_field("link_publicidad_8","options");
?>
    <div class="pub-sidebar">
        <a href="<?=$link;?>" target="_blank">
                <img src="<?=$url;?>">
        </a> 
    </div>
<?php
    }
?>

</div>


<!--
<div class="contenedor-noticias-no-destacadas sidebar-posts">
    <h4 class="h4-title-category-black">
        <span>
            Empresas
        </span>
    </h4>
    <div class="noticias-no-destacadas">
        <div class="item item-flex">
            <div class="imagen-no-destacada">
                <a href="#">
                    <img src="<?= get_template_directory_uri() . '/assets/img/noticia1.jpeg' ?>">
                </a>
            </div>
            <div class="text-no-destacada">
                <h5>
                    <a href="#">
                    Arias informa que heredó “10 problemas serios” de la gestión anterior
                    </a>
                </h5>
                <div class="date-item">
                    <span>
                        <i class="far fa-clock"></i>
                        22/junio/2021
                    </span>
                </div>
            </div>
        </div>    
    </div>  
</div>
-->

