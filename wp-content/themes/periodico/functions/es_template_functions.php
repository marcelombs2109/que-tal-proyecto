<?php

add_action ('wp_enqueue_scripts','es_wp_enqueue_scripts');

function es_wp_enqueue_scripts()
{
    //CSS
    wp_enqueue_style( 'csstheme', get_stylesheet_directory_uri() . '/style.css', array(), 'version2021.v4', false );
    wp_enqueue_style( 'csstheme-sass', get_stylesheet_directory_uri() . '/css/style.css', array(), 'version2021.v4', false );
}

