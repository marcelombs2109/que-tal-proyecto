<?php 
get_header(); 
?>

<?php
if (have_posts()) {
  the_post();
  $post_id = $post->ID;
  ?>

<div class="page-interior">
    <div class="container">

        <div class="row">
            <div class="col-md-24">
                <div class="socialmedia-rutas">
                    <div class="rutas">
                    <?php if(function_exists('bcn_display'))
                        {
                        bcn_display();
                        }?>
                    </div>
                    <div class="social-media">
                        <span>Comparte</span>
                        <ul class="ul-socialmedia-post">
                            <li>
                              <a href="https://www.facebook.com/sharer/sharer.php?u=<?=get_the_permalink();?>" target="_blank" class="fa">
                                <?=get_svg_file(get_template_directory().'/assets/img/facebook.svg')?>
                              </a>
                            </li>
                            <li>
                              <a href="https://www.twitter.com/home?status=<?=get_the_permalink();?>" target="_blank" class="twi">
                                <?=get_svg_file(get_template_directory().'/assets/img/twitter.svg')?>
                              </a>
                            </li>
                            <li>
                              <a href="https://www.linkedin.com/cws/share?url=<?=get_the_permalink();?>" target="_blank" class="inst">
                                <?=get_svg_file(get_template_directory().'/assets/img/instagram.svg')?>
                              </a>
                            </li>
                            
                          </ul>
                    </div>        
                </div>
                <div class="div-title">
                    <h1>
                        <?=get_the_title();?>
                    </h1>

                    <?php

                      if(get_field("texto_introductorio"))
                      {
                    ?>    
                      <p>
                        <?=get_field("texto_introductorio");?>
                      </p>    
                    <?php    
                      }
                    ?>

                    <div class="date-item">
                        <span>
                            <i class="far fa-clock"></i>
                            <?=get_the_date('d/F/Y');?>
                        </span>
                    </div>    
                </div>        
            </div>    
        </div>    

        <div class="row">
            <div class="col-md-17">
              
                <div class="page-info">
                    <?php
                        the_content();    
                    ?>
                </div>    
                
            </div>
            <div class="col-md-7">
                <?php get_sidebar();?>  
            </div>    
        </div>    
    </div>    
</div>
<?php
    }
?>    


<?php get_footer(); ?>