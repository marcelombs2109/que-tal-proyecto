<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?>> <!--<![endif]-->
  <head>
      
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta charset="UTF-8">

    <title>
        <?php
            wp_title();
        ?>
    </title>

    <?php

    wp_enqueue_style( 'load-fa', 'https://use.fontawesome.com/releases/v5.3.1/css/all.css' );

    wp_enqueue_style('mysliderthemejs', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick-theme.css', false, '1.0', 'all');
    wp_enqueue_style('sliderjs', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.css', false, '1.0', 'all');
    wp_enqueue_script('slick-slider', get_template_directory_uri() . '/bower_components/slick-carousel/slick/slick.min.js', array('jquery'), '1.0', true);
    
    wp_enqueue_script('bootstrap-js',get_template_directory_uri() . '/bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js',array('jquery'),'1.0',true);
    wp_enqueue_script('mijavascript', get_template_directory_uri() . '/assets/js/javascript.js', array('jquery'), 'version2021', true);
    wp_enqueue_script("jquery")
    ?>

    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
  <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
  
<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

<link rel="icon" type="image/ico" href="http://talcual.bo/wp-content/uploads/2021/09/noticias-1.svg">

    <?php wp_head(); ?>
    

  </head>

  <body <?php body_class();?>  data-theme="light_theme">

<?php
if (!is_page_template( 'page-templates/template-comming.php' )) 
{	
?>
    <header class="header">

        <?php 
            get_template_part( 'template-parts/template-part', 'pubheader' );
        ?>    
    
        <div class="container">
            <div class="row">
                <div class="col-md-24">

                    <div class="data-header">
                      <div class="fecha-socialmedia">
                        <p class="fecha">
                          <?=display_date_4();?>
                        </p>
                        <div class="socialmedia">
                          <div class="content-search hidden-xs">
                            <form id='search-form' role='search' method='get' action='/' >
                                <input placeholder="Búsqueda" type='search' value='' name='s'>
                            </form>
                            <a href="#" id="link-search">
                              <?=get_svg_file(get_template_directory().'/assets/img/search.svg')?>
                            </a> 
                          </div>
                          <ul class="ul-socialmedia">
                            <li>
                              <a href="<?=get_field("facebook","options")?>" target="_blank" class="fa">
                                <?=get_svg_file(get_template_directory().'/assets/img/facebook.svg')?>
                              </a>
                            </li>
                            <li>
                              <a href="<?=get_field("twitter","options")?>" target="_blank" class="twi">
                                <?=get_svg_file(get_template_directory().'/assets/img/twitter.svg')?>
                              </a>
                            </li>
                            <li>
                              <a href="<?=get_field("instagram","options")?>" target="_blank" class="inst">
                                <?=get_svg_file(get_template_directory().'/assets/img/instagram.svg')?>
                              </a>
                            </li>
                            
                          </ul>  
                        </div>    
                      </div>  

                      <div class="logo-audio">
                        <div class="columna">  

                        </div>

                        <div class="logo">
                          <a href="<?=get_home_url();?>">
                            <?=get_svg_file(get_template_directory().'/assets/img/logo_svg.svg')?>  
                          </a>
                        </div>
                        <div class="audio">
                          
                        </div>
                        
                      </div>
                      
                    </div>
                </div>
            <div>
        </div>
    </header>

    <div class="bandera hidden-xs"></div>
    <div class="menu-sticky hidden-xs">
      <div class="container">
        <div class="row">
          <div class="col-md-24">
            <div class="content-menu">

              <div class="site-title">
                <div class="logo-fixed">
                  <a href="<?=get_home_url();?>" class="animate__animated animate__zoomIn">
                    <?=get_svg_file(get_template_directory().'/assets/img/logo_svg.svg')?>  
                  </a>
                </div>
              </div>
              <?php
                $header_menu_args = array(
                  'theme_location' => 'menu_principal',
                  'menu'           => 'menu_principal',
                  'menu_id'        => 'menu',
                  'container'       =>''
                );
                wp_nav_menu($header_menu_args);
              ?>

            </div>
          </div>
        </div>  
      </div>  
    </div>                              


<?php
}
?>
    