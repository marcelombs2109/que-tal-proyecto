<?php 
get_header(); 
?>

<div class="page-interior">
    <div class="container">

        <div class="row">
            <div class="col-md-24">
                <div class="socialmedia-rutas">
                    <div class="rutas">
                    <?php if(function_exists('bcn_display'))
                        {
                        bcn_display();
                        }?>
                    </div>        
                </div>
                <div class="div-title-archive">
                    <h1>

                        <?php
                            $title  = __( 'Archives' );
                            $prefix = '';
                        
                            if ( is_category() ) {
                                $title  = single_cat_title( '', false );
                                $prefix = _x( 'Category:', 'category archive title prefix' );
                            } elseif ( is_tag() ) {
                                $title  = single_tag_title( '', false );
                                $prefix = _x( 'Tag:', 'tag archive title prefix' );
                            } elseif ( is_author() ) {
                                $title  = get_the_author();
                                $prefix = _x( 'Author:', 'author archive title prefix' );
                            } elseif ( is_year() ) {
                                $title  = get_the_date( _x( 'Y', 'yearly archives date format' ) );
                                $prefix = _x( 'Year:', 'date archive title prefix' );
                            } elseif ( is_month() ) {
                                $title  = get_the_date( _x( 'F Y', 'monthly archives date format' ) );
                                $prefix = _x( 'Month:', 'date archive title prefix' );
                            } elseif ( is_day() ) {
                                $title  = get_the_date( _x( 'F j, Y', 'daily archives date format' ) );
                                $prefix = _x( 'Day:', 'date archive title prefix' );
                            } elseif ( is_tax( 'post_format' ) ) {
                                if ( is_tax( 'post_format', 'post-format-aside' ) ) {
                                    $title = _x( 'Asides', 'post format archive title' );
                                } elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
                                    $title = _x( 'Galleries', 'post format archive title' );
                                } elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
                                    $title = _x( 'Images', 'post format archive title' );
                                } elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
                                    $title = _x( 'Videos', 'post format archive title' );
                                } elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
                                    $title = _x( 'Quotes', 'post format archive title' );
                                } elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
                                    $title = _x( 'Links', 'post format archive title' );
                                } elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
                                    $title = _x( 'Statuses', 'post format archive title' );
                                } elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
                                    $title = _x( 'Audio', 'post format archive title' );
                                } elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
                                    $title = _x( 'Chats', 'post format archive title' );
                                }
                            } elseif ( is_post_type_archive() ) {
                                $title  = post_type_archive_title( '', false );
                                $prefix = _x( 'Archives:', 'post type archive title prefix' );
                            } elseif ( is_tax() ) {
                                $queried_object = get_queried_object();
                                if ( $queried_object ) {
                                    $tax    = get_taxonomy( $queried_object->taxonomy );
                                    $title  = single_term_title( '', false );
                                    $prefix = sprintf(
                                        /* translators: %s: Taxonomy singular name. */
                                        _x( '%s:', 'taxonomy term archive title prefix' ),
                                        $tax->labels->singular_name
                                    );
                                }
                            }
                            echo $title;
                        ?>
                        
                    </h1>    
                </div>        
            </div>    
        </div>    

        <div class="row">
            <div class="col-md-17">
                <div class="page-listado">  

                    <?php
                        while (have_posts()) {	
                        the_post();
                    ?>

                    <div class="item-post-element">
                        <div class="text-item-post-element">
                            <h4>
                                <a href="<?= get_the_permalink();?>">
                                        <?=get_the_title();?>
                                </a>
                            </h4> 
                            <p>
                                <?=getWordsExcerpt30(get_the_excerpt());?> 
                            </p>
                            <div class="date-item">
                                <span>
                                    <i class="far fa-clock"></i>
                                    <?=get_the_date('d/F/Y');?>
                                </span>
                            </div>    
                        </div>
                        
                        <div class="image-item-post-element hidden-xs">

                            <?php
                            
                            if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-170x113')){
                                $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-170x113');
                            }
                            else{
                                $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                            }
                            
                            ?>
                            <a href="<?= get_the_permalink();?>">
                                <img src="<?= $imagen ?>">
                            </a>
                        </div>                   
                    </div>
                    
                    <?php
                        }
                        wp_reset_query();
                    ?>
                 
                </div>            

            </div>
            <div class="col-md-7">
                <?php get_sidebar();?>  
            </div>    
        </div>    
    </div>    
</div>
    
<?php get_footer(); ?>