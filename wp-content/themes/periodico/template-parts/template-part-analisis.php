<div class="contenedor-noticias-no-destacadas">
    <h4 class="h4-title-category-black">
        <span>
        Análisis
        </span>
    </h4>
    <div class="noticias-no-destacadas">

        <?php
            $args = array(
                'posts_per_page' => '4',
                'post_type' => 'analisis',
                'orderby'=>'date', 
                'order'=>'desc',
            );
            $query = new WP_Query($args);
        ?>

        <?php
            $i=1;
            while ($query->have_posts()) 
            {
                $query->the_post();
                $post_id= $query->get_the_ID() ;
        ?> 

            <div class="item item-flex">

                <?php
                    
                if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-85x85')){
                    $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-85x85');
                }
                else{
                    $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                }
                
                ?>

                <div class="imagen-no-destacada">
                    <a href="<?= get_the_permalink();?>">
                        <img src="<?= $imagen ?>" class="img-redonda">
                    </a>
                </div>
                <div class="text-no-destacada" style="padding-top:7px;">
                    <h5>
                        <a href="<?= get_the_permalink();?>">
                            <?=get_the_title();?>
                        </a>    
                    </h5>
                    <div class="date-item">
                        <span style="margin-bottom:5px;">
                            Por: <?=get_field("autor")?>
                        </span><br>    
                        <span>
                            <i class="far fa-clock"></i>
                            <?=get_the_date('d/F/Y');?>
                        </span>
                    </div>
                </div>
            </div>

        <?php
            }    
            wp_reset_query();
        ?>        
    </div>  
</div>