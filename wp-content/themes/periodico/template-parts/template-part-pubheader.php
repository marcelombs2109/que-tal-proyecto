<?php
    if(get_field("publicidad_1","options"))
    {
        $url=get_field("publicidad_1","options");
        $link=get_field("link_publicidad_1","options");
?>

<div class="content-pub-header">
    <div class="container">
        <div class="row">
            <div class="col-md-24">
                <div class="pub-header">
                    <a href="<?=$link;?>" target="_blank">
                        <img src="<?=$url;?>">
                    </a>  
                </div>  
            </div>  
        </div>  
    </div>
</div>

<?php
    }
?>