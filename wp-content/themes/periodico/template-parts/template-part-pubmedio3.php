
<?php
    if(get_field("publicidad_4","options") || get_field("publicidad_5","options"))
    {
?>
<div class="row">
    <div class="col-md-24">
        <div class="publicidad-home-vertical">

            <?php
                if(get_field("publicidad_4","options"))
                {
                    $url4=get_field("publicidad_4","options");
                    $link4=get_field("link_publicidad_4","options");
            ?>
                <div class="pub-medio ">
                    <a href="<?=$link4;?>" target="_blank">
                        <img src="<?=$url4;?>">
                    </a> 
                </div>
            <?php
                }
            ?>

            <?php
                if(get_field("publicidad_5","options"))
                {
                    $url5=get_field("publicidad_5","options");
                    $link5=get_field("link_publicidad_5","options");
            ?>

                <div class="pub-medio ">
                    <a href="<?=$link5;?>" target="_blank">
                        <img src="<?=$url5;?>">
                    </a> 
                </div>

            <?php
                }
            ?>    

        </div>  
    </div>  
</div>

<?php
    }
?>