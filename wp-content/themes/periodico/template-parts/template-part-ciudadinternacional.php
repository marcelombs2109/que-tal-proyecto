<div class="row">
    <div class="col-md-24">
        <div class="divisor-intermedio divisor-margin">
            <div class="columna-1">
                <?php 
                    get_template_part( 'template-parts/template-part', 'ciudad' );
                ?>
                
                <?php 
                    get_template_part( 'template-parts/template-part', 'internacional' );
                ?>
                
            </div>
            <div class="columna-2 hidden-sm">
                
                <?php 
                    get_template_part( 'template-parts/template-part', 'pubmedio3' );
                ?>
            </div>
        </div>        
    </div>
</div>