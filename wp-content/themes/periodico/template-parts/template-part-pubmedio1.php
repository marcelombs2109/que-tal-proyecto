<?php
    if(get_field("publicidad_2","options"))
    {
        $url=get_field("publicidad_2","options");
        $link=get_field("link_publicidad_2","options");
?>
<div class="row">
    <div class="col-md-24">
        <div class="pub-medio">
            <a href="<?=$link;?>" target="_blank">
                <img src="<?=$url;?>">
            </a>  
        </div>  
    </div>  
</div>
<?php
    }
?>