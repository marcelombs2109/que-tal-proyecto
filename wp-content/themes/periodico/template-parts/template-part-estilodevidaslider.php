<div class="cuadro">

    <?php
        $args = array(
            'posts_per_page' => '1',
            'post_type' => 'estilo_de_vida',
            'orderby'=>'date', 
            'order'=>'desc',
            'tax_query' => array(
                array(
                    'taxonomy' => 'categoria_estilo_de_vida',
                    'field'    => 'slug',
                    'terms'    => 'entre-nos',
                ),
            ),
        );
        $query = new WP_Query($args);
    ?>

    <?php
        while ($query->have_posts()) 
        {
            $query->the_post();
            $post_id= $query->get_the_ID() ;
    ?>

    <h3>
        <span>
        Entre nos...
        </span>
    </h3>
    <div class="slider-cuadro">
        <?php
            if( have_rows('fotografias') )
            {
                
                while( have_rows('fotografias') ) 
                {
                    the_row();
                    $fotografia = get_sub_field('fotografia');
        ?>   
        
        <div class="item">
            <div>
                <div class="imagen">
                    <img src="<?=$fotografia?>">
                </div>
            </div>    
        </div>

        <?php
                }
            }
        ?>    
    </div>    
    <h4>
        <a href="<?= get_the_permalink();?>">
            <?=get_field("nombre_del_personaje")?>
        </a>
    </h4>
    <p>
        <?=get_field("frase")?>  
    </p>

    <?php
        }    
        wp_reset_query();
    ?>        

</div>