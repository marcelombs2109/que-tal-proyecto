<div class="row">
    <div class="col-md-24">

        <h2 class="h2-home">
            <span>
            Estilo de Vida
            </span>    
        </h2>                    

        <div class="divisor-4-columnas-full divisor-margin">

            <div class="columna">
                
                <div class="contenedor-noticias-no-destacadas">

                    <h4 class="h4-title-category-red">
                        <span>
                            En el consultorio
                        </span>
                    </h4>
                    
                    <div class="noticias-no-destacadas">

                        <?php
                            $args = array(
                                'posts_per_page' => '3',
                                'post_type' => 'estilo_de_vida',
                                'orderby'=>'date', 
                                'order'=>'desc',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'categoria_estilo_de_vida',
                                        'field'    => 'slug',
                                        'terms'    => 'en-el-consultorio',
                                    ),
                                ),
                            );
                            $query = new WP_Query($args);
                        ?>

                        <?php
                            $i=1;
                            while ($query->have_posts()) 
                            {
                                $query->the_post();
                                $post_id= $query->get_the_ID() ;
                        ?>
                            <div class="item">

                                <?php
                                    if($i==1)
                                    {
                                ?>

                                <?php
                            
                                if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-280x185')){
                                    $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-280x185');
                                }
                                else{
                                    $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                                }
                                
                                ?>        

                                <div class="imagen-no-destacada">
                                    <a href="<?=get_the_permalink();?>">
                                        <img src="<?=$imagen;?>">
                                    </a>
                                </div>        
                                <?php
                                    }
                                ?>    
 
                                <h5>
                                    <a href="<?=get_the_permalink();?>">
                                    <?=get_the_title();?>
                                    </a>
                                </h5>
                                <div class="date-item">
                                    <span>
                                        <i class="far fa-clock"></i>
                                        <?=get_the_date('d/F/Y');?>
                                    </span>
                                </div>
                            </div>

                        <?php
                            $i=0;
                            }    
                            wp_reset_query();
                        ?>                    
    
                    </div>  
                </div>

            </div>

            <div class="columna">
                
                <div class="contenedor-noticias-no-destacadas">

                    <h4 class="h4-title-category-black">
                        <span>
                            Buen provecho
                        </span>
                    </h4>
                    
                    <div class="noticias-no-destacadas">
                        <?php
                            $args = array(
                                'posts_per_page' => '3',
                                'post_type' => 'estilo_de_vida',
                                'orderby'=>'date', 
                                'order'=>'desc',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'categoria_estilo_de_vida',
                                        'field'    => 'slug',
                                        'terms'    => 'buen-provecho',
                                    ),
                                ),
                            );
                            $query = new WP_Query($args);
                        ?>

                        <?php
                            $i=1;
                            while ($query->have_posts()) 
                            {
                                $query->the_post();
                                $post_id= $query->get_the_ID() ;
                        ?>
                            <div class="item">

                                <?php
                                    if($i==1)
                                    {
                                ?>
                                
                                <?php
                            
                                if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-280x185')){
                                    $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-280x185');
                                }
                                else{
                                    $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                                }
                                
                                ?>        

                                <div class="imagen-no-destacada">
                                    <a href="<?=get_the_permalink();?>">
                                        <img src="<?=$imagen;?>">
                                    </a>
                                </div>        
                                <?php
                                    }
                                ?>    
 
                                <h5>
                                    <a href="<?=get_the_permalink();?>">
                                    <?=get_the_title();?>
                                    </a>
                                </h5>
                                <div class="date-item">
                                    <span>
                                        <i class="far fa-clock"></i>
                                        <?=get_the_date('d/F/Y');?>
                                    </span>
                                </div>
                            </div>

                        <?php
                            $i=0;
                            }    
                            wp_reset_query();
                        ?>    
                    </div>  
                </div>

            </div>

            <div class="columna">

                <div class="contenedor-noticias-no-destacadas">
                    
                    <h4 class="h4-title-category-red">
                        <span>
                            Vinos y licores
                        </span>
                    </h4>

                    <div class="noticias-no-destacadas">
                        <?php
                            $args = array(
                                'posts_per_page' => '3',
                                'post_type' => 'estilo_de_vida',
                                'orderby'=>'date', 
                                'order'=>'desc',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'categoria_estilo_de_vida',
                                        'field'    => 'slug',
                                        'terms'    => 'vinos-y-licores',
                                    ),
                                ),
                            );
                            $query = new WP_Query($args);
                        ?>

                        <?php
                            $i=1;
                            while ($query->have_posts()) 
                            {
                                $query->the_post();
                                $post_id= $query->get_the_ID() ;
                        ?>
                            <div class="item">

                                <?php
                                    if($i==1)
                                    {
                                ?>
                                
                                <?php
                            
                                if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-280x185')){
                                    $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-280x185');
                                }
                                else{
                                    $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                                }
                                
                                ?>        

                                <div class="imagen-no-destacada">
                                    <a href="<?=get_the_permalink();?>">
                                        <img src="<?=$imagen;?>">
                                    </a>
                                </div>        
                                <?php
                                    }
                                ?>    
 
                                <h5>
                                    <a href="<?=get_the_permalink();?>">
                                    <?=get_the_title();?>
                                    </a>
                                </h5>
                                <div class="date-item">
                                    <span>
                                        <i class="far fa-clock"></i>
                                        <?=get_the_date('d/F/Y');?>
                                    </span>
                                </div>
                            </div>

                        <?php
                            $i=0;
                            }    
                            wp_reset_query();
                        ?>    
                    </div>  
                </div>

            </div>
            <div class="columna">

                <div class="contenedor-noticias-no-destacadas">
                    
                    <h4 class="h4-title-category-black">
                        <span>
                            Perros y gatos
                        </span>
                    </h4>

                    <div class="noticias-no-destacadas">
                        <?php
                            $args = array(
                                'posts_per_page' => '3',
                                'post_type' => 'estilo_de_vida',
                                'orderby'=>'date', 
                                'order'=>'desc',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'categoria_estilo_de_vida',
                                        'field'    => 'slug',
                                        'terms'    => 'perros-y-gatos',
                                    ),
                                ),
                            );
                            $query = new WP_Query($args);
                        ?>

                        <?php
                            $i=1;
                            while ($query->have_posts()) 
                            {
                                $query->the_post();
                                $post_id= $query->get_the_ID() ;
                        ?>
                            <div class="item">

                                <?php
                                    if($i==1)
                                    {
                                ?>
                                
                                <?php
                            
                                if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-280x185')){
                                    $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-280x185');
                                }
                                else{
                                    $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                                }
                                
                                ?>        

                                <div class="imagen-no-destacada">
                                    <a href="<?=get_the_permalink();?>">
                                        <img src="<?=$imagen;?>">
                                    </a>
                                </div>        
                                <?php
                                    }
                                ?>    
 
                                <h5>
                                    <a href="<?=get_the_permalink();?>">
                                    <?=get_the_title();?>
                                    </a>
                                </h5>
                                <div class="date-item">
                                    <span>
                                        <i class="far fa-clock"></i>
                                        <?=get_the_date('d/F/Y');?>
                                    </span>
                                </div>
                            </div>

                        <?php
                            $i=0;
                            }    
                            wp_reset_query();
                        ?>    
                    </div>  
                </div>

            </div>    
        </div>     
    </div>    
</div>