<?php
    if(get_field("publicidad_7","options"))
    {
        $url=get_field("publicidad_7","options");
        $link=get_field("link_publicidad_7","options");
?>
<div class="row">
    <div class="col-md-24">
        <div class="pub-medio">
            <a href="<?=$link;?>" target="_blank">
                <img src="<?=$url;?>">
            </a>  
        </div>  
    </div>  
</div>
<?php
    }
?>