
<div class="col-md-24">
    
    <h4 class="reportaje">
        <span>Reporte Especial</span>
    </h4>


    <div class="data-reportaje">

        <?php
        
            if(get_field("reportes_relacionados","options"))
            {
                if( have_rows("reportes_relacionados","options") )
                {
                    $j=1;
                    while( have_rows("reportes_relacionados","options") ) 
                    {
                        the_row();
                        $reporte_relacionado = get_sub_field('reporte_relacionado');
                        if($j==1)
                        {    
                            if( $reporte_relacionado )
                            {
                                $i=1;
                                foreach( $reporte_relacionado as $post )
                                {
                                    setup_postdata($post);
                                    $post_id = $post->ID;
                                ?>

                                    <div class="item">
                                        <?php
                                        
                                        if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-230x130')){
                                            $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-230x130');
                                        }
                                        else{
                                            $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                                        }
                                        
                                        ?>    
                                        <div class="image-item">
                                            <a href="<?= get_the_permalink();?>">
                                                <img src="<?= $imagen ?>">
                                            </a>
                                        </div>
                                        <div class="text-item">
                                            <h5>
                                                <a href="<?= get_the_permalink();?>">
                                                    <?=get_the_title();?>
                                                </a>    
                                            </h5>
                                            <?php
                                                if($i==1)
                                                {
                                            ?>
                                            <p class="hidden-sm">
                                                <?=getWordsExcerpt20(get_the_excerpt());?>
                                            </p>
                                            <?php
                                                $i=0;
                                                }
                                            ?>
                                                    
                                        </div>        
                                    </div>    

                                <?php
                                    
                                }
                                wp_reset_postdata();
                            }
                        }
                        if($j==1)
                        {
                            $j=0; 
                        }
                    }
                }
            }
        ?>
            
    </div>    
</div>    
        