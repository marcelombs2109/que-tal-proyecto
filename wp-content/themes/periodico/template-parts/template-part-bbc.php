<?php
    $args = array(
        'posts_per_page' => '1',
        'post_type' => 'bbc_mundo',
        'orderby'=>'date', 
        'order'=>'desc',
    );
    $query = new WP_Query($args);
?>


<?php
    $i=1;
    while ($query->have_posts()) 
    {
        $query->the_post();
        $post_id= $query->get_the_ID() ;
?>
<div class="cuadro-bbc">
    <div class="header-bbc">
        <img src="<?= get_template_directory_uri() . '/assets/img/bbc_logo.jpg' ?>">
    </div>

    <?php
        if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-314x170')){
            $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-314x170');
        }
        else{
            $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
        }
    ?>    

    <div class="content-bbc">
        <a href="<?=get_the_permalink();?>">
            <img src="<?=$imagen;?>">
        </a>
    </div>
    <div class="footer-bcc">
        <div class="date-item">
            <span>
                <i class="far fa-clock"></i>
                <?=get_the_date('d/F/Y');?>
            </span>
        </div>
        <h5>
            <a href="<?=get_the_permalink();?>">
            <?=get_the_title();?>
            </a>
        </h5>    
    </div>            
</div>

<?php
    }
    wp_reset_query();
?>