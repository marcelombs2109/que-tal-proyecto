
<div class="noticias-columnas">

    <?php
        $j=1;
        $args = array(
            'posts_per_page' => '3',
            'post_type' => 'noticias',
            'orderby'=>'date', 
            'order'=>'desc',
            'tax_query' => array(
                array(
                    'taxonomy' => 'destacados',
                    'field'    => 'slug',
                    'terms'    => 'subapertura',
                ),
            ),
        );
        $query = new WP_Query($args);
    ?>

    <?php
        
        while ($query->have_posts()) 
        {
            $query->the_post();
            $post_id= $query->get_the_ID() ;
    ?>
        <?php
            if($j==3)
            {
        ?>
        <div class="item hidden-sm">
        <?php
            }
            else{
        ?>
        <div class="item">
        <?php        
            }
        ?>
            <?php
        
            if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-252x250')){
                $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-252x250');
            }
            else{
                $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
            }
            
            ?> 
            
            <div class="imagen-item">
                <a href="<?=get_the_permalink();?>">
                <img src="<?=$imagen;?>">
                </a>
            </div>
            <div class="text-item">
                <h5>
                    <a href="<?=get_the_permalink();?>">
                    <?=get_the_title();?>
                    </a>
                </h5>
                <p>
                <?=getWordsExcerpt13(get_the_excerpt());?>
                
                </p>
                <div class="date-item">
                    <span>
                        <i class="far fa-clock"></i>
                        <?=get_the_date('d/F/Y');?>
                    </span>
                </div>
            </div>

        </div>

    <?php
        $j++;
        }    
        wp_reset_query();
    ?>    

    </div>    