<div class="row">
    <div class="col-md-24">
        <div class="divisor-intermedio divisor-margin">
            <div class="columna-1">
                <h4 class="h4-title-category-red h4-title-category-50">
                    <span>
                        Enfoques y Entrevistas
                    </span>
                </h4>
                <div class="divisor-2-columnas">
                    <div class="columna">
                        
                        <div class="noticias-no-destacadas">

                            <?php
                                $args = array(
                                    'posts_per_page' => '1',
                                    'post_type' => 'enfoques_entrevistas',
                                    'orderby'=>'date', 
                                    'order'=>'desc',
                                );
                                $query = new WP_Query($args);
                            ?>

                            <?php
                                
                                $post_id=-1;
                                while ($query->have_posts()) 
                                {
                                    $query->the_post();
                                    $post_id= get_the_ID();
                            ?>
                            
                            <div class="item">

                                <?php
                                
                                if(get_the_post_thumbnail($post_id,'thumb-noticias-sticky-268x204')){
                                    $imagen = get_the_post_thumbnail_url($post_id,'thumb-noticias-sticky-268x204');
                                }
                                else{
                                    $imagen = get_template_directory_uri() . '/assets/img/img-4-home.png';
                                }
                                
                                ?>    

                                <div class="imagen-no-destacada">
                                    <a href="<?=get_the_permalink();?>">
                                        <img src="<?= $imagen ?>" >
                                    </a>
                                </div>    
                                <h4>
                                    <a href="<?=get_the_permalink();?>">
                                        <?=get_the_title();?>
                                        
                                    </a>
                                </h4>
                                <p>
                                <?=getWordsExcerpt30(get_the_excerpt());?>
                                </p>
                                <div class="date-item">
                                    <span>
                                        <i class="far fa-clock"></i>
                                        <?=get_the_date('d/F/Y');?>
                                    </span>
                                </div>
                                
                            </div> 
                            <?php
                                }    
                                wp_reset_query();
                            ?>
                            
                        </div>
                    </div>
                    <div class="columna">
                        <div class="noticias-no-destacadas">
                            
                            <?php
                            
                                $args = array(
                                    'posts_per_page' => '3',
                                    'post_type' => 'enfoques_entrevistas',
                                    'post__not_in' => array($post_id),
                                    'orderby'=>'date', 
                                    'order'=>'desc',
                                );
                                $query = new WP_Query($args);
                            ?>    

                            <?php
                                while ($query->have_posts()) 
                                {
                                    $query->the_post();
                                    $post_id_= $query->get_the_ID() ;
                            ?>  
                            
                            <div class="item">
                                <h5>
                                    <a href="<?=get_the_permalink();?>">
                                        <?=get_the_title();?>
                                    </a>
                                </h5>
                                <p>
                                <?=getWordsExcerpt13(get_the_excerpt());?> 
                                </p>
                                <div class="date-item">
                                    <span>
                                        <i class="far fa-clock"></i>
                                        <?=get_the_date('d/F/Y');?>
                                    </span>
                                </div>
                            </div>
                            
                            <?php
                                }    
                                wp_reset_query();
                            ?> 

                        </div>
                    </div>    
                </div>


            </div>
            <div class="columna-2">

                <?php
                    $plantilla = get_field( 'seleccionar_plantilla' );
                    
                    if($plantilla == "Normal" || !$plantilla)
                    {
                        get_template_part( 'template-parts/template-part', 'bbc' );
                    }
                    elseif($plantilla == "Intermedia"){
                        get_template_part( 'template-parts/template-part', 'analisis' );
                    }
                    elseif($plantilla == "Importante"){
                        get_template_part( 'template-parts/template-part', 'analisis' );
                    }
                    else
                    {
                        get_template_part( 'page-templates/template-part', 'normal' );
                    }
                ?>
                
            </div>
        </div>        
    </div>
</div>