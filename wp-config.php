<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'periodico' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '123456' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '2Z77{>.JtkFA2[TqCGgg`t;tbJ,c{F7d/<On=%WW[$qU}468S/^(jc7t(HuII gj' );
define( 'SECURE_AUTH_KEY',  'y;^<M}d,HG!V/yD(8ep%fTKr h%5>(Zu ZU2?t(`PeSofV|ZvR)k/PnV0yWuV2H#' );
define( 'LOGGED_IN_KEY',    'N ycc]WS&`V=Atk),lU?`,i[){$*JNqJ;,1J`&Z>0u%4A`+SU>DJ&Aza (DF57_L' );
define( 'NONCE_KEY',        'O{NB05HIy)wS|`TKW<n(M7U[s9xVMe%Q/B?J1rGd?f6CB&0~>G4=kpS_*O?lDA_Y' );
define( 'AUTH_SALT',        '4k=%)>O(4l X9=EB&vqlQ0j&j938zV0S&6^b?G(otLG(RfbE)?];g_@$jmY^xaaZ' );
define( 'SECURE_AUTH_SALT', 'L}Af?3XY,#^S`FPEPE t4hEBnLe+i(ub_0($Gq189jS[9Yd6h3@?D&q@Qw3?=(}y' );
define( 'LOGGED_IN_SALT',   'OMnH,!QsCe2*_6!ta>eEuNjODQ;Pulz;:m.b5G[E[;`?/g.@b oDaPs/d+:hG7|t' );
define( 'NONCE_SALT',       'M?iJcYWvM8J78#LH.ObR-Q4kj `dfi =4X@kL!09Pyq}_0&KvmCQ?,APxwNM_#cb' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
