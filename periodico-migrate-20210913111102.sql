# WordPress MySQL database migration
#
# Generated: Monday 13. September 2021 11:11 UTC
# Hostname: localhost
# Database: `periodico`
# URL: //talcualstaging.ml
# Path: /home/marcelo/projects/periodico
# Tables: wp_commentmeta, wp_comments, wp_links, wp_options, wp_postmeta, wp_posts, wp_term_relationships, wp_term_taxonomy, wp_termmeta, wp_terms, wp_usermeta, wp_users
# Table Prefix: wp_
# Post Types: revision, acf-field, acf-field-group, attachment, estilo_de_vida, nav_menu_item, noticia, noticias, page, post, reportaje_especial, reporte_especial, wpcf7_contact_form
# Protocol: http
# Multisite: false
# Subsite Export: false
# --------------------------------------------------------

/*!40101 SET NAMES utf8mb4 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wp_commentmeta`
#

DROP TABLE IF EXISTS `wp_commentmeta`;


#
# Table structure of table `wp_commentmeta`
#

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wp_commentmeta`
#

#
# End of data contents of table `wp_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_comments`
#

DROP TABLE IF EXISTS `wp_comments`;


#
# Table structure of table `wp_comments`
#

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wp_comments`
#
INSERT INTO `wp_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Un comentarista de WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2021-08-13 02:46:22', '2021-08-13 02:46:22', 'Hola, esto es un comentario.\nPara empezar a moderar, editar y borrar comentarios, por favor, visita la pantalla de comentarios en el escritorio.\nLos avatares de los comentaristas provienen de <a href="https://gravatar.com">Gravatar</a>.', 0, '1', '', 'comment', 0, 0) ;

#
# End of data contents of table `wp_comments`
# --------------------------------------------------------



#
# Delete any existing table `wp_links`
#

DROP TABLE IF EXISTS `wp_links`;


#
# Table structure of table `wp_links`
#

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wp_links`
#

#
# End of data contents of table `wp_links`
# --------------------------------------------------------



#
# Delete any existing table `wp_options`
#

DROP TABLE IF EXISTS `wp_options`;


#
# Table structure of table `wp_options`
#

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`),
  KEY `autoload` (`autoload`)
) ENGINE=InnoDB AUTO_INCREMENT=873 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wp_options`
#
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://talcualstaging.ml', 'yes'),
(2, 'home', 'http://talcualstaging.ml', 'yes'),
(3, 'blogname', 'Periodico', 'yes'),
(4, 'blogdescription', 'Otro sitio realizado con WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'marcelombs2109@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:288:{s:11:"noticias/?$";s:28:"index.php?post_type=noticias";s:41:"noticias/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=noticias&feed=$matches[1]";s:36:"noticias/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=noticias&feed=$matches[1]";s:28:"noticias/page/([0-9]{1,})/?$";s:46:"index.php?post_type=noticias&paged=$matches[1]";s:11:"podcasts/?$";s:28:"index.php?post_type=podcasts";s:41:"podcasts/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=podcasts&feed=$matches[1]";s:36:"podcasts/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=podcasts&feed=$matches[1]";s:28:"podcasts/page/([0-9]{1,})/?$";s:46:"index.php?post_type=podcasts&paged=$matches[1]";s:19:"reporte_especial/?$";s:36:"index.php?post_type=reporte_especial";s:49:"reporte_especial/feed/(feed|rdf|rss|rss2|atom)/?$";s:53:"index.php?post_type=reporte_especial&feed=$matches[1]";s:44:"reporte_especial/(feed|rdf|rss|rss2|atom)/?$";s:53:"index.php?post_type=reporte_especial&feed=$matches[1]";s:36:"reporte_especial/page/([0-9]{1,})/?$";s:54:"index.php?post_type=reporte_especial&paged=$matches[1]";s:11:"analisis/?$";s:28:"index.php?post_type=analisis";s:41:"analisis/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=analisis&feed=$matches[1]";s:36:"analisis/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=analisis&feed=$matches[1]";s:28:"analisis/page/([0-9]{1,})/?$";s:46:"index.php?post_type=analisis&paged=$matches[1]";s:23:"enfoques_entrevistas/?$";s:40:"index.php?post_type=enfoques_entrevistas";s:53:"enfoques_entrevistas/feed/(feed|rdf|rss|rss2|atom)/?$";s:57:"index.php?post_type=enfoques_entrevistas&feed=$matches[1]";s:48:"enfoques_entrevistas/(feed|rdf|rss|rss2|atom)/?$";s:57:"index.php?post_type=enfoques_entrevistas&feed=$matches[1]";s:40:"enfoques_entrevistas/page/([0-9]{1,})/?$";s:58:"index.php?post_type=enfoques_entrevistas&paged=$matches[1]";s:11:"empresas/?$";s:28:"index.php?post_type=empresas";s:41:"empresas/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=empresas&feed=$matches[1]";s:36:"empresas/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=empresas&feed=$matches[1]";s:28:"empresas/page/([0-9]{1,})/?$";s:46:"index.php?post_type=empresas&paged=$matches[1]";s:17:"estilo_de_vida/?$";s:34:"index.php?post_type=estilo_de_vida";s:47:"estilo_de_vida/feed/(feed|rdf|rss|rss2|atom)/?$";s:51:"index.php?post_type=estilo_de_vida&feed=$matches[1]";s:42:"estilo_de_vida/(feed|rdf|rss|rss2|atom)/?$";s:51:"index.php?post_type=estilo_de_vida&feed=$matches[1]";s:34:"estilo_de_vida/page/([0-9]{1,})/?$";s:52:"index.php?post_type=estilo_de_vida&paged=$matches[1]";s:12:"bbc_mundo/?$";s:29:"index.php?post_type=bbc_mundo";s:42:"bbc_mundo/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?post_type=bbc_mundo&feed=$matches[1]";s:37:"bbc_mundo/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?post_type=bbc_mundo&feed=$matches[1]";s:29:"bbc_mundo/page/([0-9]{1,})/?$";s:47:"index.php?post_type=bbc_mundo&paged=$matches[1]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:17:"^wp-sitemap\\.xml$";s:23:"index.php?sitemap=index";s:17:"^wp-sitemap\\.xsl$";s:36:"index.php?sitemap-stylesheet=sitemap";s:23:"^wp-sitemap-index\\.xsl$";s:34:"index.php?sitemap-stylesheet=index";s:48:"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$";s:75:"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]";s:34:"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$";s:47:"index.php?sitemap=$matches[1]&paged=$matches[2]";s:36:"noticias/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:46:"noticias/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:66:"noticias/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"noticias/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"noticias/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:42:"noticias/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:25:"noticias/([^/]+)/embed/?$";s:41:"index.php?noticias=$matches[1]&embed=true";s:29:"noticias/([^/]+)/trackback/?$";s:35:"index.php?noticias=$matches[1]&tb=1";s:49:"noticias/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?noticias=$matches[1]&feed=$matches[2]";s:44:"noticias/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?noticias=$matches[1]&feed=$matches[2]";s:37:"noticias/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?noticias=$matches[1]&paged=$matches[2]";s:44:"noticias/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?noticias=$matches[1]&cpage=$matches[2]";s:33:"noticias/([^/]+)(?:/([0-9]+))?/?$";s:47:"index.php?noticias=$matches[1]&page=$matches[2]";s:25:"noticias/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:35:"noticias/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:55:"noticias/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"noticias/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"noticias/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:31:"noticias/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:36:"podcasts/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:46:"podcasts/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:66:"podcasts/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"podcasts/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"podcasts/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:42:"podcasts/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:25:"podcasts/([^/]+)/embed/?$";s:41:"index.php?podcasts=$matches[1]&embed=true";s:29:"podcasts/([^/]+)/trackback/?$";s:35:"index.php?podcasts=$matches[1]&tb=1";s:49:"podcasts/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?podcasts=$matches[1]&feed=$matches[2]";s:44:"podcasts/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?podcasts=$matches[1]&feed=$matches[2]";s:37:"podcasts/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?podcasts=$matches[1]&paged=$matches[2]";s:44:"podcasts/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?podcasts=$matches[1]&cpage=$matches[2]";s:33:"podcasts/([^/]+)(?:/([0-9]+))?/?$";s:47:"index.php?podcasts=$matches[1]&page=$matches[2]";s:25:"podcasts/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:35:"podcasts/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:55:"podcasts/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"podcasts/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"podcasts/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:31:"podcasts/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:44:"reporte_especial/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:54:"reporte_especial/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:74:"reporte_especial/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:69:"reporte_especial/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:69:"reporte_especial/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:50:"reporte_especial/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:33:"reporte_especial/([^/]+)/embed/?$";s:49:"index.php?reporte_especial=$matches[1]&embed=true";s:37:"reporte_especial/([^/]+)/trackback/?$";s:43:"index.php?reporte_especial=$matches[1]&tb=1";s:57:"reporte_especial/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:55:"index.php?reporte_especial=$matches[1]&feed=$matches[2]";s:52:"reporte_especial/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:55:"index.php?reporte_especial=$matches[1]&feed=$matches[2]";s:45:"reporte_especial/([^/]+)/page/?([0-9]{1,})/?$";s:56:"index.php?reporte_especial=$matches[1]&paged=$matches[2]";s:52:"reporte_especial/([^/]+)/comment-page-([0-9]{1,})/?$";s:56:"index.php?reporte_especial=$matches[1]&cpage=$matches[2]";s:41:"reporte_especial/([^/]+)(?:/([0-9]+))?/?$";s:55:"index.php?reporte_especial=$matches[1]&page=$matches[2]";s:33:"reporte_especial/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:43:"reporte_especial/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:63:"reporte_especial/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"reporte_especial/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"reporte_especial/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:39:"reporte_especial/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:36:"analisis/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:46:"analisis/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:66:"analisis/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"analisis/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"analisis/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:42:"analisis/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:25:"analisis/([^/]+)/embed/?$";s:41:"index.php?analisis=$matches[1]&embed=true";s:29:"analisis/([^/]+)/trackback/?$";s:35:"index.php?analisis=$matches[1]&tb=1";s:49:"analisis/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?analisis=$matches[1]&feed=$matches[2]";s:44:"analisis/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?analisis=$matches[1]&feed=$matches[2]";s:37:"analisis/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?analisis=$matches[1]&paged=$matches[2]";s:44:"analisis/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?analisis=$matches[1]&cpage=$matches[2]";s:33:"analisis/([^/]+)(?:/([0-9]+))?/?$";s:47:"index.php?analisis=$matches[1]&page=$matches[2]";s:25:"analisis/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:35:"analisis/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:55:"analisis/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"analisis/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"analisis/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:31:"analisis/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:48:"enfoques_entrevistas/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:58:"enfoques_entrevistas/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:78:"enfoques_entrevistas/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:73:"enfoques_entrevistas/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:73:"enfoques_entrevistas/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:54:"enfoques_entrevistas/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:37:"enfoques_entrevistas/([^/]+)/embed/?$";s:53:"index.php?enfoques_entrevistas=$matches[1]&embed=true";s:41:"enfoques_entrevistas/([^/]+)/trackback/?$";s:47:"index.php?enfoques_entrevistas=$matches[1]&tb=1";s:61:"enfoques_entrevistas/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:59:"index.php?enfoques_entrevistas=$matches[1]&feed=$matches[2]";s:56:"enfoques_entrevistas/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:59:"index.php?enfoques_entrevistas=$matches[1]&feed=$matches[2]";s:49:"enfoques_entrevistas/([^/]+)/page/?([0-9]{1,})/?$";s:60:"index.php?enfoques_entrevistas=$matches[1]&paged=$matches[2]";s:56:"enfoques_entrevistas/([^/]+)/comment-page-([0-9]{1,})/?$";s:60:"index.php?enfoques_entrevistas=$matches[1]&cpage=$matches[2]";s:45:"enfoques_entrevistas/([^/]+)(?:/([0-9]+))?/?$";s:59:"index.php?enfoques_entrevistas=$matches[1]&page=$matches[2]";s:37:"enfoques_entrevistas/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:47:"enfoques_entrevistas/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:67:"enfoques_entrevistas/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"enfoques_entrevistas/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"enfoques_entrevistas/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:43:"enfoques_entrevistas/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:36:"empresas/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:46:"empresas/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:66:"empresas/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"empresas/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"empresas/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:42:"empresas/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:25:"empresas/([^/]+)/embed/?$";s:41:"index.php?empresas=$matches[1]&embed=true";s:29:"empresas/([^/]+)/trackback/?$";s:35:"index.php?empresas=$matches[1]&tb=1";s:49:"empresas/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?empresas=$matches[1]&feed=$matches[2]";s:44:"empresas/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?empresas=$matches[1]&feed=$matches[2]";s:37:"empresas/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?empresas=$matches[1]&paged=$matches[2]";s:44:"empresas/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?empresas=$matches[1]&cpage=$matches[2]";s:33:"empresas/([^/]+)(?:/([0-9]+))?/?$";s:47:"index.php?empresas=$matches[1]&page=$matches[2]";s:25:"empresas/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:35:"empresas/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:55:"empresas/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"empresas/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"empresas/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:31:"empresas/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:42:"estilo_de_vida/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:52:"estilo_de_vida/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:72:"estilo_de_vida/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:67:"estilo_de_vida/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:67:"estilo_de_vida/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:48:"estilo_de_vida/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:31:"estilo_de_vida/([^/]+)/embed/?$";s:47:"index.php?estilo_de_vida=$matches[1]&embed=true";s:35:"estilo_de_vida/([^/]+)/trackback/?$";s:41:"index.php?estilo_de_vida=$matches[1]&tb=1";s:55:"estilo_de_vida/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:53:"index.php?estilo_de_vida=$matches[1]&feed=$matches[2]";s:50:"estilo_de_vida/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:53:"index.php?estilo_de_vida=$matches[1]&feed=$matches[2]";s:43:"estilo_de_vida/([^/]+)/page/?([0-9]{1,})/?$";s:54:"index.php?estilo_de_vida=$matches[1]&paged=$matches[2]";s:50:"estilo_de_vida/([^/]+)/comment-page-([0-9]{1,})/?$";s:54:"index.php?estilo_de_vida=$matches[1]&cpage=$matches[2]";s:39:"estilo_de_vida/([^/]+)(?:/([0-9]+))?/?$";s:53:"index.php?estilo_de_vida=$matches[1]&page=$matches[2]";s:31:"estilo_de_vida/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:41:"estilo_de_vida/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:61:"estilo_de_vida/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:56:"estilo_de_vida/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:56:"estilo_de_vida/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:37:"estilo_de_vida/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:37:"bbc_mundo/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:47:"bbc_mundo/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:67:"bbc_mundo/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"bbc_mundo/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"bbc_mundo/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:43:"bbc_mundo/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:26:"bbc_mundo/([^/]+)/embed/?$";s:42:"index.php?bbc_mundo=$matches[1]&embed=true";s:30:"bbc_mundo/([^/]+)/trackback/?$";s:36:"index.php?bbc_mundo=$matches[1]&tb=1";s:50:"bbc_mundo/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?bbc_mundo=$matches[1]&feed=$matches[2]";s:45:"bbc_mundo/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?bbc_mundo=$matches[1]&feed=$matches[2]";s:38:"bbc_mundo/([^/]+)/page/?([0-9]{1,})/?$";s:49:"index.php?bbc_mundo=$matches[1]&paged=$matches[2]";s:45:"bbc_mundo/([^/]+)/comment-page-([0-9]{1,})/?$";s:49:"index.php?bbc_mundo=$matches[1]&cpage=$matches[2]";s:34:"bbc_mundo/([^/]+)(?:/([0-9]+))?/?$";s:48:"index.php?bbc_mundo=$matches[1]&page=$matches[2]";s:26:"bbc_mundo/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:36:"bbc_mundo/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:56:"bbc_mundo/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"bbc_mundo/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"bbc_mundo/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:32:"bbc_mundo/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:59:"categoria_noticias/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:57:"index.php?categoria_noticias=$matches[1]&feed=$matches[2]";s:54:"categoria_noticias/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:57:"index.php?categoria_noticias=$matches[1]&feed=$matches[2]";s:35:"categoria_noticias/([^/]+)/embed/?$";s:51:"index.php?categoria_noticias=$matches[1]&embed=true";s:47:"categoria_noticias/([^/]+)/page/?([0-9]{1,})/?$";s:58:"index.php?categoria_noticias=$matches[1]&paged=$matches[2]";s:29:"categoria_noticias/([^/]+)/?$";s:40:"index.php?categoria_noticias=$matches[1]";s:65:"categoria_estilo_de_vida/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:63:"index.php?categoria_estilo_de_vida=$matches[1]&feed=$matches[2]";s:60:"categoria_estilo_de_vida/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:63:"index.php?categoria_estilo_de_vida=$matches[1]&feed=$matches[2]";s:41:"categoria_estilo_de_vida/([^/]+)/embed/?$";s:57:"index.php?categoria_estilo_de_vida=$matches[1]&embed=true";s:53:"categoria_estilo_de_vida/([^/]+)/page/?([0-9]{1,})/?$";s:64:"index.php?categoria_estilo_de_vida=$matches[1]&paged=$matches[2]";s:35:"categoria_estilo_de_vida/([^/]+)/?$";s:46:"index.php?categoria_estilo_de_vida=$matches[1]";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:13:"favicon\\.ico$";s:19:"index.php?favicon=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:38:"index.php?&page_id=6&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:11:{i:0;s:37:"acf-options-page/acf-options-page.php";i:1;s:29:"acf-repeater/acf-repeater.php";i:2;s:30:"advanced-custom-fields/acf.php";i:3;s:37:"breadcrumb-navxt/breadcrumb-navxt.php";i:4;s:36:"contact-form-7/wp-contact-form-7.php";i:5;s:39:"copy-delete-posts/copy-delete-posts.php";i:6;s:41:"post-type-switcher/post-type-switcher.php";i:7;s:47:"regenerate-thumbnails/regenerate-thumbnails.php";i:8;s:43:"sticky-posts-switch/sticky-posts-switch.php";i:9;s:31:"wp-migrate-db/wp-migrate-db.php";i:10;s:27:"wp-pagenavi/wp-pagenavi.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:2:{i:0;s:70:"/home/marcelo/projects/periodico/wp-content/themes/periodico/style.css";i:1;s:0:"";}', 'no'),
(40, 'template', 'periodico', 'yes'),
(41, 'stylesheet', 'periodico', 'yes'),
(42, 'comment_registration', '0', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '49752', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '1', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'page', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '150', 'yes'),
(57, 'thumbnail_size_h', '150', 'yes'),
(58, 'thumbnail_crop', '1', 'yes'),
(59, 'medium_size_w', '300', 'yes'),
(60, 'medium_size_h', '300', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '1024', 'yes'),
(63, 'large_size_h', '1024', 'yes'),
(64, 'image_default_link_type', 'none', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '0', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '0', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:2:{i:0;i:1;i:1;i:152;}', 'yes'),
(76, 'widget_categories', 'a:0:{}', 'yes'),
(77, 'widget_text', 'a:0:{}', 'yes'),
(78, 'widget_rss', 'a:0:{}', 'yes'),
(79, 'uninstall_plugins', 'a:2:{s:27:"wp-pagenavi/wp-pagenavi.php";s:14:"__return_false";s:39:"copy-delete-posts/copy-delete-posts.php";a:2:{i:0;s:15:"Account\\Account";i:1;s:25:"onUninstallPluginListener";}}', 'no'),
(80, 'timezone_string', '', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '6', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '0', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1644374777', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'auto_update_core_dev', 'enabled', 'yes'),
(96, 'auto_update_core_minor', 'enabled', 'yes'),
(97, 'auto_update_core_major', 'enabled', 'yes'),
(98, 'wp_force_deactivated_plugins', 'a:0:{}', 'yes'),
(99, 'initial_db_version', '49752', 'yes'),
(100, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:62:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:18:"bcn_manage_options";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(101, 'fresh_site', '0', 'yes'),
(102, 'WPLANG', 'es_ES', 'yes'),
(103, 'widget_block', 'a:6:{i:2;a:1:{s:7:"content";s:19:"<!-- wp:search /-->";}i:3;a:1:{s:7:"content";s:160:"<!-- wp:group --><div class="wp-block-group"><!-- wp:heading --><h2>Entradas recientes</h2><!-- /wp:heading --><!-- wp:latest-posts /--></div><!-- /wp:group -->";}i:4;a:1:{s:7:"content";s:233:"<!-- wp:group --><div class="wp-block-group"><!-- wp:heading --><h2>Comentarios recientes</h2><!-- /wp:heading --><!-- wp:latest-comments {"displayAvatar":false,"displayDate":false,"displayExcerpt":false} /--></div><!-- /wp:group -->";}i:5;a:1:{s:7:"content";s:146:"<!-- wp:group --><div class="wp-block-group"><!-- wp:heading --><h2>Archivos</h2><!-- /wp:heading --><!-- wp:archives /--></div><!-- /wp:group -->";}i:6;a:1:{s:7:"content";s:151:"<!-- wp:group --><div class="wp-block-group"><!-- wp:heading --><h2>Categorías</h2><!-- /wp:heading --><!-- wp:categories /--></div><!-- /wp:group -->";}s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'sidebars_widgets', 'a:2:{s:19:"wp_inactive_widgets";a:5:{i:0;s:7:"block-2";i:1;s:7:"block-3";i:2;s:7:"block-4";i:3;s:7:"block-5";i:4;s:7:"block-6";}s:13:"array_version";i:3;}', 'yes'),
(105, 'cron', 'a:7:{i:1631533583;a:1:{s:34:"wp_privacy_delete_old_export_files";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1631544383;a:4:{s:18:"wp_https_detection";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1631566824;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1631587582;a:1:{s:32:"recovery_mode_clean_expired_keys";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1631587662;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1631933182;a:1:{s:30:"wp_site_health_scheduled_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"weekly";s:4:"args";a:0:{}s:8:"interval";i:604800;}}}s:7:"version";i:2;}', 'yes'),
(106, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_archives', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(110, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(111, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(112, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(113, 'widget_meta', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(114, 'widget_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(115, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(116, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(117, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(119, 'recovery_keys', 'a:0:{}', 'yes'),
(120, 'https_detection_errors', 'a:1:{s:20:"https_request_failed";a:1:{i:0;s:24:"Solicitud HTTPS fallida.";}}', 'yes'),
(122, 'theme_mods_twentytwentyone', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1628822867;s:4:"data";a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:3:{i:0;s:7:"block-2";i:1;s:7:"block-3";i:2;s:7:"block-4";}s:9:"sidebar-2";a:2:{i:0;s:7:"block-5";i:1;s:7:"block-6";}}}}', 'yes'),
(136, 'can_compress_scripts', '0', 'no'),
(148, 'current_theme', 'Theme Periodico', 'yes'),
(149, 'theme_mods_periodico', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:1:{s:14:"menu_principal";i:2;}s:18:"custom_css_post_id";i:-1;}', 'yes'),
(150, 'theme_switched', '', 'yes'),
(153, 'finished_updating_comment_type', '1', 'yes'),
(175, 'recently_activated', 'a:1:{s:51:"custom-post-type-sticky/custom-post-type-sticky.php";i:1631395994;}', 'yes'),
(176, 'wpcf7', 'a:2:{s:7:"version";s:5:"5.4.2";s:13:"bulk_validate";a:4:{s:9:"timestamp";i:1629238485;s:7:"version";s:5:"5.1.3";s:11:"count_valid";i:1;s:13:"count_invalid";i:0;}}', 'yes'),
(177, 'pagenavi_options', 'a:15:{s:10:"pages_text";s:36:"Page %CURRENT_PAGE% of %TOTAL_PAGES%";s:12:"current_text";s:13:"%PAGE_NUMBER%";s:9:"page_text";s:13:"%PAGE_NUMBER%";s:10:"first_text";s:13:"&laquo; First";s:9:"last_text";s:12:"Last &raquo;";s:9:"prev_text";s:7:"&laquo;";s:9:"next_text";s:7:"&raquo;";s:12:"dotleft_text";s:3:"...";s:13:"dotright_text";s:3:"...";s:9:"num_pages";i:5;s:23:"num_larger_page_numbers";i:3;s:28:"larger_page_numbers_multiple";i:10;s:11:"always_show";b:0;s:16:"use_pagenavi_css";b:1;s:5:"style";i:1;}', 'yes'),
(178, 'acf_version', '5.9.9', 'yes'),
(181, 'ftp_credentials', 'a:3:{s:8:"hostname";s:9:"localhost";s:8:"username";s:7:"marcelo";s:15:"connection_type";s:3:"ftp";}', 'yes'),
(214, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(271, 'wpmdb_usage', 'a:2:{s:6:"action";s:8:"savefile";s:4:"time";i:1631531461;}', 'no'),
(347, 'category_children', 'a:0:{}', 'yes'),
(362, 'widget_bcn_widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(363, 'bcn_version', '6.6.0', 'no'),
(364, 'bcn_options_bk', 'a:74:{s:17:"bmainsite_display";b:1;s:18:"Hmainsite_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:28:"Hmainsite_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:13:"bhome_display";b:1;s:14:"Hhome_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:24:"Hhome_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:13:"bblog_display";b:1;s:10:"hseparator";s:15:"&nbsp;•&nbsp;";s:12:"blimit_title";b:0;s:17:"amax_title_length";i:20;s:20:"bcurrent_item_linked";b:0;s:28:"bpost_page_hierarchy_display";b:1;s:33:"bpost_page_hierarchy_parent_first";b:1;s:25:"Spost_page_hierarchy_type";s:15:"BCN_POST_PARENT";s:19:"Hpost_page_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:29:"Hpost_page_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:15:"apost_page_root";s:1:"6";s:15:"Hpaged_template";s:41:"<span class="%type%">Page %htitle%</span>";s:14:"bpaged_display";b:0;s:19:"Hpost_post_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:29:"Hpost_post_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:15:"apost_post_root";s:1:"0";s:28:"bpost_post_hierarchy_display";b:1;s:33:"bpost_post_hierarchy_parent_first";b:0;s:27:"bpost_post_taxonomy_referer";b:0;s:25:"Spost_post_hierarchy_type";s:8:"category";s:32:"bpost_attachment_archive_display";b:1;s:34:"bpost_attachment_hierarchy_display";b:1;s:39:"bpost_attachment_hierarchy_parent_first";b:1;s:33:"bpost_attachment_taxonomy_referer";b:0;s:31:"Spost_attachment_hierarchy_type";s:15:"BCN_POST_PARENT";s:21:"apost_attachment_root";i:0;s:25:"Hpost_attachment_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:35:"Hpost_attachment_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:13:"H404_template";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:10:"S404_title";s:3:"404";s:16:"Hsearch_template";s:319:"<span property="itemListElement" typeof="ListItem"><span property="name">Search results for &#039;<a property="item" typeof="WebPage" title="Go to the first page of search results for %title%." href="%link%" class="%type%" bcn-aria-current>%htitle%</a>&#039;</span><meta property="position" content="%position%"></span>";s:26:"Hsearch_template_no_anchor";s:67:"<span class="%type%">Search results for &#039;%htitle%&#039;</span>";s:22:"Htax_post_tag_template";s:268:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to the %title% tag archives." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:32:"Htax_post_tag_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:25:"Htax_post_format_template";s:264:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to the %title% archives." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:35:"Htax_post_format_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:16:"Hauthor_template";s:258:"<span property="itemListElement" typeof="ListItem"><span property="name">Articles by: <a title="Go to the first page of posts by %title%." href="%link%" class="%type%" bcn-aria-current>%htitle%</a></span><meta property="position" content="%position%"></span>";s:26:"Hauthor_template_no_anchor";s:49:"<span class="%type%">Articles by: %htitle%</span>";s:12:"Sauthor_name";s:12:"display_name";s:12:"aauthor_root";i:0;s:22:"Htax_category_template";s:273:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to the %title% category archives." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:32:"Htax_category_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:14:"Hdate_template";s:264:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to the %title% archives." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:24:"Hdate_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:38:"bpost_acf-field-group_taxonomy_referer";b:0;s:30:"Hpost_acf-field-group_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:40:"Hpost_acf-field-group_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:26:"apost_acf-field-group_root";i:0;s:39:"bpost_acf-field-group_hierarchy_display";b:0;s:37:"bpost_acf-field-group_archive_display";b:0;s:36:"Spost_acf-field-group_hierarchy_type";s:10:"BCN_PARENT";s:44:"bpost_acf-field-group_hierarchy_parent_first";b:0;s:32:"bpost_acf-field_taxonomy_referer";b:0;s:24:"Hpost_acf-field_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:34:"Hpost_acf-field_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:20:"apost_acf-field_root";i:0;s:33:"bpost_acf-field_hierarchy_display";b:0;s:31:"bpost_acf-field_archive_display";b:0;s:30:"Spost_acf-field_hierarchy_type";s:10:"BCN_PARENT";s:38:"bpost_acf-field_hierarchy_parent_first";b:0;s:41:"bpost_wpcf7_contact_form_taxonomy_referer";b:0;s:33:"Hpost_wpcf7_contact_form_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:43:"Hpost_wpcf7_contact_form_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:29:"apost_wpcf7_contact_form_root";i:0;s:42:"bpost_wpcf7_contact_form_hierarchy_display";b:0;s:40:"bpost_wpcf7_contact_form_archive_display";b:0;s:39:"Spost_wpcf7_contact_form_hierarchy_type";s:8:"BCN_DATE";s:47:"bpost_wpcf7_contact_form_hierarchy_parent_first";b:0;}', 'yes'),
(365, 'bcn_options', 'a:74:{s:17:"bmainsite_display";b:1;s:18:"Hmainsite_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:28:"Hmainsite_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:13:"bhome_display";b:1;s:14:"Hhome_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:24:"Hhome_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:13:"bblog_display";b:1;s:10:"hseparator";s:27:"&nbsp;&nbsp;•&nbsp;&nbsp;";s:12:"blimit_title";b:0;s:17:"amax_title_length";i:20;s:20:"bcurrent_item_linked";b:0;s:28:"bpost_page_hierarchy_display";b:1;s:33:"bpost_page_hierarchy_parent_first";b:1;s:25:"Spost_page_hierarchy_type";s:15:"BCN_POST_PARENT";s:19:"Hpost_page_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:29:"Hpost_page_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:15:"apost_page_root";s:1:"6";s:15:"Hpaged_template";s:41:"<span class="%type%">Page %htitle%</span>";s:14:"bpaged_display";b:0;s:19:"Hpost_post_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:29:"Hpost_post_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:15:"apost_post_root";s:1:"0";s:28:"bpost_post_hierarchy_display";b:1;s:33:"bpost_post_hierarchy_parent_first";b:0;s:27:"bpost_post_taxonomy_referer";b:0;s:25:"Spost_post_hierarchy_type";s:8:"category";s:32:"bpost_attachment_archive_display";b:1;s:34:"bpost_attachment_hierarchy_display";b:1;s:39:"bpost_attachment_hierarchy_parent_first";b:1;s:33:"bpost_attachment_taxonomy_referer";b:0;s:31:"Spost_attachment_hierarchy_type";s:15:"BCN_POST_PARENT";s:21:"apost_attachment_root";i:0;s:25:"Hpost_attachment_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:35:"Hpost_attachment_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:13:"H404_template";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:10:"S404_title";s:3:"404";s:16:"Hsearch_template";s:319:"<span property="itemListElement" typeof="ListItem"><span property="name">Search results for &#039;<a property="item" typeof="WebPage" title="Go to the first page of search results for %title%." href="%link%" class="%type%" bcn-aria-current>%htitle%</a>&#039;</span><meta property="position" content="%position%"></span>";s:26:"Hsearch_template_no_anchor";s:67:"<span class="%type%">Search results for &#039;%htitle%&#039;</span>";s:22:"Htax_post_tag_template";s:268:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to the %title% tag archives." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:32:"Htax_post_tag_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:25:"Htax_post_format_template";s:264:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to the %title% archives." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:35:"Htax_post_format_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:16:"Hauthor_template";s:258:"<span property="itemListElement" typeof="ListItem"><span property="name">Articles by: <a title="Go to the first page of posts by %title%." href="%link%" class="%type%" bcn-aria-current>%htitle%</a></span><meta property="position" content="%position%"></span>";s:26:"Hauthor_template_no_anchor";s:49:"<span class="%type%">Articles by: %htitle%</span>";s:12:"Sauthor_name";s:12:"display_name";s:12:"aauthor_root";i:0;s:22:"Htax_category_template";s:273:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to the %title% category archives." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:32:"Htax_category_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:14:"Hdate_template";s:264:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to the %title% archives." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:24:"Hdate_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:38:"bpost_acf-field-group_taxonomy_referer";b:0;s:30:"Hpost_acf-field-group_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:40:"Hpost_acf-field-group_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:26:"apost_acf-field-group_root";i:0;s:39:"bpost_acf-field-group_hierarchy_display";b:0;s:37:"bpost_acf-field-group_archive_display";b:0;s:36:"Spost_acf-field-group_hierarchy_type";s:10:"BCN_PARENT";s:44:"bpost_acf-field-group_hierarchy_parent_first";b:0;s:32:"bpost_acf-field_taxonomy_referer";b:0;s:24:"Hpost_acf-field_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:34:"Hpost_acf-field_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:20:"apost_acf-field_root";i:0;s:33:"bpost_acf-field_hierarchy_display";b:0;s:31:"bpost_acf-field_archive_display";b:0;s:30:"Spost_acf-field_hierarchy_type";s:10:"BCN_PARENT";s:38:"bpost_acf-field_hierarchy_parent_first";b:0;s:41:"bpost_wpcf7_contact_form_taxonomy_referer";b:0;s:33:"Hpost_wpcf7_contact_form_template";s:251:"<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to %title%." href="%link%" class="%type%" bcn-aria-current><span property="name">%htitle%</span></a><meta property="position" content="%position%"></span>";s:43:"Hpost_wpcf7_contact_form_template_no_anchor";s:195:"<span property="itemListElement" typeof="ListItem"><span property="name" class="%type%">%htitle%</span><meta property="url" content="%link%"><meta property="position" content="%position%"></span>";s:29:"apost_wpcf7_contact_form_root";i:0;s:42:"bpost_wpcf7_contact_form_hierarchy_display";b:0;s:40:"bpost_wpcf7_contact_form_archive_display";b:0;s:39:"Spost_wpcf7_contact_form_hierarchy_type";s:8:"BCN_DATE";s:47:"bpost_wpcf7_contact_form_hierarchy_parent_first";b:0;}', 'yes'),
(704, 'categoria_noticias_children', 'a:0:{}', 'yes'),
(712, 'categoria_estilo_de_vida_children', 'a:0:{}', 'yes'),
(726, 'analyst_cache', 's:6:"a:0:{}";', 'yes'),
(729, '_cdp_review', 'a:2:{s:9:"installed";i:1631394079;s:5:"users";a:0:{}}', 'yes'),
(730, '_cdp_globals', 'a:1:{s:6:"others";a:13:{s:17:"cdp-content-pages";s:4:"true";s:17:"cdp-content-posts";s:4:"true";s:18:"cdp-content-custom";s:4:"true";s:17:"cdp-display-posts";s:4:"true";s:16:"cdp-display-edit";s:4:"true";s:17:"cdp-display-admin";s:4:"true";s:16:"cdp-display-bulk";s:4:"true";s:21:"cdp-display-gutenberg";s:4:"true";s:19:"cdp-references-post";s:5:"false";s:19:"cdp-references-edit";s:5:"false";s:18:"cdp-premium-import";s:5:"false";s:24:"cdp-premium-hide-tooltip";s:5:"false";s:20:"cdp-menu-in-settings";s:5:"false";}}', 'yes'),
(732, '_cdp_profiles', 'a:1:{s:7:"default";a:25:{s:5:"title";s:4:"true";s:4:"date";s:5:"false";s:6:"status";s:5:"false";s:4:"slug";s:4:"true";s:7:"excerpt";s:4:"true";s:7:"content";s:4:"true";s:7:"f_image";s:4:"true";s:8:"template";s:4:"true";s:6:"format";s:4:"true";s:6:"author";s:4:"true";s:8:"password";s:4:"true";s:11:"attachments";s:5:"false";s:8:"children";s:5:"false";s:8:"comments";s:5:"false";s:10:"menu_order";s:4:"true";s:8:"category";s:4:"true";s:8:"post_tag";s:4:"true";s:8:"taxonomy";s:4:"true";s:8:"nav_menu";s:4:"true";s:13:"link_category";s:4:"true";s:12:"all_metadata";s:5:"false";s:5:"names";a:5:{s:6:"prefix";s:0:"";s:6:"suffix";s:10:"#[Counter]";s:6:"format";s:1:"1";s:6:"custom";s:5:"m/d/Y";s:7:"display";s:7:"Default";}s:9:"usmplugin";s:5:"false";s:5:"yoast";s:5:"false";s:3:"woo";s:5:"false";}}', 'yes'),
(733, '_cdp_default_setup', '1', 'yes'),
(734, '_irb_h_bn_review', 'a:2:{s:5:"users";a:0:{}s:17:"copy-delete-posts";i:1631394079;}', 'yes'),
(735, '_cdp_preselections', 'a:1:{i:1;s:7:"default";}', 'yes'),
(736, 'analyst_notices', 's:709:"O:29:"Analyst\\Notices\\NoticeFactory":2:{s:10:"\0*\0notices";a:1:{i:1;O:22:"Analyst\\Notices\\Notice":4:{s:5:"\0*\0id";s:13:"613d195be97de";s:7:"\0*\0body";s:36:"Thank you for confirming your email.";s:12:"\0*\0accountId";s:16:"ovgxe3xq075ladbp";s:13:"\0*\0pluginName";s:23:"Copy &amp; Delete Posts";}}s:7:"notices";a:1:{i:1;O:22:"Analyst\\Notices\\Notice":8:{s:5:"\0*\0id";s:13:"613d195be97de";s:7:"\0*\0body";s:36:"Thank you for confirming your email.";s:12:"\0*\0accountId";s:16:"ovgxe3xq075ladbp";s:13:"\0*\0pluginName";s:23:"Copy &amp; Delete Posts";s:2:"id";s:13:"613d195be97de";s:4:"body";s:36:"Thank you for confirming your email.";s:9:"accountId";s:16:"ovgxe3xq075ladbp";s:10:"pluginName";s:23:"Copy &amp; Delete Posts";}}}";', 'yes'),
(737, 'analyst_accounts_data', 's:1086:"O:26:"Account\\AccountDataFactory":2:{s:11:"\0*\0accounts";a:1:{i:0;O:19:"Account\\AccountData":7:{s:5:"\0*\0id";s:16:"ovgxe3xq075ladbp";s:9:"\0*\0secret";s:40:"b4de5ed2ba7be687e233d152ec1e8fd116052ab0";s:7:"\0*\0path";s:91:"/home/marcelo/projects/periodico/wp-content/plugins/copy-delete-posts/copy-delete-posts.php";s:14:"\0*\0isInstalled";b:0;s:12:"\0*\0isOptedIn";b:1;s:11:"\0*\0isSigned";b:1;s:20:"\0*\0isInstallResolved";b:1;}}s:8:"accounts";a:1:{i:0;O:19:"Account\\AccountData":14:{s:5:"\0*\0id";s:16:"ovgxe3xq075ladbp";s:9:"\0*\0secret";s:40:"b4de5ed2ba7be687e233d152ec1e8fd116052ab0";s:7:"\0*\0path";s:91:"/home/marcelo/projects/periodico/wp-content/plugins/copy-delete-posts/copy-delete-posts.php";s:14:"\0*\0isInstalled";b:0;s:12:"\0*\0isOptedIn";b:1;s:11:"\0*\0isSigned";b:1;s:20:"\0*\0isInstallResolved";b:1;s:2:"id";s:16:"ovgxe3xq075ladbp";s:6:"secret";s:40:"b4de5ed2ba7be687e233d152ec1e8fd116052ab0";s:4:"path";s:91:"/home/marcelo/projects/periodico/wp-content/plugins/copy-delete-posts/copy-delete-posts.php";s:11:"isInstalled";b:0;s:9:"isOptedIn";b:1;s:8:"isSigned";b:1;s:17:"isInstallResolved";b:1;}}}";', 'yes'),
(750, 'sticky_posts_switch_options', 'a:4:{s:10:"post_types";a:2:{i:0;s:4:"post";i:1;s:8:"noticias";}s:18:"show_on_front_page";a:1:{i:0;s:4:"post";}s:15:"show_on_archive";a:1:{i:0;s:4:"post";}s:10:"sort_order";a:8:{i:0;s:2:"cb";i:1;s:11:"sticky_post";i:2;s:5:"title";i:3;s:6:"author";i:4;s:10:"categories";i:5;s:4:"tags";i:6;s:8:"comments";i:7;s:4:"date";}}', 'yes'),
(783, 'options_facebook', '#', 'no'),
(784, '_options_facebook', 'field_613d11efb5b1d', 'no'),
(785, 'options_twitter', '#', 'no'),
(786, '_options_twitter', 'field_613d1210b5b1e', 'no'),
(787, 'options_instagram', '#', 'no'),
(788, '_options_instagram', 'field_613d1216b5b1f', 'no') ;

#
# End of data contents of table `wp_options`
# --------------------------------------------------------



#
# Delete any existing table `wp_postmeta`
#

DROP TABLE IF EXISTS `wp_postmeta`;


#
# Table structure of table `wp_postmeta`
#

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=539 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wp_postmeta`
#
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_form', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit "Send"]'),
(4, 5, '_mail', 'a:8:{s:7:"subject";s:26:"Periodico "[your-subject]"";s:6:"sender";s:41:"Periodico <wordpress@periodico.sitios.mb>";s:4:"body";s:172:"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Periodico (http://talcualstaging.ml)";s:9:"recipient";s:24:"marcelombs2109@gmail.com";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";i:0;s:13:"exclude_blank";i:0;}'),
(5, 5, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:26:"Periodico "[your-subject]"";s:6:"sender";s:41:"Periodico <wordpress@periodico.sitios.mb>";s:4:"body";s:114:"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Periodico (http://talcualstaging.ml)";s:9:"recipient";s:12:"[your-email]";s:18:"additional_headers";s:34:"Reply-To: marcelombs2109@gmail.com";s:11:"attachments";s:0:"";s:8:"use_html";i:0;s:13:"exclude_blank";i:0;}'),
(6, 5, '_messages', 'a:8:{s:12:"mail_sent_ok";s:45:"Thank you for your message. It has been sent.";s:12:"mail_sent_ng";s:71:"There was an error trying to send your message. Please try again later.";s:16:"validation_error";s:61:"One or more fields have an error. Please check and try again.";s:4:"spam";s:71:"There was an error trying to send your message. Please try again later.";s:12:"accept_terms";s:69:"You must accept the terms and conditions before sending your message.";s:16:"invalid_required";s:22:"The field is required.";s:16:"invalid_too_long";s:22:"The field is too long.";s:17:"invalid_too_short";s:23:"The field is too short.";}'),
(7, 5, '_additional_settings', NULL),
(8, 5, '_locale', 'es_ES'),
(9, 6, '_edit_lock', '1631531182:1'),
(10, 6, '_wp_page_template', 'page-templates/template-home.php'),
(38, 12, '_menu_item_type', 'custom'),
(39, 12, '_menu_item_menu_item_parent', '0'),
(40, 12, '_menu_item_object_id', '12'),
(41, 12, '_menu_item_object', 'custom'),
(42, 12, '_menu_item_target', ''),
(43, 12, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(44, 12, '_menu_item_xfn', ''),
(45, 12, '_menu_item_url', '#'),
(47, 13, '_menu_item_type', 'custom'),
(48, 13, '_menu_item_menu_item_parent', '0'),
(49, 13, '_menu_item_object_id', '13'),
(50, 13, '_menu_item_object', 'custom'),
(51, 13, '_menu_item_target', ''),
(52, 13, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(53, 13, '_menu_item_xfn', ''),
(54, 13, '_menu_item_url', '#'),
(56, 14, '_menu_item_type', 'custom'),
(57, 14, '_menu_item_menu_item_parent', '0'),
(58, 14, '_menu_item_object_id', '14'),
(59, 14, '_menu_item_object', 'custom'),
(60, 14, '_menu_item_target', ''),
(61, 14, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(62, 14, '_menu_item_xfn', ''),
(63, 14, '_menu_item_url', '#'),
(65, 15, '_menu_item_type', 'custom'),
(66, 15, '_menu_item_menu_item_parent', '0'),
(67, 15, '_menu_item_object_id', '15'),
(68, 15, '_menu_item_object', 'custom'),
(69, 15, '_menu_item_target', ''),
(70, 15, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(71, 15, '_menu_item_xfn', ''),
(72, 15, '_menu_item_url', '#'),
(93, 12, '_wp_old_date', '2021-08-20'),
(94, 13, '_wp_old_date', '2021-08-20'),
(95, 14, '_wp_old_date', '2021-08-20'),
(96, 15, '_wp_old_date', '2021-08-20'),
(97, 18, '_wp_attached_file', '2021/08/unreal_dm_-_Blue_Circles.mp3'),
(98, 18, '_wp_attachment_metadata', 'a:23:{s:10:"dataformat";s:3:"mp3";s:8:"channels";i:2;s:11:"sample_rate";i:44100;s:7:"bitrate";d:235966.03184158087;s:11:"channelmode";s:6:"stereo";s:12:"bitrate_mode";s:3:"vbr";s:5:"codec";s:4:"LAME";s:7:"encoder";s:9:"LAME3.98r";s:8:"lossless";b:0;s:15:"encoder_options";s:21:"--preset extreme -b32";s:17:"compression_ratio";d:0.16720948968365992;s:10:"fileformat";s:3:"mp3";s:8:"filesize";i:4644183;s:9:"mime_type";s:10:"audio/mpeg";s:6:"length";i:157;s:16:"length_formatted";s:4:"2:37";s:5:"title";s:24:"Blue Circles (ft. CSoul)";s:6:"artist";s:9:"unreal_dm";s:17:"copyright_message";s:139:"2011 unreal_dm Licensed to the public under http://creativecommons.org/licenses/by/2.5/ Verify at http://ccmixter.org/files/unreal_dm/33850";s:7:"remixer";s:9:"unreal_dm";s:4:"year";s:4:"2011";s:8:"url_user";s:36:"http://ccmixter.org/people/unreal_dm";s:5:"album";s:8:"ccMixter";}'),
(99, 19, '_edit_last', '1'),
(100, 19, '_edit_lock', '1629647664:1'),
(107, 19, '_wp_trash_meta_status', 'publish'),
(108, 19, '_wp_trash_meta_time', '1629647840'),
(109, 19, '_wp_desired_post_slug', 'group_612272471496c'),
(110, 20, '_wp_trash_meta_status', 'publish'),
(111, 20, '_wp_trash_meta_time', '1629647840'),
(112, 20, '_wp_desired_post_slug', 'field_612272506e122'),
(113, 6, '_edit_last', '1'),
(114, 6, 'seleccionar_plantilla', 'Importante'),
(115, 6, '_seleccionar_plantilla', 'field_612272506e122'),
(116, 21, 'seleccionar_plantilla', 'Intermedia'),
(117, 21, '_seleccionar_plantilla', 'field_612272506e122'),
(118, 22, 'seleccionar_plantilla', 'Normal'),
(119, 22, '_seleccionar_plantilla', 'field_612272506e122'),
(120, 23, 'seleccionar_plantilla', 'Intermedia'),
(121, 23, '_seleccionar_plantilla', 'field_612272506e122'),
(122, 24, 'seleccionar_plantilla', 'Intermedia'),
(123, 24, '_seleccionar_plantilla', 'field_612272506e122'),
(124, 25, 'seleccionar_plantilla', 'Importante'),
(125, 25, '_seleccionar_plantilla', 'field_612272506e122'),
(126, 26, 'seleccionar_plantilla', 'Normal'),
(127, 26, '_seleccionar_plantilla', 'field_612272506e122'),
(128, 27, '_menu_item_type', 'custom'),
(129, 27, '_menu_item_menu_item_parent', '0'),
(130, 27, '_menu_item_object_id', '27'),
(131, 27, '_menu_item_object', 'custom'),
(132, 27, '_menu_item_target', ''),
(133, 27, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(134, 27, '_menu_item_xfn', ''),
(135, 27, '_menu_item_url', '#'),
(137, 28, 'seleccionar_plantilla', 'Intermedia'),
(138, 28, '_seleccionar_plantilla', 'field_612272506e122'),
(139, 29, 'seleccionar_plantilla', 'Normal'),
(140, 29, '_seleccionar_plantilla', 'field_612272506e122'),
(141, 30, 'seleccionar_plantilla', 'Intermedia'),
(142, 30, '_seleccionar_plantilla', 'field_612272506e122'),
(143, 31, 'seleccionar_plantilla', 'Importante'),
(144, 31, '_seleccionar_plantilla', 'field_612272506e122'),
(145, 33, '_menu_item_type', 'custom'),
(146, 33, '_menu_item_menu_item_parent', '0'),
(147, 33, '_menu_item_object_id', '33'),
(148, 33, '_menu_item_object', 'custom'),
(149, 33, '_menu_item_target', ''),
(150, 33, '_menu_item_classes', 'a:1:{i:0;s:15:"li-item-submenu";}'),
(151, 33, '_menu_item_xfn', ''),
(152, 33, '_menu_item_url', '#'),
(155, 12, '_wp_old_date', '2021-08-22'),
(156, 13, '_wp_old_date', '2021-08-22'),
(157, 14, '_wp_old_date', '2021-08-22'),
(158, 15, '_wp_old_date', '2021-08-22'),
(159, 27, '_wp_old_date', '2021-08-22') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(160, 34, 'seleccionar_plantilla', 'Intermedia'),
(161, 34, '_seleccionar_plantilla', 'field_612272506e122'),
(162, 35, 'seleccionar_plantilla', 'Importante'),
(163, 35, '_seleccionar_plantilla', 'field_612272506e122'),
(164, 36, '_menu_item_type', 'custom'),
(165, 36, '_menu_item_menu_item_parent', '156'),
(166, 36, '_menu_item_object_id', '36'),
(167, 36, '_menu_item_object', 'custom'),
(168, 36, '_menu_item_target', ''),
(169, 36, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(170, 36, '_menu_item_xfn', ''),
(171, 36, '_menu_item_url', '#'),
(173, 37, '_menu_item_type', 'custom'),
(174, 37, '_menu_item_menu_item_parent', '156'),
(175, 37, '_menu_item_object_id', '37'),
(176, 37, '_menu_item_object', 'custom'),
(177, 37, '_menu_item_target', ''),
(178, 37, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(179, 37, '_menu_item_xfn', ''),
(180, 37, '_menu_item_url', '#'),
(182, 38, '_menu_item_type', 'custom'),
(183, 38, '_menu_item_menu_item_parent', '156'),
(184, 38, '_menu_item_object_id', '38'),
(185, 38, '_menu_item_object', 'custom'),
(186, 38, '_menu_item_target', ''),
(187, 38, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(188, 38, '_menu_item_xfn', ''),
(189, 38, '_menu_item_url', '#'),
(191, 39, '_menu_item_type', 'custom'),
(192, 39, '_menu_item_menu_item_parent', '156'),
(193, 39, '_menu_item_object_id', '39'),
(194, 39, '_menu_item_object', 'custom'),
(195, 39, '_menu_item_target', ''),
(196, 39, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(197, 39, '_menu_item_xfn', ''),
(198, 39, '_menu_item_url', '#'),
(200, 40, '_menu_item_type', 'custom'),
(201, 40, '_menu_item_menu_item_parent', '156'),
(202, 40, '_menu_item_object_id', '40'),
(203, 40, '_menu_item_object', 'custom'),
(204, 40, '_menu_item_target', ''),
(205, 40, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(206, 40, '_menu_item_xfn', ''),
(207, 40, '_menu_item_url', '#'),
(209, 41, '_menu_item_type', 'custom'),
(210, 41, '_menu_item_menu_item_parent', '33'),
(211, 41, '_menu_item_object_id', '41'),
(212, 41, '_menu_item_object', 'custom'),
(213, 41, '_menu_item_target', ''),
(214, 41, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(215, 41, '_menu_item_xfn', ''),
(216, 41, '_menu_item_url', '#'),
(218, 42, '_menu_item_type', 'custom'),
(219, 42, '_menu_item_menu_item_parent', '33'),
(220, 42, '_menu_item_object_id', '42'),
(221, 42, '_menu_item_object', 'custom'),
(222, 42, '_menu_item_target', ''),
(223, 42, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(224, 42, '_menu_item_xfn', ''),
(225, 42, '_menu_item_url', '#'),
(227, 43, '_menu_item_type', 'custom'),
(228, 43, '_menu_item_menu_item_parent', '33'),
(229, 43, '_menu_item_object_id', '43'),
(230, 43, '_menu_item_object', 'custom'),
(231, 43, '_menu_item_target', ''),
(232, 43, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(233, 43, '_menu_item_xfn', ''),
(234, 43, '_menu_item_url', '#'),
(236, 44, '_menu_item_type', 'custom'),
(237, 44, '_menu_item_menu_item_parent', '33'),
(238, 44, '_menu_item_object_id', '44'),
(239, 44, '_menu_item_object', 'custom'),
(240, 44, '_menu_item_target', ''),
(241, 44, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(242, 44, '_menu_item_xfn', ''),
(243, 44, '_menu_item_url', '#'),
(245, 45, '_menu_item_type', 'custom'),
(246, 45, '_menu_item_menu_item_parent', '33'),
(247, 45, '_menu_item_object_id', '45'),
(248, 45, '_menu_item_object', 'custom'),
(249, 45, '_menu_item_target', ''),
(250, 45, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(251, 45, '_menu_item_xfn', ''),
(252, 45, '_menu_item_url', '#'),
(254, 46, '_menu_item_type', 'custom'),
(255, 46, '_menu_item_menu_item_parent', '33'),
(256, 46, '_menu_item_object_id', '46'),
(257, 46, '_menu_item_object', 'custom'),
(258, 46, '_menu_item_target', ''),
(259, 46, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(260, 46, '_menu_item_xfn', ''),
(261, 46, '_menu_item_url', '#'),
(263, 47, 'seleccionar_plantilla', 'Intermedia'),
(264, 47, '_seleccionar_plantilla', 'field_612272506e122'),
(265, 48, 'seleccionar_plantilla', 'Normal'),
(266, 48, '_seleccionar_plantilla', 'field_612272506e122'),
(267, 1, '_edit_lock', '1631397722:1'),
(268, 49, '_wp_attached_file', '2021/08/noticia2.jpeg'),
(269, 49, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1280;s:6:"height";i:894;s:4:"file";s:21:"2021/08/noticia2.jpeg";s:5:"sizes";a:9:{s:6:"medium";a:4:{s:4:"file";s:21:"noticia2-300x210.jpeg";s:5:"width";i:300;s:6:"height";i:210;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:22:"noticia2-1024x715.jpeg";s:5:"width";i:1024;s:6:"height";i:715;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:21:"noticia2-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:21:"noticia2-768x536.jpeg";s:5:"width";i:768;s:6:"height";i:536;s:9:"mime-type";s:10:"image/jpeg";}s:19:"thumb-noticias-home";a:4:{s:4:"file";s:21:"noticia2-275x275.jpeg";s:5:"width";i:275;s:6:"height";i:275;s:9:"mime-type";s:10:"image/jpeg";}s:32:"thumb-noticias-sticky-intermedia";a:4:{s:4:"file";s:21:"noticia2-275x400.jpeg";s:5:"width";i:275;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}s:28:"thumb-noticias-sticky-normal";a:4:{s:4:"file";s:21:"noticia2-490x375.jpeg";s:5:"width";i:490;s:6:"height";i:375;s:9:"mime-type";s:10:"image/jpeg";}s:32:"thumb-noticias-sticky-importante";a:4:{s:4:"file";s:22:"noticia2-1165x555.jpeg";s:5:"width";i:1165;s:6:"height";i:555;s:9:"mime-type";s:10:"image/jpeg";}s:27:"thumb-noticias-sticky-95x75";a:4:{s:4:"file";s:19:"noticia2-95x75.jpeg";s:5:"width";i:95;s:6:"height";i:75;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(278, 56, 'seleccionar_plantilla', 'Intermedia') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(279, 56, '_seleccionar_plantilla', 'field_612272506e122'),
(280, 58, '_edit_last', '1'),
(281, 58, '_edit_lock', '1630262916:1'),
(282, 59, 'seleccionar_plantilla', 'Normal'),
(283, 59, '_seleccionar_plantilla', 'field_612272506e122'),
(284, 60, 'seleccionar_plantilla', 'Intermedia'),
(285, 60, '_seleccionar_plantilla', 'field_612272506e122'),
(286, 61, 'seleccionar_plantilla', 'Normal'),
(287, 61, '_seleccionar_plantilla', 'field_612272506e122'),
(288, 62, 'seleccionar_plantilla', 'Importante'),
(289, 62, '_seleccionar_plantilla', 'field_612272506e122'),
(290, 63, 'seleccionar_plantilla', 'Normal'),
(291, 63, '_seleccionar_plantilla', 'field_612272506e122'),
(292, 64, 'seleccionar_plantilla', 'Intermedia'),
(293, 64, '_seleccionar_plantilla', 'field_612272506e122'),
(294, 65, 'seleccionar_plantilla', 'Normal'),
(295, 65, '_seleccionar_plantilla', 'field_612272506e122'),
(296, 66, 'seleccionar_plantilla', 'Importante'),
(297, 66, '_seleccionar_plantilla', 'field_612272506e122'),
(298, 67, 'seleccionar_plantilla', 'Intermedia'),
(299, 67, '_seleccionar_plantilla', 'field_612272506e122'),
(300, 68, 'seleccionar_plantilla', 'Importante'),
(301, 68, '_seleccionar_plantilla', 'field_612272506e122'),
(302, 69, 'seleccionar_plantilla', 'Intermedia'),
(303, 69, '_seleccionar_plantilla', 'field_612272506e122'),
(304, 70, 'seleccionar_plantilla', 'Importante'),
(305, 70, '_seleccionar_plantilla', 'field_612272506e122'),
(306, 71, 'seleccionar_plantilla', 'Importante'),
(307, 71, '_seleccionar_plantilla', 'field_612272506e122'),
(308, 72, 'seleccionar_plantilla', 'Intermedia'),
(309, 72, '_seleccionar_plantilla', 'field_612272506e122'),
(310, 73, 'seleccionar_plantilla', 'Normal'),
(311, 73, '_seleccionar_plantilla', 'field_612272506e122'),
(312, 74, 'seleccionar_plantilla', 'Normal'),
(313, 74, '_seleccionar_plantilla', 'field_612272506e122'),
(314, 75, 'seleccionar_plantilla', 'Normal'),
(315, 75, '_seleccionar_plantilla', 'field_612272506e122'),
(316, 76, 'seleccionar_plantilla', 'Importante'),
(317, 76, '_seleccionar_plantilla', 'field_612272506e122'),
(318, 77, 'seleccionar_plantilla', 'Intermedia'),
(319, 77, '_seleccionar_plantilla', 'field_612272506e122'),
(320, 78, 'seleccionar_plantilla', 'Normal'),
(321, 78, '_seleccionar_plantilla', 'field_612272506e122'),
(322, 79, 'seleccionar_plantilla', 'Importante'),
(323, 79, '_seleccionar_plantilla', 'field_612272506e122'),
(324, 81, 'seleccionar_plantilla', 'Intermedia'),
(325, 81, '_seleccionar_plantilla', 'field_612272506e122'),
(326, 82, 'seleccionar_plantilla', 'Normal'),
(327, 82, '_seleccionar_plantilla', 'field_612272506e122'),
(328, 83, 'seleccionar_plantilla', 'Normal'),
(329, 83, '_seleccionar_plantilla', 'field_612272506e122'),
(330, 84, 'seleccionar_plantilla', 'Intermedia'),
(331, 84, '_seleccionar_plantilla', 'field_612272506e122'),
(332, 85, 'seleccionar_plantilla', 'Normal'),
(333, 85, '_seleccionar_plantilla', 'field_612272506e122'),
(334, 86, 'seleccionar_plantilla', 'Intermedia'),
(335, 86, '_seleccionar_plantilla', 'field_612272506e122'),
(336, 87, 'seleccionar_plantilla', 'Normal'),
(337, 87, '_seleccionar_plantilla', 'field_612272506e122'),
(338, 88, 'seleccionar_plantilla', 'Importante'),
(339, 88, '_seleccionar_plantilla', 'field_612272506e122'),
(340, 89, 'seleccionar_plantilla', 'Intermedia'),
(341, 89, '_seleccionar_plantilla', 'field_612272506e122'),
(342, 90, 'seleccionar_plantilla', 'Normal'),
(343, 90, '_seleccionar_plantilla', 'field_612272506e122'),
(344, 91, 'seleccionar_plantilla', 'Intermedia'),
(345, 91, '_seleccionar_plantilla', 'field_612272506e122'),
(346, 92, 'seleccionar_plantilla', 'Normal'),
(347, 92, '_seleccionar_plantilla', 'field_612272506e122'),
(348, 93, 'seleccionar_plantilla', 'Intermedia'),
(349, 93, '_seleccionar_plantilla', 'field_612272506e122'),
(350, 94, 'seleccionar_plantilla', 'Normal'),
(351, 94, '_seleccionar_plantilla', 'field_612272506e122'),
(352, 95, 'seleccionar_plantilla', 'Importante'),
(353, 95, '_seleccionar_plantilla', 'field_612272506e122'),
(354, 96, 'seleccionar_plantilla', 'Intermedia'),
(355, 96, '_seleccionar_plantilla', 'field_612272506e122'),
(356, 97, 'seleccionar_plantilla', 'Intermedia'),
(357, 97, '_seleccionar_plantilla', 'field_612272506e122'),
(358, 98, 'seleccionar_plantilla', 'Importante'),
(359, 98, '_seleccionar_plantilla', 'field_612272506e122'),
(360, 99, 'seleccionar_plantilla', 'Normal'),
(361, 99, '_seleccionar_plantilla', 'field_612272506e122'),
(362, 100, 'seleccionar_plantilla', 'Intermedia'),
(363, 100, '_seleccionar_plantilla', 'field_612272506e122'),
(364, 101, 'seleccionar_plantilla', 'Normal'),
(365, 101, '_seleccionar_plantilla', 'field_612272506e122'),
(366, 102, 'seleccionar_plantilla', 'Normal'),
(367, 102, '_seleccionar_plantilla', 'field_612272506e122'),
(368, 103, 'seleccionar_plantilla', 'Importante'),
(369, 103, '_seleccionar_plantilla', 'field_612272506e122'),
(370, 104, 'seleccionar_plantilla', 'Normal'),
(371, 104, '_seleccionar_plantilla', 'field_612272506e122'),
(372, 105, 'seleccionar_plantilla', 'Intermedia'),
(373, 105, '_seleccionar_plantilla', 'field_612272506e122'),
(374, 106, 'seleccionar_plantilla', 'Normal'),
(375, 106, '_seleccionar_plantilla', 'field_612272506e122'),
(376, 107, 'seleccionar_plantilla', 'Importante'),
(377, 107, '_seleccionar_plantilla', 'field_612272506e122'),
(378, 108, 'seleccionar_plantilla', 'Intermedia') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(379, 108, '_seleccionar_plantilla', 'field_612272506e122'),
(380, 109, 'seleccionar_plantilla', 'Importante'),
(381, 109, '_seleccionar_plantilla', 'field_612272506e122'),
(382, 110, 'seleccionar_plantilla', 'Intermedia'),
(383, 110, '_seleccionar_plantilla', 'field_612272506e122'),
(384, 111, 'seleccionar_plantilla', 'Importante'),
(385, 111, '_seleccionar_plantilla', 'field_612272506e122'),
(386, 112, 'seleccionar_plantilla', 'Normal'),
(387, 112, '_seleccionar_plantilla', 'field_612272506e122'),
(388, 113, 'seleccionar_plantilla', 'Intermedia'),
(389, 113, '_seleccionar_plantilla', 'field_612272506e122'),
(390, 114, 'seleccionar_plantilla', 'Importante'),
(391, 114, '_seleccionar_plantilla', 'field_612272506e122'),
(392, 115, 'seleccionar_plantilla', 'Normal'),
(393, 115, '_seleccionar_plantilla', 'field_612272506e122'),
(394, 116, 'seleccionar_plantilla', 'Intermedia'),
(395, 116, '_seleccionar_plantilla', 'field_612272506e122'),
(396, 117, 'seleccionar_plantilla', 'Normal'),
(397, 117, '_seleccionar_plantilla', 'field_612272506e122'),
(398, 118, 'seleccionar_plantilla', 'Importante'),
(399, 118, '_seleccionar_plantilla', 'field_612272506e122'),
(400, 119, 'seleccionar_plantilla', 'Intermedia'),
(401, 119, '_seleccionar_plantilla', 'field_612272506e122'),
(402, 120, 'seleccionar_plantilla', 'Normal'),
(403, 120, '_seleccionar_plantilla', 'field_612272506e122'),
(404, 121, 'seleccionar_plantilla', 'Normal'),
(405, 121, '_seleccionar_plantilla', 'field_612272506e122'),
(406, 122, 'seleccionar_plantilla', 'Intermedia'),
(407, 122, '_seleccionar_plantilla', 'field_612272506e122'),
(408, 123, 'seleccionar_plantilla', 'Normal'),
(409, 123, '_seleccionar_plantilla', 'field_612272506e122'),
(410, 124, 'seleccionar_plantilla', 'Importante'),
(411, 124, '_seleccionar_plantilla', 'field_612272506e122'),
(412, 125, 'seleccionar_plantilla', 'Normal'),
(413, 125, '_seleccionar_plantilla', 'field_612272506e122'),
(414, 126, 'seleccionar_plantilla', 'Intermedia'),
(415, 126, '_seleccionar_plantilla', 'field_612272506e122'),
(416, 127, 'seleccionar_plantilla', 'Normal'),
(417, 127, '_seleccionar_plantilla', 'field_612272506e122'),
(418, 128, 'seleccionar_plantilla', 'Importante'),
(419, 128, '_seleccionar_plantilla', 'field_612272506e122'),
(420, 129, 'seleccionar_plantilla', 'Importante'),
(421, 129, '_seleccionar_plantilla', 'field_612272506e122'),
(422, 130, 'seleccionar_plantilla', 'Intermedia'),
(423, 130, '_seleccionar_plantilla', 'field_612272506e122'),
(424, 131, 'seleccionar_plantilla', 'Normal'),
(425, 131, '_seleccionar_plantilla', 'field_612272506e122'),
(426, 132, 'seleccionar_plantilla', 'Importante'),
(427, 132, '_seleccionar_plantilla', 'field_612272506e122'),
(428, 133, 'seleccionar_plantilla', 'Normal'),
(429, 133, '_seleccionar_plantilla', 'field_612272506e122'),
(430, 134, 'seleccionar_plantilla', 'Intermedia'),
(431, 134, '_seleccionar_plantilla', 'field_612272506e122'),
(432, 140, '_edit_last', '1'),
(433, 140, '_edit_lock', '1631406448:1'),
(434, 146, '_edit_last', '1'),
(435, 146, '_edit_lock', '1631392428:1'),
(438, 152, '_cdp_origin', '1'),
(439, 152, '_cdp_origin_site', '-1'),
(440, 152, '_cdp_origin_title', ' ¡Hola, mundo! #[Counter]'),
(441, 152, '_cdp_counter', '2'),
(442, 152, '_edit_lock', '1631415501:1'),
(445, 152, '_wp_old_slug', 'hola-mundo'),
(446, 152, '_edit_last', '1'),
(447, 154, '_cdp_origin', '152'),
(448, 154, '_cdp_origin_site', '-1'),
(449, 154, '_cdp_origin_title', ' ¡Hola, mundo! #2 #[Counter]'),
(450, 154, '_cdp_counter', '2'),
(451, 154, '_cdp_origin', '1'),
(452, 154, '_wp_old_slug', 'hola-mundo'),
(453, 154, '_edit_lock', '1631416701:1'),
(454, 154, '_edit_last', '1'),
(455, 154, '_wp_old_slug', 'hola-mundo-2'),
(456, 155, '_wp_attached_file', '2021/09/foto1.jpeg'),
(457, 155, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:500;s:6:"height";i:625;s:4:"file";s:18:"2021/09/foto1.jpeg";s:5:"sizes";a:7:{s:6:"medium";a:4:{s:4:"file";s:18:"foto1-240x300.jpeg";s:5:"width";i:240;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:9:"thumbnail";a:4:{s:4:"file";s:18:"foto1-150x150.jpeg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:19:"thumb-noticias-home";a:4:{s:4:"file";s:18:"foto1-275x275.jpeg";s:5:"width";i:275;s:6:"height";i:275;s:9:"mime-type";s:10:"image/jpeg";}s:32:"thumb-noticias-sticky-intermedia";a:4:{s:4:"file";s:18:"foto1-275x400.jpeg";s:5:"width";i:275;s:6:"height";i:400;s:9:"mime-type";s:10:"image/jpeg";}s:28:"thumb-noticias-sticky-normal";a:4:{s:4:"file";s:18:"foto1-490x375.jpeg";s:5:"width";i:490;s:6:"height";i:375;s:9:"mime-type";s:10:"image/jpeg";}s:32:"thumb-noticias-sticky-importante";a:4:{s:4:"file";s:18:"foto1-500x555.jpeg";s:5:"width";i:500;s:6:"height";i:555;s:9:"mime-type";s:10:"image/jpeg";}s:27:"thumb-noticias-sticky-95x75";a:4:{s:4:"file";s:16:"foto1-95x75.jpeg";s:5:"width";i:95;s:6:"height";i:75;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(458, 154, '_thumbnail_id', '49'),
(459, 152, '_thumbnail_id', '155'),
(462, 1, '_thumbnail_id', '49'),
(463, 156, '_menu_item_type', 'post_type_archive'),
(464, 156, '_menu_item_menu_item_parent', '0'),
(465, 156, '_menu_item_object_id', '-8'),
(466, 156, '_menu_item_object', 'noticias'),
(467, 156, '_menu_item_target', ''),
(468, 156, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(469, 156, '_menu_item_xfn', ''),
(470, 156, '_menu_item_url', ''),
(472, 36, '_wp_old_date', '2021-08-28'),
(473, 37, '_wp_old_date', '2021-08-28'),
(474, 38, '_wp_old_date', '2021-08-28'),
(475, 39, '_wp_old_date', '2021-08-28'),
(476, 40, '_wp_old_date', '2021-08-28'),
(477, 12, '_wp_old_date', '2021-08-28'),
(478, 13, '_wp_old_date', '2021-08-28'),
(479, 14, '_wp_old_date', '2021-08-28'),
(480, 15, '_wp_old_date', '2021-08-28'),
(481, 27, '_wp_old_date', '2021-08-28'),
(482, 33, '_wp_old_date', '2021-08-28'),
(483, 41, '_wp_old_date', '2021-08-28'),
(484, 42, '_wp_old_date', '2021-08-28'),
(485, 43, '_wp_old_date', '2021-08-28') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(486, 44, '_wp_old_date', '2021-08-28'),
(487, 45, '_wp_old_date', '2021-08-28'),
(488, 46, '_wp_old_date', '2021-08-28'),
(489, 157, 'seleccionar_plantilla', 'Normal'),
(490, 157, '_seleccionar_plantilla', 'field_612272506e122'),
(491, 158, 'seleccionar_plantilla', 'Intermedia'),
(492, 158, '_seleccionar_plantilla', 'field_612272506e122'),
(493, 159, 'seleccionar_plantilla', 'Normal'),
(494, 159, '_seleccionar_plantilla', 'field_612272506e122'),
(495, 160, 'seleccionar_plantilla', 'Importante'),
(496, 160, '_seleccionar_plantilla', 'field_612272506e122'),
(497, 163, '_edit_last', '1'),
(498, 163, '_edit_lock', '1631406607:1'),
(499, 152, 'texto_introductorio', 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ad veritatis, placeat laudantium, aliquid tempore magnam voluptatibus nisi tenetur commodi perferendis, voluptatum dolorem iure voluptas itaque voluptatem magni earum? Corporis, veniam.'),
(500, 152, '_texto_introductorio', 'field_613d4a43c4e91'),
(501, 154, 'texto_introductorio', ''),
(502, 154, '_texto_introductorio', 'field_613d4a43c4e91'),
(503, 165, '_edit_last', '1'),
(504, 165, '_edit_lock', '1631416748:1'),
(505, 165, 'texto_introductorio', ''),
(506, 165, '_texto_introductorio', 'field_613d4a43c4e91'),
(507, 166, '_edit_last', '1'),
(508, 166, '_edit_lock', '1631415302:1'),
(509, 166, 'texto_introductorio', ''),
(510, 166, '_texto_introductorio', 'field_613d4a43c4e91'),
(511, 167, '_edit_last', '1'),
(512, 167, 'texto_introductorio', ''),
(513, 167, '_texto_introductorio', 'field_613d4a43c4e91'),
(514, 167, '_edit_lock', '1631415302:1'),
(515, 168, 'seleccionar_plantilla', 'Intermedia'),
(516, 168, '_seleccionar_plantilla', 'field_612272506e122'),
(517, 169, 'seleccionar_plantilla', 'Normal'),
(518, 169, '_seleccionar_plantilla', 'field_612272506e122'),
(519, 170, 'seleccionar_plantilla', 'Intermedia'),
(520, 170, '_seleccionar_plantilla', 'field_612272506e122'),
(521, 171, 'seleccionar_plantilla', 'Importante'),
(522, 171, '_seleccionar_plantilla', 'field_612272506e122'),
(523, 173, 'seleccionar_plantilla', 'Intermedia'),
(524, 173, '_seleccionar_plantilla', 'field_612272506e122'),
(525, 174, 'seleccionar_plantilla', 'Normal'),
(526, 174, '_seleccionar_plantilla', 'field_612272506e122'),
(527, 175, 'seleccionar_plantilla', 'Importante'),
(528, 175, '_seleccionar_plantilla', 'field_612272506e122'),
(529, 176, 'seleccionar_plantilla', 'Intermedia'),
(530, 176, '_seleccionar_plantilla', 'field_612272506e122'),
(531, 177, 'seleccionar_plantilla', 'Normal'),
(532, 177, '_seleccionar_plantilla', 'field_612272506e122'),
(533, 178, 'seleccionar_plantilla', 'Intermedia'),
(534, 178, '_seleccionar_plantilla', 'field_612272506e122'),
(535, 179, 'seleccionar_plantilla', 'Importante'),
(536, 179, '_seleccionar_plantilla', 'field_612272506e122'),
(537, 180, 'seleccionar_plantilla', 'Importante'),
(538, 180, '_seleccionar_plantilla', 'field_612272506e122') ;

#
# End of data contents of table `wp_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_posts`
#

DROP TABLE IF EXISTS `wp_posts`;


#
# Table structure of table `wp_posts`
#

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wp_posts`
#
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2021-08-13 02:46:22', '2021-08-13 02:46:22', '<!-- wp:paragraph -->\n<p>Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡luego empieza a escribir! Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {"align":"center","id":49,"sizeSlug":"large","linkDestination":"none"} -->\n<div class="wp-block-image"><figure class="aligncenter size-large"><img src="http://talcualstaging.ml/wp-content/uploads/2021/08/noticia2-1024x715.jpeg" alt="" class="wp-image-49"/><figcaption>Héberger un Tanguy ne permet pas d’échapper au surloyer...&nbsp;Eric Berger dans le film «&nbsp;Tanguy&nbsp;» (2001).</figcaption></figure></div>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including <a href="#">versions of Lorem Ipsum.</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>listado</li><li>listado 2</li><li>listado 3</li><li>listado 4</li><li>listado 5</li><li>listado 6</li></ul>\n<!-- /wp:list -->', '¡Hola, mundo!', '', 'publish', 'open', 'open', '', 'hola-mundo', '', '', '2021-09-11 22:04:25', '2021-09-11 22:04:25', '', 0, 'http://talcualstaging.ml/?p=1', 0, 'post', '', 1),
(2, 1, '2021-08-13 02:46:22', '2021-08-13 02:46:22', '<!-- wp:paragraph -->\n<p>Esta es una página de ejemplo. Es diferente a una entrada del blog porque permanecerá en un solo lugar y aparecerá en la navegación de tu sitio (en la mayoría de los temas). La mayoría de las personas comienzan con una página «Acerca de» que les presenta a los visitantes potenciales del sitio. Podrías decir algo así:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>¡Bienvenido! Soy camarero de día, aspirante a actor de noche y esta es mi web. Vivo en Mairena del Alcor, tengo un perro que se llama Firulais y me gusta el rebujito. (Y las tardes largas con café).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>…o algo así:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class="wp-block-quote"><p>La empresa «Mariscos Recio» fue fundada por Antonio Recio Mata. Empezó siendo una pequeña empresa que suministraba marisco a hoteles y restaurantes, pero poco a poco se ha ido transformando en un gran imperio. Mariscos Recio, el mar al mejor precio.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como nuevo usuario de WordPress, deberías ir a <a href="http://talcualstaging.ml/wp-admin/">tu escritorio</a> para borrar esta página y crear nuevas páginas para tu contenido. ¡Pásalo bien!</p>\n<!-- /wp:paragraph -->', 'Página de ejemplo', '', 'publish', 'closed', 'open', '', 'pagina-ejemplo', '', '', '2021-08-13 02:46:22', '2021-08-13 02:46:22', '', 0, 'http://talcualstaging.ml/?page_id=2', 0, 'page', '', 0),
(3, 1, '2021-08-13 02:46:22', '2021-08-13 02:46:22', '<!-- wp:heading --><h2>Quiénes somos</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class="privacy-policy-tutorial">Texto sugerido: </strong>La dirección de nuestra web es: http://talcualstaging.ml.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Comentarios</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class="privacy-policy-tutorial">Texto sugerido: </strong>Cuando los visitantes dejan comentarios en la web, recopilamos los datos que se muestran en el formulario de comentarios, así como la dirección IP del visitante y la cadena de agentes de usuario del navegador para ayudar a la detección de spam.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Una cadena anónima creada a partir de tu dirección de correo electrónico (también llamada hash) puede ser proporcionada al servicio de Gravatar para ver si la estás usando. La política de privacidad del servicio Gravatar está disponible aquí: https://automattic.com/privacy/. Después de la aprobación de tu comentario, la imagen de tu perfil es visible para el público en el contexto de tu comentario.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Medios</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class="privacy-policy-tutorial">Texto sugerido: </strong>Si subes imágenes a la web, deberías evitar subir imágenes con datos de ubicación (GPS EXIF) incluidos. Los visitantes de la web pueden descargar y extraer cualquier dato de ubicación de las imágenes de la web.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Cookies</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class="privacy-policy-tutorial">Texto sugerido: </strong>Si dejas un comentario en nuestro sitio puedes elegir guardar tu nombre, dirección de correo electrónico y web en cookies. Esto es para tu comodidad, para que no tengas que volver a rellenar tus datos cuando dejes otro comentario. Estas cookies tendrán una duración de un año.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Si tienes una cuenta y te conectas a este sitio, instalaremos una cookie temporal para determinar si tu navegador acepta cookies. Esta cookie no contiene datos personales y se elimina al cerrar el navegador.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Cuando accedas, también instalaremos varias cookies para guardar tu información de acceso y tus opciones de visualización de pantalla. Las cookies de acceso duran dos días, y las cookies de opciones de pantalla duran un año. Si seleccionas «Recuérdarme», tu acceso perdurará durante dos semanas. Si sales de tu cuenta, las cookies de acceso se eliminarán.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Si editas o publicas un artículo se guardará una cookie adicional en tu navegador. Esta cookie no incluye datos personales y simplemente indica el ID del artículo que acabas de editar. Caduca después de 1 día.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Contenido incrustado de otros sitios web</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class="privacy-policy-tutorial">Texto sugerido: </strong>Los artículos de este sitio pueden incluir contenido incrustado (por ejemplo, vídeos, imágenes, artículos, etc.). El contenido incrustado de otras webs se comporta exactamente de la misma manera que si el visitante hubiera visitado la otra web.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Estas web pueden recopilar datos sobre ti, utilizar cookies, incrustar un seguimiento adicional de terceros, y supervisar tu interacción con ese contenido incrustado, incluido el seguimiento de tu interacción con el contenido incrustado si tienes una cuenta y estás conectado a esa web.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Con quién compartimos tus datos</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class="privacy-policy-tutorial">Texto sugerido: </strong>Si solicitas un restablecimiento de contraseña, tu dirección IP será incluida en el correo electrónico de restablecimiento.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Cuánto tiempo conservamos tus datos</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class="privacy-policy-tutorial">Texto sugerido: </strong>Si dejas un comentario, el comentario y sus metadatos se conservan indefinidamente. Esto es para que podamos reconocer y aprobar comentarios sucesivos automáticamente, en lugar de mantenerlos en una cola de moderación.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>De los usuarios que se registran en nuestra web (si los hay), también almacenamos la información personal que proporcionan en su perfil de usuario. Todos los usuarios pueden ver, editar o eliminar su información personal en cualquier momento (excepto que no pueden cambiar su nombre de usuario). Los administradores de la web también pueden ver y editar esa información.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Qué derechos tienes sobre tus datos</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class="privacy-policy-tutorial">Texto sugerido: </strong>Si tienes una cuenta o has dejado comentarios en esta web, puedes solicitar recibir un archivo de exportación de los datos personales que tenemos sobre ti, incluyendo cualquier dato que nos hayas proporcionado. También puedes solicitar que eliminemos cualquier dato personal que tengamos sobre ti. Esto no incluye ningún dato que estemos obligados a conservar con fines administrativos, legales o de seguridad.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Dónde enviamos tus datos</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class="privacy-policy-tutorial">Texto sugerido: </strong>Los comentarios de los visitantes puede que los revise un servicio de detección automática de spam.</p><!-- /wp:paragraph -->', 'Política de privacidad', '', 'draft', 'closed', 'open', '', 'politica-privacidad', '', '', '2021-08-13 02:46:22', '2021-08-13 02:46:22', '', 0, 'http://talcualstaging.ml/?page_id=3', 0, 'page', '', 0),
(5, 1, '2021-08-17 22:14:45', '2021-08-17 22:14:45', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit "Send"]\nPeriodico "[your-subject]"\nPeriodico <wordpress@periodico.sitios.mb>\nFrom: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Periodico (http://talcualstaging.ml)\nmarcelombs2109@gmail.com\nReply-To: [your-email]\n\n0\n0\n\nPeriodico "[your-subject]"\nPeriodico <wordpress@periodico.sitios.mb>\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Periodico (http://talcualstaging.ml)\n[your-email]\nReply-To: marcelombs2109@gmail.com\n\n0\n0\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2021-08-17 22:14:45', '2021-08-17 22:14:45', '', 0, 'http://talcualstaging.ml/?post_type=wpcf7_contact_form&p=5', 0, 'wpcf7_contact_form', '', 0),
(6, 1, '2021-08-18 21:02:05', '2021-08-18 21:02:05', '', 'Inicio', '', 'publish', 'closed', 'closed', '', 'inicio', '', '', '2021-09-13 10:56:02', '2021-09-13 10:56:02', '', 0, 'http://talcualstaging.ml/?page_id=6', 0, 'page', '', 0),
(7, 1, '2021-08-18 21:02:05', '2021-08-18 21:02:05', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-18 21:02:05', '2021-08-18 21:02:05', '', 6, 'http://talcualstaging.ml/?p=7', 0, 'revision', '', 0),
(12, 1, '2021-09-11 22:22:12', '2021-08-20 11:21:21', '', 'Podcasts', '', 'publish', 'closed', 'closed', '', 'transfondo', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=12', 7, 'nav_menu_item', '', 0),
(13, 1, '2021-09-11 22:22:12', '2021-08-20 11:21:21', '', 'Reportajes', '', 'publish', 'closed', 'closed', '', 'historias', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=13', 8, 'nav_menu_item', '', 0),
(14, 1, '2021-09-11 22:22:12', '2021-08-20 11:21:21', '', 'Análisis', '', 'publish', 'closed', 'closed', '', 'deporte', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=14', 9, 'nav_menu_item', '', 0),
(15, 1, '2021-09-11 22:22:12', '2021-08-20 11:21:21', '', 'Enfoques y entrevistas', '', 'publish', 'closed', 'closed', '', 'ocio', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=15', 10, 'nav_menu_item', '', 0),
(18, 1, '2021-08-22 14:57:09', '2021-08-22 14:57:09', '«Blue Circles (ft. CSoul)» en ccMixter por unreal_dm. Lanzamiento: 2011.', 'Blue Circles (ft. CSoul)', '', 'inherit', 'open', 'closed', '', 'blue-circles-ft-csoul', '', '', '2021-08-22 14:57:09', '2021-08-22 14:57:09', '', 0, 'http://talcualstaging.ml/wp-content/uploads/2021/08/unreal_dm_-_Blue_Circles.mp3', 0, 'attachment', 'audio/mpeg', 0),
(19, 1, '2021-08-22 15:52:38', '2021-08-22 15:52:38', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:13:"page_template";s:8:"operator";s:2:"==";s:5:"value";s:32:"page-templates/template-home.php";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Plantillas', 'plantillas', 'trash', 'closed', 'closed', '', 'group_612272471496c__trashed', '', '', '2021-08-22 15:57:20', '2021-08-22 15:57:20', '', 0, 'http://talcualstaging.ml/?post_type=acf-field-group&#038;p=19', 0, 'acf-field-group', '', 0),
(20, 1, '2021-08-22 15:52:38', '2021-08-22 15:52:38', 'a:13:{s:4:"type";s:6:"select";s:12:"instructions";s:0:"";s:8:"required";i:1;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:7:"choices";a:3:{s:6:"Normal";s:6:"Normal";s:10:"Intermedia";s:10:"Intermedia";s:10:"Importante";s:10:"Importante";}s:13:"default_value";b:0;s:10:"allow_null";i:0;s:8:"multiple";i:0;s:2:"ui";i:0;s:13:"return_format";s:5:"value";s:4:"ajax";i:0;s:11:"placeholder";s:0:"";}', 'Seleccionar Plantilla', 'seleccionar_plantilla', 'trash', 'closed', 'closed', '', 'field_612272506e122__trashed', '', '', '2021-08-22 15:57:20', '2021-08-22 15:57:20', '', 19, 'http://talcualstaging.ml/?post_type=acf-field&#038;p=20', 0, 'acf-field', '', 0),
(21, 1, '2021-08-22 16:01:47', '2021-08-22 16:01:47', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-22 16:01:47', '2021-08-22 16:01:47', '', 6, 'http://talcualstaging.ml/?p=21', 0, 'revision', '', 0),
(22, 1, '2021-08-22 16:02:01', '2021-08-22 16:02:01', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-22 16:02:01', '2021-08-22 16:02:01', '', 6, 'http://talcualstaging.ml/?p=22', 0, 'revision', '', 0),
(23, 1, '2021-08-22 16:05:34', '2021-08-22 16:05:34', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-22 16:05:34', '2021-08-22 16:05:34', '', 6, 'http://talcualstaging.ml/?p=23', 0, 'revision', '', 0),
(24, 1, '2021-08-22 16:05:46', '2021-08-22 16:05:46', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-22 16:05:46', '2021-08-22 16:05:46', '', 6, 'http://talcualstaging.ml/?p=24', 0, 'revision', '', 0),
(25, 1, '2021-08-22 16:06:37', '2021-08-22 16:06:37', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-22 16:06:37', '2021-08-22 16:06:37', '', 6, 'http://talcualstaging.ml/?p=25', 0, 'revision', '', 0),
(26, 1, '2021-08-22 16:07:16', '2021-08-22 16:07:16', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-22 16:07:16', '2021-08-22 16:07:16', '', 6, 'http://talcualstaging.ml/?p=26', 0, 'revision', '', 0),
(27, 1, '2021-09-11 22:22:12', '2021-08-22 18:49:42', '', 'Empresas', '', 'publish', 'closed', 'closed', '', 'empresa-y-negocios', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=27', 11, 'nav_menu_item', '', 0),
(28, 1, '2021-08-22 20:13:55', '2021-08-22 20:13:55', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-22 20:13:55', '2021-08-22 20:13:55', '', 6, 'http://talcualstaging.ml/?p=28', 0, 'revision', '', 0),
(29, 1, '2021-08-22 20:38:48', '2021-08-22 20:38:48', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-22 20:38:48', '2021-08-22 20:38:48', '', 6, 'http://talcualstaging.ml/?p=29', 0, 'revision', '', 0),
(30, 1, '2021-08-22 20:56:04', '2021-08-22 20:56:04', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-22 20:56:04', '2021-08-22 20:56:04', '', 6, 'http://talcualstaging.ml/?p=30', 0, 'revision', '', 0),
(31, 1, '2021-08-22 20:58:33', '2021-08-22 20:58:33', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-22 20:58:33', '2021-08-22 20:58:33', '', 6, 'http://talcualstaging.ml/?p=31', 0, 'revision', '', 0),
(33, 1, '2021-09-11 22:22:12', '2021-08-28 13:04:34', '', 'C’est La Vie', '', 'publish', 'closed', 'closed', '', 'cest-la-vie', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=33', 12, 'nav_menu_item', '', 0),
(34, 1, '2021-08-28 13:22:40', '2021-08-28 13:22:40', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-28 13:22:40', '2021-08-28 13:22:40', '', 6, 'http://talcualstaging.ml/?p=34', 0, 'revision', '', 0),
(35, 1, '2021-08-28 13:26:36', '2021-08-28 13:26:36', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-28 13:26:36', '2021-08-28 13:26:36', '', 6, 'http://talcualstaging.ml/?p=35', 0, 'revision', '', 0),
(36, 1, '2021-09-11 22:22:11', '2021-08-28 13:49:18', '', 'Política', '', 'publish', 'closed', 'closed', '', 'politica', '', '', '2021-09-11 22:22:11', '2021-09-11 22:22:11', '', 0, 'http://talcualstaging.ml/?p=36', 2, 'nav_menu_item', '', 0),
(37, 1, '2021-09-11 22:22:12', '2021-08-28 13:49:18', '', 'Economía', '', 'publish', 'closed', 'closed', '', 'economia', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=37', 3, 'nav_menu_item', '', 0),
(38, 1, '2021-09-11 22:22:12', '2021-08-28 13:49:18', '', 'Sociedad', '', 'publish', 'closed', 'closed', '', 'sociedad', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=38', 4, 'nav_menu_item', '', 0),
(39, 1, '2021-09-11 22:22:12', '2021-08-28 13:49:18', '', 'Ciudades', '', 'publish', 'closed', 'closed', '', 'ciudades', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=39', 5, 'nav_menu_item', '', 0),
(40, 1, '2021-09-11 22:22:12', '2021-08-28 13:49:44', '', 'Internacional', '', 'publish', 'closed', 'closed', '', 'internacional', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=40', 6, 'nav_menu_item', '', 0),
(41, 1, '2021-09-11 22:22:12', '2021-08-28 14:51:11', '', 'Nuestra gente', '', 'publish', 'closed', 'closed', '', 'nuestra-gente', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=41', 13, 'nav_menu_item', '', 0),
(42, 1, '2021-09-11 22:22:12', '2021-08-28 14:51:11', '', 'En el consultorio', '', 'publish', 'closed', 'closed', '', 'en-el-consultorio', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=42', 14, 'nav_menu_item', '', 0),
(43, 1, '2021-09-11 22:22:12', '2021-08-28 14:51:11', '', 'Gourmet', '', 'publish', 'closed', 'closed', '', 'gourmet', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=43', 15, 'nav_menu_item', '', 0),
(44, 1, '2021-09-11 22:22:12', '2021-08-28 14:51:11', '', 'Vinos y licores', '', 'publish', 'closed', 'closed', '', 'vinos-y-licores', '', '', '2021-09-11 22:22:12', '2021-09-11 22:22:12', '', 0, 'http://talcualstaging.ml/?p=44', 16, 'nav_menu_item', '', 0),
(45, 1, '2021-09-11 22:22:13', '2021-08-28 14:51:11', '', 'Lecturas', '', 'publish', 'closed', 'closed', '', 'lecturas', '', '', '2021-09-11 22:22:13', '2021-09-11 22:22:13', '', 0, 'http://talcualstaging.ml/?p=45', 17, 'nav_menu_item', '', 0),
(46, 1, '2021-09-11 22:22:13', '2021-08-28 14:51:11', '', 'Perros y gatos', '', 'publish', 'closed', 'closed', '', 'perros-y-gatos', '', '', '2021-09-11 22:22:13', '2021-09-11 22:22:13', '', 0, 'http://talcualstaging.ml/?p=46', 18, 'nav_menu_item', '', 0),
(47, 1, '2021-08-28 15:33:51', '2021-08-28 15:33:51', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-28 15:33:51', '2021-08-28 15:33:51', '', 6, 'http://talcualstaging.ml/?p=47', 0, 'revision', '', 0),
(48, 1, '2021-08-28 15:48:27', '2021-08-28 15:48:27', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-28 15:48:27', '2021-08-28 15:48:27', '', 6, 'http://talcualstaging.ml/?p=48', 0, 'revision', '', 0),
(49, 1, '2021-08-28 20:07:36', '2021-08-28 20:07:36', '', 'noticia2', '', 'inherit', 'open', 'closed', '', 'noticia2', '', '', '2021-08-28 20:07:36', '2021-08-28 20:07:36', '', 1, 'http://talcualstaging.ml/wp-content/uploads/2021/08/noticia2.jpeg', 0, 'attachment', 'image/jpeg', 0),
(51, 1, '2021-08-28 20:07:47', '2021-08-28 20:07:47', '<!-- wp:paragraph -->\n<p>Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡luego empieza a escribir!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {"align":"center","id":49,"sizeSlug":"large","linkDestination":"none"} -->\n<div class="wp-block-image"><figure class="aligncenter size-large"><img src="http://talcualstaging.ml/wp-content/uploads/2021/08/noticia2-1024x715.jpeg" alt="" class="wp-image-49"/></figure></div>\n<!-- /wp:image -->', '¡Hola, mundo!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-08-28 20:07:47', '2021-08-28 20:07:47', '', 1, 'http://talcualstaging.ml/?p=51', 0, 'revision', '', 0),
(52, 1, '2021-08-29 01:15:34', '2021-08-29 01:15:34', '<!-- wp:paragraph -->\n<p>Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡luego empieza a escribir! Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {"align":"center","id":49,"sizeSlug":"large","linkDestination":"none"} -->\n<div class="wp-block-image"><figure class="aligncenter size-large"><img src="http://talcualstaging.ml/wp-content/uploads/2021/08/noticia2-1024x715.jpeg" alt="" class="wp-image-49"/></figure></div>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>listado</li><li>listado 2</li><li>listado 3</li><li>listado 4</li><li>listado 5</li><li>listado 6</li></ul>\n<!-- /wp:list -->', '¡Hola, mundo!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-08-29 01:15:34', '2021-08-29 01:15:34', '', 1, 'http://talcualstaging.ml/?p=52', 0, 'revision', '', 0),
(53, 1, '2021-08-29 01:24:06', '2021-08-29 01:24:06', '<!-- wp:paragraph -->\n<p>Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡luego empieza a escribir! Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {"align":"center","id":49,"sizeSlug":"large","linkDestination":"none"} -->\n<div class="wp-block-image"><figure class="aligncenter size-large"><img src="http://talcualstaging.ml/wp-content/uploads/2021/08/noticia2-1024x715.jpeg" alt="" class="wp-image-49"/><figcaption>Héberger un Tanguy ne permet pas d’échapper au surloyer... Eric Berger dans le film « Tanguy » (2001).</figcaption></figure></div>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>listado</li><li>listado 2</li><li>listado 3</li><li>listado 4</li><li>listado 5</li><li>listado 6</li></ul>\n<!-- /wp:list -->', '¡Hola, mundo!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-08-29 01:24:06', '2021-08-29 01:24:06', '', 1, 'http://talcualstaging.ml/?p=53', 0, 'revision', '', 0),
(54, 1, '2021-08-29 01:27:02', '2021-08-29 01:27:02', '<!-- wp:paragraph -->\n<p>Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡luego empieza a escribir! Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {"align":"center","id":49,"sizeSlug":"large","linkDestination":"none"} -->\n<div class="wp-block-image"><figure class="aligncenter size-large"><img src="http://talcualstaging.ml/wp-content/uploads/2021/08/noticia2-1024x715.jpeg" alt="" class="wp-image-49"/></figure></div>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>listado</li><li>listado 2</li><li>listado 3</li><li>listado 4</li><li>listado 5</li><li>listado 6</li></ul>\n<!-- /wp:list -->', '¡Hola, mundo!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-08-29 01:27:02', '2021-08-29 01:27:02', '', 1, 'http://talcualstaging.ml/?p=54', 0, 'revision', '', 0),
(55, 1, '2021-08-29 01:28:04', '2021-08-29 01:28:04', '<!-- wp:paragraph -->\n<p>Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡luego empieza a escribir! Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {"align":"center","id":49,"sizeSlug":"large","linkDestination":"none"} -->\n<div class="wp-block-image"><figure class="aligncenter size-large"><img src="http://talcualstaging.ml/wp-content/uploads/2021/08/noticia2-1024x715.jpeg" alt="" class="wp-image-49"/><figcaption>Héberger un Tanguy ne permet pas d’échapper au surloyer... Eric Berger dans le film « Tanguy » (2001).</figcaption></figure></div>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>listado</li><li>listado 2</li><li>listado 3</li><li>listado 4</li><li>listado 5</li><li>listado 6</li></ul>\n<!-- /wp:list -->', '¡Hola, mundo!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-08-29 01:28:04', '2021-08-29 01:28:04', '', 1, 'http://talcualstaging.ml/?p=55', 0, 'revision', '', 0),
(56, 1, '2021-08-29 01:40:26', '2021-08-29 01:40:26', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-08-29 01:40:26', '2021-08-29 01:40:26', '', 6, 'http://talcualstaging.ml/?p=56', 0, 'revision', '', 0),
(57, 1, '2021-08-29 01:52:37', '2021-08-29 01:52:37', '<!-- wp:paragraph -->\n<p>Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡luego empieza a escribir! Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {"align":"center","id":49,"sizeSlug":"large","linkDestination":"none"} -->\n<div class="wp-block-image"><figure class="aligncenter size-large"><img src="http://talcualstaging.ml/wp-content/uploads/2021/08/noticia2-1024x715.jpeg" alt="" class="wp-image-49"/><figcaption>Héberger un Tanguy ne permet pas d’échapper au surloyer...&nbsp;Eric Berger dans le film «&nbsp;Tanguy&nbsp;» (2001).</figcaption></figure></div>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including <a href="#">versions of Lorem Ipsum.</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>listado</li><li>listado 2</li><li>listado 3</li><li>listado 4</li><li>listado 5</li><li>listado 6</li></ul>\n<!-- /wp:list -->', '¡Hola, mundo!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-08-29 01:52:37', '2021-08-29 01:52:37', '', 1, 'http://talcualstaging.ml/?p=57', 0, 'revision', '', 0),
(58, 1, '2021-08-29 17:53:47', '2021-08-29 17:53:47', 'De gré ou de force, on savait déjà que les Français avaient beaucoup, beaucoup épargné en 2020 en réaction aux différents effets de la pandémie de Covid-19 ; on sait désormais qu’une partie de cette épargne s’est dirigée vers les produits solidaires.\r\n\r\nSelon le baromètre de la finance solidaire La Croix/Finansol, l’encours de ces produits au 31 décembre 2020, c’est-à-dire le total des sommes qui y sont placées, a en effet augmenté de 5 milliards d’euros – un record annuel – pour atteindre 20,3 milliards. Soit une hausse d’environ 33 %, selon les chiffres publiés le 7 juin.', 'Noticia de prueba', '', 'publish', 'open', 'closed', '', 'noticia-de-prueba', '', '', '2021-08-29 17:57:42', '2021-08-29 17:57:42', '', 0, 'http://talcualstaging.ml/?post_type=noticia&#038;p=58', 0, 'noticia', '', 0),
(59, 1, '2021-09-01 11:28:05', '2021-09-01 11:28:05', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-01 11:28:05', '2021-09-01 11:28:05', '', 6, 'http://talcualstaging.ml/?p=59', 0, 'revision', '', 0),
(60, 1, '2021-09-01 23:13:18', '2021-09-01 23:13:18', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-01 23:13:18', '2021-09-01 23:13:18', '', 6, 'http://talcualstaging.ml/?p=60', 0, 'revision', '', 0),
(61, 1, '2021-09-02 00:03:05', '2021-09-02 00:03:05', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-02 00:03:05', '2021-09-02 00:03:05', '', 6, 'http://talcualstaging.ml/?p=61', 0, 'revision', '', 0),
(62, 1, '2021-09-02 00:03:36', '2021-09-02 00:03:36', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-02 00:03:36', '2021-09-02 00:03:36', '', 6, 'http://talcualstaging.ml/?p=62', 0, 'revision', '', 0),
(63, 1, '2021-09-02 00:10:56', '2021-09-02 00:10:56', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-02 00:10:56', '2021-09-02 00:10:56', '', 6, 'http://talcualstaging.ml/?p=63', 0, 'revision', '', 0),
(64, 1, '2021-09-02 00:16:55', '2021-09-02 00:16:55', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-02 00:16:55', '2021-09-02 00:16:55', '', 6, 'http://talcualstaging.ml/?p=64', 0, 'revision', '', 0),
(65, 1, '2021-09-02 00:20:35', '2021-09-02 00:20:35', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-02 00:20:35', '2021-09-02 00:20:35', '', 6, 'http://talcualstaging.ml/?p=65', 0, 'revision', '', 0),
(66, 1, '2021-09-02 02:07:39', '2021-09-02 02:07:39', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-02 02:07:39', '2021-09-02 02:07:39', '', 6, 'http://talcualstaging.ml/?p=66', 0, 'revision', '', 0),
(67, 1, '2021-09-02 02:07:54', '2021-09-02 02:07:54', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-02 02:07:54', '2021-09-02 02:07:54', '', 6, 'http://talcualstaging.ml/?p=67', 0, 'revision', '', 0),
(68, 1, '2021-09-02 02:12:47', '2021-09-02 02:12:47', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-02 02:12:47', '2021-09-02 02:12:47', '', 6, 'http://talcualstaging.ml/?p=68', 0, 'revision', '', 0),
(69, 1, '2021-09-03 23:42:26', '2021-09-03 23:42:26', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-03 23:42:26', '2021-09-03 23:42:26', '', 6, 'http://talcualstaging.ml/?p=69', 0, 'revision', '', 0),
(70, 1, '2021-09-04 01:49:29', '2021-09-04 01:49:29', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-04 01:49:29', '2021-09-04 01:49:29', '', 6, 'http://talcualstaging.ml/?p=70', 0, 'revision', '', 0),
(71, 1, '2021-09-04 01:49:48', '2021-09-04 01:49:48', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-04 01:49:48', '2021-09-04 01:49:48', '', 6, 'http://talcualstaging.ml/?p=71', 0, 'revision', '', 0),
(72, 1, '2021-09-04 01:51:36', '2021-09-04 01:51:36', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-04 01:51:36', '2021-09-04 01:51:36', '', 6, 'http://talcualstaging.ml/?p=72', 0, 'revision', '', 0),
(73, 1, '2021-09-04 15:56:48', '2021-09-04 15:56:48', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-04 15:56:48', '2021-09-04 15:56:48', '', 6, 'http://talcualstaging.ml/?p=73', 0, 'revision', '', 0),
(74, 1, '2021-09-04 15:57:32', '2021-09-04 15:57:32', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-04 15:57:32', '2021-09-04 15:57:32', '', 6, 'http://talcualstaging.ml/?p=74', 0, 'revision', '', 0),
(75, 1, '2021-09-04 15:57:40', '2021-09-04 15:57:40', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-04 15:57:40', '2021-09-04 15:57:40', '', 6, 'http://talcualstaging.ml/?p=75', 0, 'revision', '', 0),
(76, 1, '2021-09-04 16:39:16', '2021-09-04 16:39:16', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-04 16:39:16', '2021-09-04 16:39:16', '', 6, 'http://talcualstaging.ml/?p=76', 0, 'revision', '', 0),
(77, 1, '2021-09-04 16:48:11', '2021-09-04 16:48:11', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-04 16:48:11', '2021-09-04 16:48:11', '', 6, 'http://talcualstaging.ml/?p=77', 0, 'revision', '', 0),
(78, 1, '2021-09-04 16:48:37', '2021-09-04 16:48:37', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-04 16:48:37', '2021-09-04 16:48:37', '', 6, 'http://talcualstaging.ml/?p=78', 0, 'revision', '', 0),
(79, 1, '2021-09-04 16:50:41', '2021-09-04 16:50:41', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-04 16:50:41', '2021-09-04 16:50:41', '', 6, 'http://talcualstaging.ml/?p=79', 0, 'revision', '', 0),
(81, 1, '2021-09-05 13:21:02', '2021-09-05 13:21:02', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-05 13:21:02', '2021-09-05 13:21:02', '', 6, 'http://talcualstaging.ml/?p=81', 0, 'revision', '', 0),
(82, 1, '2021-09-05 14:10:17', '2021-09-05 14:10:17', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-05 14:10:17', '2021-09-05 14:10:17', '', 6, 'http://talcualstaging.ml/?p=82', 0, 'revision', '', 0),
(83, 1, '2021-09-05 14:48:09', '2021-09-05 14:48:09', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-05 14:48:09', '2021-09-05 14:48:09', '', 6, 'http://talcualstaging.ml/?p=83', 0, 'revision', '', 0),
(84, 1, '2021-09-05 14:51:38', '2021-09-05 14:51:38', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-05 14:51:38', '2021-09-05 14:51:38', '', 6, 'http://talcualstaging.ml/?p=84', 0, 'revision', '', 0),
(85, 1, '2021-09-05 16:05:28', '2021-09-05 16:05:28', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-05 16:05:28', '2021-09-05 16:05:28', '', 6, 'http://talcualstaging.ml/?p=85', 0, 'revision', '', 0),
(86, 1, '2021-09-05 16:09:25', '2021-09-05 16:09:25', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-05 16:09:25', '2021-09-05 16:09:25', '', 6, 'http://talcualstaging.ml/?p=86', 0, 'revision', '', 0),
(87, 1, '2021-09-05 16:22:20', '2021-09-05 16:22:20', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-05 16:22:20', '2021-09-05 16:22:20', '', 6, 'http://talcualstaging.ml/?p=87', 0, 'revision', '', 0),
(88, 1, '2021-09-05 16:24:12', '2021-09-05 16:24:12', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-05 16:24:12', '2021-09-05 16:24:12', '', 6, 'http://talcualstaging.ml/?p=88', 0, 'revision', '', 0),
(89, 1, '2021-09-06 10:47:42', '2021-09-06 10:47:42', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-06 10:47:42', '2021-09-06 10:47:42', '', 6, 'http://talcualstaging.ml/?p=89', 0, 'revision', '', 0),
(90, 1, '2021-09-06 11:01:04', '2021-09-06 11:01:04', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-06 11:01:04', '2021-09-06 11:01:04', '', 6, 'http://talcualstaging.ml/?p=90', 0, 'revision', '', 0),
(91, 1, '2021-09-06 11:05:42', '2021-09-06 11:05:42', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-06 11:05:42', '2021-09-06 11:05:42', '', 6, 'http://talcualstaging.ml/?p=91', 0, 'revision', '', 0),
(92, 1, '2021-09-06 11:06:36', '2021-09-06 11:06:36', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-06 11:06:36', '2021-09-06 11:06:36', '', 6, 'http://talcualstaging.ml/?p=92', 0, 'revision', '', 0),
(93, 1, '2021-09-07 02:42:27', '2021-09-07 02:42:27', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-07 02:42:27', '2021-09-07 02:42:27', '', 6, 'http://talcualstaging.ml/?p=93', 0, 'revision', '', 0),
(94, 1, '2021-09-07 03:03:49', '2021-09-07 03:03:49', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-07 03:03:49', '2021-09-07 03:03:49', '', 6, 'http://talcualstaging.ml/?p=94', 0, 'revision', '', 0),
(95, 1, '2021-09-07 03:14:06', '2021-09-07 03:14:06', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-07 03:14:06', '2021-09-07 03:14:06', '', 6, 'http://talcualstaging.ml/?p=95', 0, 'revision', '', 0),
(96, 1, '2021-09-11 12:52:21', '2021-09-11 12:52:21', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 12:52:21', '2021-09-11 12:52:21', '', 6, 'http://talcualstaging.ml/?p=96', 0, 'revision', '', 0),
(97, 1, '2021-09-11 12:52:45', '2021-09-11 12:52:45', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 12:52:45', '2021-09-11 12:52:45', '', 6, 'http://talcualstaging.ml/?p=97', 0, 'revision', '', 0),
(98, 1, '2021-09-11 13:09:33', '2021-09-11 13:09:33', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 13:09:33', '2021-09-11 13:09:33', '', 6, 'http://talcualstaging.ml/?p=98', 0, 'revision', '', 0),
(99, 1, '2021-09-11 13:10:56', '2021-09-11 13:10:56', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 13:10:56', '2021-09-11 13:10:56', '', 6, 'http://talcualstaging.ml/?p=99', 0, 'revision', '', 0),
(100, 1, '2021-09-11 13:32:14', '2021-09-11 13:32:14', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 13:32:14', '2021-09-11 13:32:14', '', 6, 'http://talcualstaging.ml/?p=100', 0, 'revision', '', 0),
(101, 1, '2021-09-11 14:05:19', '2021-09-11 14:05:19', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 14:05:19', '2021-09-11 14:05:19', '', 6, 'http://talcualstaging.ml/?p=101', 0, 'revision', '', 0),
(102, 1, '2021-09-11 14:08:07', '2021-09-11 14:08:07', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 14:08:07', '2021-09-11 14:08:07', '', 6, 'http://talcualstaging.ml/?p=102', 0, 'revision', '', 0),
(103, 1, '2021-09-11 14:08:53', '2021-09-11 14:08:53', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 14:08:53', '2021-09-11 14:08:53', '', 6, 'http://talcualstaging.ml/?p=103', 0, 'revision', '', 0),
(104, 1, '2021-09-11 14:17:16', '2021-09-11 14:17:16', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 14:17:16', '2021-09-11 14:17:16', '', 6, 'http://talcualstaging.ml/?p=104', 0, 'revision', '', 0),
(105, 1, '2021-09-11 14:32:08', '2021-09-11 14:32:08', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 14:32:08', '2021-09-11 14:32:08', '', 6, 'http://talcualstaging.ml/?p=105', 0, 'revision', '', 0),
(106, 1, '2021-09-11 14:53:40', '2021-09-11 14:53:40', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 14:53:40', '2021-09-11 14:53:40', '', 6, 'http://talcualstaging.ml/?p=106', 0, 'revision', '', 0),
(107, 1, '2021-09-11 14:56:06', '2021-09-11 14:56:06', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 14:56:06', '2021-09-11 14:56:06', '', 6, 'http://talcualstaging.ml/?p=107', 0, 'revision', '', 0),
(108, 1, '2021-09-11 15:01:55', '2021-09-11 15:01:55', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:01:55', '2021-09-11 15:01:55', '', 6, 'http://talcualstaging.ml/?p=108', 0, 'revision', '', 0) ;
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(109, 1, '2021-09-11 15:03:00', '2021-09-11 15:03:00', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:03:00', '2021-09-11 15:03:00', '', 6, 'http://talcualstaging.ml/?p=109', 0, 'revision', '', 0),
(110, 1, '2021-09-11 15:03:33', '2021-09-11 15:03:33', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:03:33', '2021-09-11 15:03:33', '', 6, 'http://talcualstaging.ml/?p=110', 0, 'revision', '', 0) ;
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(111, 1, '2021-09-11 15:04:28', '2021-09-11 15:04:28', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:04:28', '2021-09-11 15:04:28', '', 6, 'http://talcualstaging.ml/?p=111', 0, 'revision', '', 0),
(112, 1, '2021-09-11 15:04:39', '2021-09-11 15:04:39', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:04:39', '2021-09-11 15:04:39', '', 6, 'http://talcualstaging.ml/?p=112', 0, 'revision', '', 0),
(113, 1, '2021-09-11 15:04:50', '2021-09-11 15:04:50', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:04:50', '2021-09-11 15:04:50', '', 6, 'http://talcualstaging.ml/?p=113', 0, 'revision', '', 0),
(114, 1, '2021-09-11 15:19:43', '2021-09-11 15:19:43', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:19:43', '2021-09-11 15:19:43', '', 6, 'http://talcualstaging.ml/?p=114', 0, 'revision', '', 0),
(115, 1, '2021-09-11 15:20:18', '2021-09-11 15:20:18', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:20:18', '2021-09-11 15:20:18', '', 6, 'http://talcualstaging.ml/?p=115', 0, 'revision', '', 0),
(116, 1, '2021-09-11 15:21:14', '2021-09-11 15:21:14', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:21:14', '2021-09-11 15:21:14', '', 6, 'http://talcualstaging.ml/?p=116', 0, 'revision', '', 0),
(117, 1, '2021-09-11 15:37:33', '2021-09-11 15:37:33', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:37:33', '2021-09-11 15:37:33', '', 6, 'http://talcualstaging.ml/?p=117', 0, 'revision', '', 0),
(118, 1, '2021-09-11 15:38:00', '2021-09-11 15:38:00', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:38:00', '2021-09-11 15:38:00', '', 6, 'http://talcualstaging.ml/?p=118', 0, 'revision', '', 0),
(119, 1, '2021-09-11 15:42:51', '2021-09-11 15:42:51', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:42:51', '2021-09-11 15:42:51', '', 6, 'http://talcualstaging.ml/?p=119', 0, 'revision', '', 0),
(120, 1, '2021-09-11 15:48:34', '2021-09-11 15:48:34', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:48:34', '2021-09-11 15:48:34', '', 6, 'http://talcualstaging.ml/?p=120', 0, 'revision', '', 0),
(121, 1, '2021-09-11 15:48:48', '2021-09-11 15:48:48', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:48:48', '2021-09-11 15:48:48', '', 6, 'http://talcualstaging.ml/?p=121', 0, 'revision', '', 0),
(122, 1, '2021-09-11 15:49:20', '2021-09-11 15:49:20', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:49:20', '2021-09-11 15:49:20', '', 6, 'http://talcualstaging.ml/?p=122', 0, 'revision', '', 0),
(123, 1, '2021-09-11 15:50:09', '2021-09-11 15:50:09', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:50:09', '2021-09-11 15:50:09', '', 6, 'http://talcualstaging.ml/?p=123', 0, 'revision', '', 0),
(124, 1, '2021-09-11 15:51:31', '2021-09-11 15:51:31', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:51:31', '2021-09-11 15:51:31', '', 6, 'http://talcualstaging.ml/?p=124', 0, 'revision', '', 0),
(125, 1, '2021-09-11 15:56:39', '2021-09-11 15:56:39', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:56:39', '2021-09-11 15:56:39', '', 6, 'http://talcualstaging.ml/?p=125', 0, 'revision', '', 0),
(126, 1, '2021-09-11 15:57:42', '2021-09-11 15:57:42', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:57:42', '2021-09-11 15:57:42', '', 6, 'http://talcualstaging.ml/?p=126', 0, 'revision', '', 0),
(127, 1, '2021-09-11 15:59:39', '2021-09-11 15:59:39', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:59:39', '2021-09-11 15:59:39', '', 6, 'http://talcualstaging.ml/?p=127', 0, 'revision', '', 0),
(128, 1, '2021-09-11 15:59:55', '2021-09-11 15:59:55', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 15:59:55', '2021-09-11 15:59:55', '', 6, 'http://talcualstaging.ml/?p=128', 0, 'revision', '', 0),
(129, 1, '2021-09-11 16:01:48', '2021-09-11 16:01:48', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 16:01:48', '2021-09-11 16:01:48', '', 6, 'http://talcualstaging.ml/?p=129', 0, 'revision', '', 0),
(130, 1, '2021-09-11 16:05:32', '2021-09-11 16:05:32', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 16:05:32', '2021-09-11 16:05:32', '', 6, 'http://talcualstaging.ml/?p=130', 0, 'revision', '', 0),
(131, 1, '2021-09-11 16:11:03', '2021-09-11 16:11:03', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 16:11:03', '2021-09-11 16:11:03', '', 6, 'http://talcualstaging.ml/?p=131', 0, 'revision', '', 0),
(132, 1, '2021-09-11 16:11:30', '2021-09-11 16:11:30', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 16:11:30', '2021-09-11 16:11:30', '', 6, 'http://talcualstaging.ml/?p=132', 0, 'revision', '', 0),
(133, 1, '2021-09-11 16:12:49', '2021-09-11 16:12:49', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 16:12:49', '2021-09-11 16:12:49', '', 6, 'http://talcualstaging.ml/?p=133', 0, 'revision', '', 0),
(134, 1, '2021-09-11 16:16:47', '2021-09-11 16:16:47', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 16:16:47', '2021-09-11 16:16:47', '', 6, 'http://talcualstaging.ml/?p=134', 0, 'revision', '', 0),
(135, 1, '2021-09-11 18:57:43', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-09-11 18:57:43', '0000-00-00 00:00:00', '', 0, 'http://talcualstaging.ml/?post_type=reportaje_especial&p=135', 0, 'reportaje_especial', '', 0),
(136, 1, '2021-09-11 19:13:15', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-09-11 19:13:15', '0000-00-00 00:00:00', '', 0, 'http://talcualstaging.ml/?post_type=reportaje_especial&p=136', 0, 'reportaje_especial', '', 0),
(137, 1, '2021-09-11 19:27:32', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-09-11 19:27:32', '0000-00-00 00:00:00', '', 0, 'http://talcualstaging.ml/?post_type=reportaje_especial&p=137', 0, 'reportaje_especial', '', 0),
(138, 1, '2021-09-11 20:12:25', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-09-11 20:12:25', '0000-00-00 00:00:00', '', 0, 'http://talcualstaging.ml/?post_type=reporte_especial&p=138', 0, 'reporte_especial', '', 0),
(139, 1, '2021-09-11 20:22:09', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-09-11 20:22:09', '0000-00-00 00:00:00', '', 0, 'http://talcualstaging.ml/?post_type=reporte_especial&p=139', 0, 'reporte_especial', '', 0),
(140, 1, '2021-09-11 20:28:00', '2021-09-11 20:28:00', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:13:"post_taxonomy";s:8:"operator";s:2:"==";s:5:"value";s:34:"categoria_estilo_de_vida:entre-nos";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Personaje Entre Nos', 'personaje-entre-nos', 'publish', 'closed', 'closed', '', 'group_613d105992795', '', '', '2021-09-12 00:29:37', '2021-09-12 00:29:37', '', 0, 'http://talcualstaging.ml/?post_type=acf-field-group&#038;p=140', 0, 'acf-field-group', '', 0),
(141, 1, '2021-09-11 20:28:01', '2021-09-11 20:28:01', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Nombre del Personaje', 'nombre_del_personaje', 'publish', 'closed', 'closed', '', 'field_613d108f135ea', '', '', '2021-09-11 20:28:01', '2021-09-11 20:28:01', '', 140, 'http://talcualstaging.ml/?post_type=acf-field&p=141', 0, 'acf-field', '', 0),
(142, 1, '2021-09-11 20:28:01', '2021-09-11 20:28:01', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Frase', 'frase', 'publish', 'closed', 'closed', '', 'field_613d10b5135eb', '', '', '2021-09-11 20:28:01', '2021-09-11 20:28:01', '', 140, 'http://talcualstaging.ml/?post_type=acf-field&p=142', 1, 'acf-field', '', 0),
(143, 1, '2021-09-11 20:28:01', '2021-09-11 20:28:01', 'a:10:{s:4:"type";s:8:"repeater";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:9:"collapsed";s:0:"";s:3:"min";s:0:"";s:3:"max";s:0:"";s:6:"layout";s:5:"table";s:12:"button_label";s:0:"";}', 'Fotografías', 'fotografias', 'publish', 'closed', 'closed', '', 'field_613d10ef135ec', '', '', '2021-09-12 00:29:37', '2021-09-12 00:29:37', '', 140, 'http://talcualstaging.ml/?post_type=acf-field&#038;p=143', 2, 'acf-field', '', 0),
(144, 1, '2021-09-11 20:28:19', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-09-11 20:28:19', '0000-00-00 00:00:00', '', 0, 'http://talcualstaging.ml/?post_type=estilo_de_vida&p=144', 0, 'estilo_de_vida', '', 0),
(145, 1, '2021-09-11 20:29:19', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-09-11 20:29:19', '0000-00-00 00:00:00', '', 0, 'http://talcualstaging.ml/?post_type=estilo_de_vida&p=145', 0, 'estilo_de_vida', '', 0),
(146, 1, '2021-09-11 20:31:36', '2021-09-11 20:31:36', 'a:7:{s:8:"location";a:1:{i:0;a:1:{i:0;a:3:{s:5:"param";s:12:"options_page";s:8:"operator";s:2:"==";s:5:"value";s:11:"acf-options";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Contactos', 'contactos', 'publish', 'closed', 'closed', '', 'group_613d11e90e151', '', '', '2021-09-11 20:31:36', '2021-09-11 20:31:36', '', 0, 'http://talcualstaging.ml/?post_type=acf-field-group&#038;p=146', 0, 'acf-field-group', '', 0),
(147, 1, '2021-09-11 20:31:36', '2021-09-11 20:31:36', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Facebook', 'facebook', 'publish', 'closed', 'closed', '', 'field_613d11efb5b1d', '', '', '2021-09-11 20:31:36', '2021-09-11 20:31:36', '', 146, 'http://talcualstaging.ml/?post_type=acf-field&p=147', 0, 'acf-field', '', 0),
(148, 1, '2021-09-11 20:31:36', '2021-09-11 20:31:36', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Twitter', 'twitter', 'publish', 'closed', 'closed', '', 'field_613d1210b5b1e', '', '', '2021-09-11 20:31:36', '2021-09-11 20:31:36', '', 146, 'http://talcualstaging.ml/?post_type=acf-field&p=148', 1, 'acf-field', '', 0),
(149, 1, '2021-09-11 20:31:36', '2021-09-11 20:31:36', 'a:10:{s:4:"type";s:4:"text";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:9:"maxlength";s:0:"";}', 'Instagram', 'instagram', 'publish', 'closed', 'closed', '', 'field_613d1216b5b1f', '', '', '2021-09-11 20:31:36', '2021-09-11 20:31:36', '', 146, 'http://talcualstaging.ml/?post_type=acf-field&p=149', 2, 'acf-field', '', 0),
(151, 1, '2021-09-11 20:57:35', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-09-11 20:57:35', '0000-00-00 00:00:00', '', 0, 'http://talcualstaging.ml/?post_type=noticias&p=151', 0, 'noticias', '', 0),
(152, 1, '2021-09-11 21:03:13', '2021-09-11 21:03:13', '<!-- wp:paragraph -->\r\n<p>Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡luego empieza a escribir! Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:image {"align":"center","id":49,"sizeSlug":"large","linkDestination":"none"} -->\r\n<div class="wp-block-image">\r\n<figure class="aligncenter size-large"><img class="wp-image-49" src="http://talcualstaging.ml/wp-content/uploads/2021/08/noticia2-1024x715.jpeg" alt="" />\r\n<figcaption>Héberger un Tanguy ne permet pas d’échapper au surloyer... Eric Berger dans le film « Tanguy » (2001).</figcaption>\r\n</figure>\r\n</div>\r\n<!-- /wp:image -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including <a href="#">versions of Lorem Ipsum.</a></p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:list -->\r\n<ul>\r\n<li>listado</li>\r\n<li>listado 2</li>\r\n<li>listado 3</li>\r\n<li>listado 4</li>\r\n<li>listado 5</li>\r\n<li>listado 6</li>\r\n</ul>\r\n<!-- /wp:list -->', '¡Hola, mundo! #2', '', 'publish', 'open', 'open', '', 'hola-mundo-2', '', '', '2021-09-12 02:18:06', '2021-09-12 02:18:06', '', 0, 'http://talcualstaging.ml/?p=152', 0, 'noticias', '', 0),
(153, 1, '2021-09-11 21:03:13', '2021-09-11 21:03:13', '<!-- wp:paragraph -->\n<p>Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡luego empieza a escribir! Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image {"align":"center","id":49,"sizeSlug":"large","linkDestination":"none"} -->\n<div class="wp-block-image"><figure class="aligncenter size-large"><img src="http://talcualstaging.ml/wp-content/uploads/2021/08/noticia2-1024x715.jpeg" alt="" class="wp-image-49"/><figcaption>Héberger un Tanguy ne permet pas d’échapper au surloyer...&nbsp;Eric Berger dans le film «&nbsp;Tanguy&nbsp;» (2001).</figcaption></figure></div>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including <a href="#">versions of Lorem Ipsum.</a></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>listado</li><li>listado 2</li><li>listado 3</li><li>listado 4</li><li>listado 5</li><li>listado 6</li></ul>\n<!-- /wp:list -->', '¡Hola, mundo! #2', '', 'inherit', 'closed', 'closed', '', '152-revision-v1', '', '', '2021-09-11 21:03:13', '2021-09-11 21:03:13', '', 152, 'http://talcualstaging.ml/?p=153', 0, 'revision', '', 0),
(154, 1, '2021-09-11 21:39:21', '2021-09-11 21:39:21', '<!-- wp:paragraph -->\r\n<p>Bienvenido a WordPress. Esta es tu primera entrada. Edítala o bórrala, ¡luego empieza a escribir! Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:image {"align":"center","id":49,"sizeSlug":"large","linkDestination":"none"} -->\r\n<div class="wp-block-image">\r\n<figure class="aligncenter size-large"><img class="wp-image-49" src="http://talcualstaging.ml/wp-content/uploads/2021/08/noticia2-1024x715.jpeg" alt="" />\r\n<figcaption>Héberger un Tanguy ne permet pas d’échapper au surloyer... Eric Berger dans le film « Tanguy » (2001).</figcaption>\r\n</figure>\r\n</div>\r\n<!-- /wp:image -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:paragraph -->\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including <a href="#">versions of Lorem Ipsum.</a></p>\r\n<!-- /wp:paragraph -->\r\n\r\n<!-- wp:list -->\r\n<ul>\r\n<li>listado</li>\r\n<li>listado 2</li>\r\n<li>listado 3</li>\r\n<li>listado 4</li>\r\n<li>listado 5</li>\r\n<li>listado 6</li>\r\n</ul>\r\n<!-- /wp:list -->', 'Hola como estas marcelo', '', 'publish', 'open', 'closed', '', 'hola-mundo-2-2', '', '', '2021-09-12 03:01:06', '2021-09-12 03:01:06', '', 0, 'http://talcualstaging.ml/?post_type=noticias&#038;p=154', 0, 'noticias', '', 0),
(155, 1, '2021-09-11 22:02:52', '2021-09-11 22:02:52', '', 'foto1', '', 'inherit', 'open', 'closed', '', 'foto1', '', '', '2021-09-11 22:02:52', '2021-09-11 22:02:52', '', 152, 'http://talcualstaging.ml/wp-content/uploads/2021/09/foto1.jpeg', 0, 'attachment', 'image/jpeg', 0),
(156, 1, '2021-09-11 22:22:11', '2021-09-11 22:22:11', '', 'Noticias', '', 'publish', 'closed', 'closed', '', 'noticias', '', '', '2021-09-11 22:22:11', '2021-09-11 22:22:11', '', 0, 'http://talcualstaging.ml/?p=156', 1, 'nav_menu_item', '', 0),
(157, 1, '2021-09-11 22:28:06', '2021-09-11 22:28:06', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 22:28:06', '2021-09-11 22:28:06', '', 6, 'http://talcualstaging.ml/?p=157', 0, 'revision', '', 0),
(158, 1, '2021-09-11 22:40:54', '2021-09-11 22:40:54', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 22:40:54', '2021-09-11 22:40:54', '', 6, 'http://talcualstaging.ml/?p=158', 0, 'revision', '', 0),
(159, 1, '2021-09-11 22:42:50', '2021-09-11 22:42:50', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-11 22:42:50', '2021-09-11 22:42:50', '', 6, 'http://talcualstaging.ml/?p=159', 0, 'revision', '', 0),
(160, 1, '2021-09-12 00:10:40', '2021-09-12 00:10:40', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-12 00:10:40', '2021-09-12 00:10:40', '', 6, 'http://talcualstaging.ml/?p=160', 0, 'revision', '', 0),
(161, 1, '2021-09-12 00:29:37', '2021-09-12 00:29:37', 'a:15:{s:4:"type";s:5:"image";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"return_format";s:3:"url";s:12:"preview_size";s:6:"medium";s:7:"library";s:3:"all";s:9:"min_width";s:0:"";s:10:"min_height";s:0:"";s:8:"min_size";s:0:"";s:9:"max_width";s:0:"";s:10:"max_height";s:0:"";s:8:"max_size";s:0:"";s:10:"mime_types";s:0:"";}', 'Fotografía', 'fotografia', 'publish', 'closed', 'closed', '', 'field_613d49db8e353', '', '', '2021-09-12 00:29:37', '2021-09-12 00:29:37', '', 143, 'http://talcualstaging.ml/?post_type=acf-field&p=161', 0, 'acf-field', '', 0),
(162, 1, '2021-09-12 00:30:04', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-09-12 00:30:04', '0000-00-00 00:00:00', '', 0, 'http://talcualstaging.ml/?post_type=estilo_de_vida&p=162', 0, 'estilo_de_vida', '', 0),
(163, 1, '2021-09-12 00:32:24', '2021-09-12 00:32:24', 'a:7:{s:8:"location";a:8:{i:0;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:8:"noticias";}}i:1;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:8:"podcasts";}}i:2;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:16:"reporte_especial";}}i:3;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:8:"analisis";}}i:4;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:20:"enfoques_entrevistas";}}i:5;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:8:"empresas";}}i:6;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:14:"estilo_de_vida";}}i:7;a:1:{i:0;a:3:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:9:"bbc_mundo";}}}s:8:"position";s:6:"normal";s:5:"style";s:7:"default";s:15:"label_placement";s:3:"top";s:21:"instruction_placement";s:5:"label";s:14:"hide_on_screen";s:0:"";s:11:"description";s:0:"";}', 'Texto Introductorio', 'texto-introductorio', 'publish', 'closed', 'closed', '', 'group_613d4a34b1f4a', '', '', '2021-09-12 00:32:28', '2021-09-12 00:32:28', '', 0, 'http://talcualstaging.ml/?post_type=acf-field-group&#038;p=163', 0, 'acf-field-group', '', 0),
(164, 1, '2021-09-12 00:32:24', '2021-09-12 00:32:24', 'a:10:{s:4:"type";s:8:"textarea";s:12:"instructions";s:0:"";s:8:"required";i:0;s:17:"conditional_logic";i:0;s:7:"wrapper";a:3:{s:5:"width";s:0:"";s:5:"class";s:0:"";s:2:"id";s:0:"";}s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:9:"maxlength";s:0:"";s:4:"rows";s:0:"";s:9:"new_lines";s:0:"";}', 'Texto Introductorio', 'texto_introductorio', 'publish', 'closed', 'closed', '', 'field_613d4a43c4e91', '', '', '2021-09-12 00:32:24', '2021-09-12 00:32:24', '', 163, 'http://talcualstaging.ml/?post_type=acf-field&p=164', 0, 'acf-field', '', 0),
(165, 1, '2021-09-12 01:47:33', '2021-09-12 01:47:33', '', 'fafaf', '', 'publish', 'open', 'closed', '', 'fafaf', '', '', '2021-09-12 01:47:33', '2021-09-12 01:47:33', '', 0, 'http://talcualstaging.ml/?post_type=estilo_de_vida&#038;p=165', 0, 'estilo_de_vida', '', 0),
(166, 1, '2021-09-12 02:23:11', '2021-09-12 02:23:11', '', 'fafafafa', '', 'publish', 'open', 'closed', '', 'fafafafa', '', '', '2021-09-12 02:24:01', '2021-09-12 02:24:01', '', 0, 'http://talcualstaging.ml/?post_type=estilo_de_vida&#038;p=166', 0, 'estilo_de_vida', '', 0),
(167, 1, '2021-09-12 02:29:52', '2021-09-12 02:29:52', '', 'fafafa', '', 'publish', 'open', 'closed', '', 'fafafa', '', '', '2021-09-12 02:29:52', '2021-09-12 02:29:52', '', 0, 'http://talcualstaging.ml/?post_type=noticias&#038;p=167', 0, 'noticias', '', 0),
(168, 1, '2021-09-12 19:49:13', '2021-09-12 19:49:13', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-12 19:49:13', '2021-09-12 19:49:13', '', 6, 'http://talcualstaging.ml/?p=168', 0, 'revision', '', 0),
(169, 1, '2021-09-12 21:48:29', '2021-09-12 21:48:29', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-12 21:48:29', '2021-09-12 21:48:29', '', 6, 'http://talcualstaging.ml/?p=169', 0, 'revision', '', 0),
(170, 1, '2021-09-12 22:27:00', '2021-09-12 22:27:00', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-12 22:27:00', '2021-09-12 22:27:00', '', 6, 'http://talcualstaging.ml/?p=170', 0, 'revision', '', 0),
(171, 1, '2021-09-12 22:34:09', '2021-09-12 22:34:09', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-12 22:34:09', '2021-09-12 22:34:09', '', 6, 'http://talcualstaging.ml/?p=171', 0, 'revision', '', 0),
(172, 1, '2021-09-13 10:01:59', '0000-00-00 00:00:00', '', 'Borrador automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2021-09-13 10:01:59', '0000-00-00 00:00:00', '', 0, 'http://talcualstaging.ml/?p=172', 0, 'post', '', 0),
(173, 1, '2021-09-13 10:02:22', '2021-09-13 10:02:22', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-13 10:02:22', '2021-09-13 10:02:22', '', 6, 'http://talcualstaging.ml/?p=173', 0, 'revision', '', 0),
(174, 1, '2021-09-13 10:24:08', '2021-09-13 10:24:08', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-13 10:24:08', '2021-09-13 10:24:08', '', 6, 'http://talcualstaging.ml/?p=174', 0, 'revision', '', 0),
(175, 1, '2021-09-13 10:30:55', '2021-09-13 10:30:55', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-13 10:30:55', '2021-09-13 10:30:55', '', 6, 'http://talcualstaging.ml/?p=175', 0, 'revision', '', 0),
(176, 1, '2021-09-13 10:31:28', '2021-09-13 10:31:28', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-13 10:31:28', '2021-09-13 10:31:28', '', 6, 'http://talcualstaging.ml/?p=176', 0, 'revision', '', 0),
(177, 1, '2021-09-13 10:31:52', '2021-09-13 10:31:52', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-13 10:31:52', '2021-09-13 10:31:52', '', 6, 'http://talcualstaging.ml/?p=177', 0, 'revision', '', 0),
(178, 1, '2021-09-13 10:49:25', '2021-09-13 10:49:25', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-13 10:49:25', '2021-09-13 10:49:25', '', 6, 'http://talcualstaging.ml/?p=178', 0, 'revision', '', 0),
(179, 1, '2021-09-13 10:55:59', '2021-09-13 10:55:59', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-13 10:55:59', '2021-09-13 10:55:59', '', 6, 'http://talcualstaging.ml/?p=179', 0, 'revision', '', 0),
(180, 1, '2021-09-13 10:56:02', '2021-09-13 10:56:02', '', 'Inicio', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2021-09-13 10:56:02', '2021-09-13 10:56:02', '', 6, 'http://talcualstaging.ml/?p=180', 0, 'revision', '', 0) ;

#
# End of data contents of table `wp_posts`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_relationships`
#

DROP TABLE IF EXISTS `wp_term_relationships`;


#
# Table structure of table `wp_term_relationships`
#

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wp_term_relationships`
#
INSERT INTO `wp_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(12, 2, 0),
(13, 2, 0),
(14, 2, 0),
(15, 2, 0),
(27, 2, 0),
(33, 2, 0),
(36, 2, 0),
(37, 2, 0),
(38, 2, 0),
(39, 2, 0),
(40, 2, 0),
(41, 2, 0),
(42, 2, 0),
(43, 2, 0),
(44, 2, 0),
(45, 2, 0),
(46, 2, 0),
(152, 1, 0),
(152, 3, 0),
(154, 1, 0),
(154, 3, 0),
(156, 2, 0),
(165, 10, 0),
(166, 10, 0) ;

#
# End of data contents of table `wp_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_taxonomy`
#

DROP TABLE IF EXISTS `wp_term_taxonomy`;


#
# Table structure of table `wp_term_taxonomy`
#

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wp_term_taxonomy`
#
INSERT INTO `wp_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 18),
(3, 3, 'categoria_noticias', '', 0, 2),
(4, 4, 'categoria_noticias', '', 0, 0),
(5, 5, 'categoria_noticias', '', 0, 0),
(6, 6, 'categoria_noticias', '', 0, 0),
(7, 7, 'categoria_noticias', '', 0, 0),
(8, 8, 'categoria_estilo_de_vida', '', 0, 0),
(9, 9, 'categoria_estilo_de_vida', '', 0, 0),
(10, 10, 'categoria_estilo_de_vida', '', 0, 2),
(11, 11, 'categoria_estilo_de_vida', '', 0, 0),
(12, 12, 'categoria_estilo_de_vida', '', 0, 0),
(13, 13, 'categoria_estilo_de_vida', '', 0, 0) ;

#
# End of data contents of table `wp_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wp_termmeta`
#

DROP TABLE IF EXISTS `wp_termmeta`;


#
# Table structure of table `wp_termmeta`
#

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wp_termmeta`
#

#
# End of data contents of table `wp_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_terms`
#

DROP TABLE IF EXISTS `wp_terms`;


#
# Table structure of table `wp_terms`
#

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wp_terms`
#
INSERT INTO `wp_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sin categoría', 'sin-categoria', 0),
(2, 'Menu', 'menu', 0),
(3, 'Política', 'politica', 0),
(4, 'Economía', 'economia', 0),
(5, 'Sociedad', 'sociedad', 0),
(6, 'Ciudades', 'ciudades', 0),
(7, 'Internacional', 'internacional', 0),
(8, 'Entre nos...', 'entre-nos', 0),
(9, 'En el consultorio', 'en-el-consultorio', 0),
(10, 'Buen provecho', 'buen-provecho', 0),
(11, 'Vinos y licores', 'vinos-y-licores', 0),
(12, 'Lecturas', 'lecturas', 0),
(13, 'Perros y gatos', 'perros_y_gatos', 0) ;

#
# End of data contents of table `wp_terms`
# --------------------------------------------------------



#
# Delete any existing table `wp_usermeta`
#

DROP TABLE IF EXISTS `wp_usermeta`;


#
# Table structure of table `wp_usermeta`
#

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wp_usermeta`
#
INSERT INTO `wp_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'periodico'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:"7e3e1002cc21b5be7f2396a0a82a550b5460bb7c42446908d20ced9a23a222ce";a:4:{s:10:"expiration";i:1632574306;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:105:"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36";s:5:"login";i:1631364706;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '172'),
(18, 1, 'community-events-location', 'a:1:{s:2:"ip";s:9:"127.0.0.0";}'),
(19, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:"link-target";i:1;s:15:"title-attribute";i:2;s:3:"xfn";i:3;s:11:"description";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:"add-post_tag";}'),
(21, 1, 'nav_menu_recently_edited', '2'),
(22, 1, 'wp_user-settings', 'libraryContent=browse&hidetb=1'),
(23, 1, 'wp_user-settings-time', '1631407176') ;

#
# End of data contents of table `wp_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_users`
#

DROP TABLE IF EXISTS `wp_users`;


#
# Table structure of table `wp_users`
#

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


#
# Data contents of table `wp_users`
#
INSERT INTO `wp_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'periodico', '$P$BLsByHk4PFFm89gZ1oMdP4g3yGlc.D/', 'periodico', 'marcelombs2109@gmail.com', 'http://talcualstaging.ml', '2021-08-13 02:46:21', '', 0, 'periodico') ;

#
# End of data contents of table `wp_users`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#

